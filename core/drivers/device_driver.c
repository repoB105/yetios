/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * device_driver.c
 *
 *  Created on: 11 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file device_driver.c
 */

#include "device.h"
#include "process.h"
#include "system_api.h"

extern device_t* getDeviceFromName(const char* name);

/**
 *
 * @param device_name
 * @param flags
 * @return
 */
uint32_t y_open(const char* device_name, uint32_t flags){
	dev_file_t* dev_file;
	device_t* device = getDeviceFromName(device_name);
	if(device == NULL){
		return 0;
	}
	if(device->device_state != DEV_STATE_INIT){
		return 0;
	}
	dev_file = (dev_file_t*) ytMalloc(sizeof(dev_file_t));
	if(dev_file == NULL){
		return 0;
	}
	dev_file->flags = flags;
	dev_file->ops = device->ops;
	if(dev_file->ops->open != NULL){
		if(dev_file->ops->open(device, dev_file) == RET_ERROR){
			ytFree(dev_file);
			return 0;
		}
	}
	dev_file->fil_state = DEV_FILE_OPENED;
	return (uint32_t) dev_file;
}

/**
 *
 * @param fildes
 * @param buf
 * @param bytes
 * @return
 */
size_t y_read(uint32_t fildes, void* buf, size_t bytes){
	dev_file_t* dev_file = (dev_file_t*) fildes;
	size_t ret = 0;
	if(dev_file == NULL){
		return 0;
	}
	if(dev_file->ops->read != NULL){
		ret = dev_file->ops->read(dev_file, (uint8_t*) buf, bytes);
	}
	dev_file->fil_state = DEV_FILE_OPENED;
	return ret;
}

/**
 *
 * @param fildes
 * @param buf
 * @param bytes
 * @return
 */
size_t y_write(uint32_t fildes, void* buf, size_t bytes){
	dev_file_t* dev_file = (dev_file_t*) fildes;
	size_t ret = 0;
	if(dev_file == NULL){
		return 0;
	}
	if(dev_file->ops->write != NULL){
		ret = dev_file->ops->write(dev_file, (uint8_t*) buf, bytes);
	}
	dev_file->fil_state = DEV_FILE_OPENED;
	return ret;
}

/**
 *
 * @param fildes
 * @param command
 * @param args
 * @return
 */
retval_t y_ioctl(uint32_t fildes, uint16_t command, void* args){
	dev_file_t* dev_file = (dev_file_t*) fildes;
	retval_t ret = RET_ERROR;
	if(dev_file == NULL){
		return ret;
	}
	if(dev_file->ops->ioctl != NULL){
		ret = dev_file->ops->ioctl(dev_file, command, args);
	}
	dev_file->fil_state = DEV_FILE_OPENED;
	return ret;
}

/**
 *
 * @param fildes
 * @return
 */
retval_t y_close(uint32_t fildes){
	dev_file_t* dev_file = (dev_file_t*) fildes;
	retval_t ret = RET_ERROR;
	if(dev_file == NULL){
		return ret;
	}

	if(dev_file->ops->close != NULL){
		ret = dev_file->ops->close(dev_file);
	}
	dev_file->fil_state =  DEV_FILE_CLOSED;
	ytFree(dev_file);
	return ret;
}
