/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * yetimote_stdio.c
 *
 *  Created on: 7 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file yetimote_stdio.c
 */

#include "yetimote_stdio.h"
#include "system_api.h"
#include "yetishell.h"

#if ENABLE_STDIO_FUNCS

#if USE_YETISHELL
#define YETISHELL_LINE_BUFFER_SIZE	256			//Number of bytes of the yetishell buffer. Larger lines wont be processed by the yetishell

static uint8_t yethisell_line_buffer[YETISHELL_LINE_BUFFER_SIZE];
static uint32_t read_line_semaphore_id;
static uint8_t* reading_buff_ptr;
static uint16_t yetishell_task_id;
static uint16_t reading_max_size;
static uint16_t reading_line = 0;

static void yetishellTask_callback(void const * argument);
#endif


#endif
static void null_printf(char* format, ...);


/**
 *
 * @return
 */
retval_t stdio_init(void){
	_printf = null_printf;
#if ENABLE_STDIO_FUNCS

#if (STDIO_INTERFACE == USB_STDIO)
	usb_stdio_init();
	_printf = usb_printf;
	#if USE_YETISHELL
	reading_line = 0;
	read_line_semaphore_id = ytSemaphoreCreate(1);
	ytSemaphoreWait(read_line_semaphore_id, YT_WAIT_FOREVER);
	yetishell_init();
	ytStartProcess("YetiShellTask", yetishellTask_callback, VERY_LOW_PRIORITY_PROCESS, 256, &yetishell_task_id, NULL);
	#endif
#elif (STDIO_INTERFACE == UART_STDIO)
	uart_stdio_init();
	_printf = uart_printf;
	#if USE_YETISHELL
	reading_line = 0;
	read_line_semaphore_id = ytSemaphoreCreate(1);
	ytSemaphoreWait(read_line_semaphore_id, YT_WAIT_FOREVER);
	yetishell_init();
	ytStartProcess("YetiShellTask", yetishellTask_callback, VERY_LOW_PRIORITY_PROCESS, 256, &yetishell_task_id, NULL);
	#endif
#endif
#endif
	return RET_OK;
}

/**
 *
 * @return
 */
retval_t stdio_deinit(void){
	_printf = null_printf;
#if ENABLE_STDIO_FUNCS

#if (STDIO_INTERFACE == USB_STDIO)
	usb_stdio_deinit();
	#if USE_YETISHELL
	reading_line = 0;
	ytExitProcess(yetishell_task_id);
	ytSemaphoreDelete(read_line_semaphore_id);
	yetishell_deinit();
	#endif
#elif (STDIO_INTERFACE == UART_STDIO)
	uart_stdio_deinit();
	#if USE_YETISHELL
	reading_line = 0;
	ytExitProcess(yetishell_task_id);
	ytSemaphoreDelete(read_line_semaphore_id);
	yetishell_deinit();
	#endif
#endif
#endif
	return RET_OK;
}

/**
 *
 * @param data
 * @param size
 * @param timeout
 * @return
 */
retval_t stdout_send(uint8_t* data, uint16_t size, uint32_t timeout){
	retval_t ret = RET_ERROR;
#if ENABLE_STDIO_FUNCS
	if(!size){
		return ret;
	}

#if (STDIO_INTERFACE == USB_STDIO)
	ret = usb_stdout_send(data, size, timeout);
#elif (STDIO_INTERFACE == UART_STDIO)
	ret = uart_stdout_send(data, size, timeout);
#endif

#endif
	return ret;
}

/**
 *
 * @param data
 * @param size
 * @param timeout
 * @return
 */
retval_t stdin_read(uint8_t* data, uint16_t size, uint32_t timeout){
	retval_t ret = RET_ERROR;
#if ENABLE_STDIO_FUNCS
	if(!size){
		return ret;
	}
#if USE_YETISHELL
	return ret;			//This function cannot be used together with the yetishell
#endif
#if (STDIO_INTERFACE == USB_STDIO)
	ret = usb_stdin_read(data, size, timeout);
#elif (STDIO_INTERFACE == UART_STDIO)
	ret = uart_stdin_read(data, size, timeout);
#endif
#endif
	return ret;
}


retval_t stdin_read_line(uint8_t* data, uint16_t max_size, uint32_t timeout){
	retval_t ret = RET_ERROR;
#if ENABLE_STDIO_FUNCS
	if(!max_size){
		return ret;
	}

#if USE_YETISHELL
#if ((STDIO_INTERFACE == USB_STDIO) | (STDIO_INTERFACE == UART_STDIO))
	if(!reading_line){
		reading_max_size = max_size;
		reading_buff_ptr = data;
		reading_line++;
		if(ytSemaphoreWait(read_line_semaphore_id, timeout) == RET_OK){
			if(reading_max_size){	//Line succesfully read
				ret = RET_OK;
			}
		}
		reading_line--;
	}
#endif
#else
#if (STDIO_INTERFACE == USB_STDIO)
	ret = usb_stdin_read_line(data, max_size, timeout);
#elif (STDIO_INTERFACE == UART_STDIO)
	ret = uart_stdin_read_line(data, max_size, timeout);
#endif
#endif

#endif
	return ret;
}

#if USE_YETISHELL
static void yetishellTask_callback(void const * argument){

	yetishell_command_data_t yetishell_command_data;
	retval_t ret;
	uint16_t line_size;
	while(1){
#if (STDIO_INTERFACE == USB_STDIO)
		ret = usb_stdin_read_line(yethisell_line_buffer, YETISHELL_LINE_BUFFER_SIZE, YT_WAIT_FOREVER);
#elif (STDIO_INTERFACE == UART_STDIO)
		ret = uart_stdin_read_line(yethisell_line_buffer, YETISHELL_LINE_BUFFER_SIZE, YT_WAIT_FOREVER);
#endif

		if(ret == RET_OK){

			//First process if anyone is using the function stdin_read_line
			if(reading_line){
				line_size = (uint16_t)strlen((char*)yethisell_line_buffer);
				if(line_size < reading_max_size){
					memcpy(reading_buff_ptr, yethisell_line_buffer, line_size+1);	//Also copy the \0 char
					reading_max_size = line_size;	//Line succesfully read
				}
				else{
					reading_max_size = 0; 	//Error reading line
				}
				ytSemaphoreRelease(read_line_semaphore_id);
			}

			//Then process yetishell commans

			if(get_yetishell_command_data((char*)yethisell_line_buffer, &yetishell_command_data) == RET_OK){	//Received a valid yetishell command
				if(yetishell_command_data.command_func != NULL){
					yetishell_command_data.command_func(yetishell_command_data.argc, yetishell_command_data.argv);	//Execute the command
				}
				delete_yetishell_command_data(&yetishell_command_data);		//Delete command data
			}

			ytPrintf(">");
		}
	}
}
#endif

static void null_printf(char* format, ...){
	return;
}
