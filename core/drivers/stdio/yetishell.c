/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * yetishell.c
 *
 *  Created on: 28 de ago. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file yetishell.c
 */

#include "yetishell.h"
#include "system_api.h"
#include <string.h>



#if ENABLE_STDIO_FUNCS && USE_YETISHELL

#define LIST_COMMANDS_PRINT_CMD_STR		"lcmd"
#define FREE_MEM_CMD_STR				"mem"

static gen_list* yetishell_commands_list = NULL;

static retval_t list_commands_print_cmd(uint16_t argc, char** argv);
static retval_t free_mem_cmd(uint16_t argc, char** argv);
#endif

/**
 *
 * @return
 */
retval_t yetishell_init(void){
#if ENABLE_STDIO_FUNCS && USE_YETISHELL
	if((yetishell_commands_list = gen_list_init()) != NULL){
		register_yetishell_command(LIST_COMMANDS_PRINT_CMD_STR, list_commands_print_cmd);
		register_yetishell_command(FREE_MEM_CMD_STR, free_mem_cmd);
		return RET_OK;
	}
#endif
	return RET_ERROR;
}

/**
 *
 * @return
 */
retval_t yetishell_deinit(void){
#if ENABLE_STDIO_FUNCS && USE_YETISHELL
	gen_list* current;
	gen_list* del;
	yetishell_stored_command_t* stored_command;
	if(yetishell_commands_list == NULL){
		return RET_ERROR;
	}

	//Unregister all commands
	current = yetishell_commands_list;

	while(current->next != NULL){			//Free all fields of the list
		stored_command = (yetishell_stored_command_t*) current->next->item;
		ytFree(stored_command->command_name);
		ytFree(stored_command);

		del = current->next;
		current->next = current->next->next;
		ytFree(del);
	}

	gen_list_remove_and_delete_all(yetishell_commands_list);
	return RET_OK;
#endif
	return RET_ERROR;
}


/**
 *
 * @param read_line
 * @param yetishell_command_data
 * @return
 */
retval_t get_yetishell_command_data(char* read_line, yetishell_command_data_t* yetishell_command_data){
#if ENABLE_STDIO_FUNCS && USE_YETISHELL
	gen_list* current;
	yetishell_stored_command_t* stored_command;
	if(yetishell_commands_list == NULL){
		return RET_ERROR;
	}
	if(yetishell_command_data == NULL){
		return RET_ERROR;
	}
	if(strlen(read_line) == 0){
		return RET_ERROR;
	}

	yetishell_command_data->argv = str_split(read_line, ' ', ((int*)&(yetishell_command_data->argc)) );	//Memory for argv is reserved inside str_split function. It is necessary to free it later

	if(!yetishell_command_data->argc){
		return RET_ERROR;
	}

	//Check if the command exists
	current = yetishell_commands_list;
	while(current->next != NULL){
		stored_command = (yetishell_stored_command_t*) current->next->item;
		if(strcmp(yetishell_command_data->argv[0], stored_command->command_name) == 0){
			yetishell_command_data->command_func = stored_command->command_func;
			return RET_OK;		//The command exists. It is copied in yetishell_command_data and ok is returned
		}
		current = current->next;
	}
	delete_yetishell_command_data(yetishell_command_data);
	ytPrintf("-Error: Command does not exist\r\n>");
#endif
	return RET_ERROR;
}


/**
 *
 * @param yetishell_command_data
 * @return
 */
retval_t delete_yetishell_command_data(yetishell_command_data_t* yetishell_command_data){
#if ENABLE_STDIO_FUNCS && USE_YETISHELL
	uint16_t i;
	if(yetishell_commands_list == NULL){
		return RET_ERROR;
	}
	if(yetishell_command_data == NULL){
		return RET_ERROR;
	}

	for (i=0; i<yetishell_command_data->argc; i++){			//When a command is executed, the memory occupied by the args must be freed
		ytFree(yetishell_command_data->argv[i]);
	}
	ytFree(yetishell_command_data->argv);
	yetishell_command_data->argc = 0;
	yetishell_command_data->command_func = NULL;

	return RET_OK;
#endif
	return RET_ERROR;

}


/**
 *
 * @param command_name
 * @param command_cb_func
 * @return
 */
retval_t register_yetishell_command(char* command_name, yetishell_func_t command_cb_func){
#if ENABLE_STDIO_FUNCS && USE_YETISHELL
	gen_list* current;
	yetishell_stored_command_t* new_stored_command;
	yetishell_stored_command_t* prev_stored_command;
	if(yetishell_commands_list == NULL){
		return RET_ERROR;
	}
	//Check if the command already exists
	current = yetishell_commands_list;
	while(current->next != NULL){
		prev_stored_command = (yetishell_stored_command_t*) current->next->item;
		if(strcmp(command_name, prev_stored_command->command_name) == 0){

			return RET_ERROR;
		}
		current = current->next;
	}

	//Create and register the new command
	new_stored_command = ytMalloc(sizeof(yetishell_stored_command_t));
	new_stored_command->command_name = ytMalloc(strlen(command_name));
	strcpy(new_stored_command->command_name, command_name);
	new_stored_command->command_func = command_cb_func;
	gen_list_add(yetishell_commands_list, (void*) new_stored_command);

	return RET_OK;
#endif
	return RET_ERROR;
}


/**
 *
 * @param command_name
 * @return
 */
retval_t unregister_yetishell_command(char* command_name){
#if ENABLE_STDIO_FUNCS && USE_YETISHELL
	gen_list* current;
	gen_list* del;
	yetishell_stored_command_t* stored_command;

	if(yetishell_commands_list == NULL){
		return RET_ERROR;
	}

	current = yetishell_commands_list;
	while(current->next != NULL){
		stored_command = (yetishell_stored_command_t*) current->next->item;
		if(strcmp(command_name, stored_command->command_name) == 0){
			ytFree(stored_command->command_name);
			ytFree(stored_command);

			del = current->next;
			current->next = current->next->next;
			ytFree(del);
			return RET_OK;

		}
		current= current->next;
	}
#endif
	return RET_ERROR;
}

#if ENABLE_STDIO_FUNCS && USE_YETISHELL
/**
 *
 * @param argc
 * @param argv
 * @return
 */
static retval_t list_commands_print_cmd(uint16_t argc, char** argv){
	gen_list* current;
	yetishell_stored_command_t* stored_command;

	current = yetishell_commands_list;
	while(current->next != NULL){
		stored_command = (yetishell_stored_command_t*) current->next->item;

		ytPrintf("-%s\r\n", stored_command->command_name);

		current= current->next;
	}
	return RET_OK;
}

/**
 *
 * @param argc
 * @param argv
 * @return
 */
static retval_t free_mem_cmd(uint16_t argc, char** argv){
	size_t freeMEM = xPortGetFreeHeapSize();
	ytPrintf( "-Available Memory: %.3f KB\r\n", (float32_t) ((float32_t)freeMEM/1000));
	return RET_OK;
}
#endif
