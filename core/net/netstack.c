/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * netstack.c
 *
 *  Created on: 24 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file netstack.c
 */
#include "netstack.h"
#include "system_api.h"
#include "netstack-conf.h"
#include "phy_layer.h"
#include "mac_layer.h"
#include "transport_layer.h"
#include "routing_layer.h"
#include <stdlib.h>

netstack_state_t netstack_state = NS_STACK_NO_INIT;

#if ENABLE_NETSTACK_ARCH

#ifdef DEBUG
#undef DEBUG
#undef PRINTF
#endif
#define DEBUG 1
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif


#define NETSTACK_REACTOR_HANDLE_TIMEOUT	50

#define NETSTACK_THREAD_STACK_SIZE		350

#define TRANSPORT_LAYER_FUNCS			CONCAT(TRANSPORT_LAYER, _funcs)
#define ROUTING_LAYER_FUNCS				CONCAT(ROUTING_LAYER, _funcs)
#define MAC_LAYER_FUNCS					CONCAT(MAC_LAYER, _funcs)
#define PHY_LAYER_FUNCS					CONCAT(PHY_LAYER, _funcs)

extern tp_layer_funcs_t TRANSPORT_LAYER_FUNCS;
extern rt_layer_funcs_t ROUTING_LAYER_FUNCS;
extern mac_layer_funcs_t MAC_LAYER_FUNCS;
extern phy_layer_funcs_t PHY_LAYER_FUNCS;


void netstackProc(void const * argument);
static retval_t net_commands_func(uint16_t argc, char** argv);
/* INIT DE INIT FUNCTIONS*/
/**
 *
 * @param netstack
 * @return
 */
retval_t netstack_init(void){

	if(netstack_state == NS_STACK_NO_INIT){
		netstack.netstack_data  = (netstack_data_t*) ytMalloc(sizeof(netstack_data_t));
		netstack.netstack_data->netstack_reactor = ytReactorCreate (NETSTACK_REACTOR_HANDLE_TIMEOUT);

		tx_packetbuffer_init();
		rx_packetbuffer_init();


		if((init_phy_layer(&PHY_LAYER_FUNCS)) != RET_OK){
			ytReactorDelete(netstack.netstack_data->netstack_reactor);
			ytFree(netstack.netstack_data);
			netstack.netstack_data = NULL;
			PRINTF(">ERROR: UNABLE TO INIT PHY LAYER\r\n");
			PRINTF(">NETSTACK ERROR\r\n");
			return RET_ERROR;
		}
		PRINTF(">PHY Layer Init\r\n");


		if((init_mac_layer(&MAC_LAYER_FUNCS)) != RET_OK){
			deinit_phy_layer();
			ytReactorDelete(netstack.netstack_data->netstack_reactor);
			ytFree(netstack.netstack_data);
			netstack.netstack_data = NULL;
			PRINTF(">ERROR: UNABLE TO INIT MAC LAYER\r\n");
			PRINTF(">NETSTACK ERROR\r\n");
			return RET_ERROR;
		}
		PRINTF(">MAC Layer Init\r\n");

		if((init_routing_layer(&ROUTING_LAYER_FUNCS)) != RET_OK){
			deinit_mac_layer();
			deinit_phy_layer();
			ytReactorDelete(netstack.netstack_data->netstack_reactor);
			ytFree(netstack.netstack_data);
			netstack.netstack_data = NULL;
			PRINTF(">ERROR: UNABLE TO INIT ROUTING LAYER\r\n");
			PRINTF(">NETSTACK ERROR\r\n");
			return RET_ERROR;
		}
		PRINTF(">ROUTING Layer Init\r\n");

		if((init_transport_layer(&TRANSPORT_LAYER_FUNCS)) != RET_OK){
			deinit_routing_layer();
			deinit_mac_layer();
			deinit_phy_layer();
			ytReactorDelete(netstack.netstack_data->netstack_reactor);
			ytFree(netstack.netstack_data);
			netstack.netstack_data = NULL;
			PRINTF(">ERROR: UNABLE TO INIT TRANSPORT LAYER\r\n");
			PRINTF(">NETSTACK ERROR\r\n");
			return RET_ERROR;
		}
		PRINTF(">TRANSPORT Layer Init\r\n");


		ytStartProcess("NETSTACK_PROCESS", netstackProc, NETSTACK_PROCESS_PRIORITY, NETSTACK_THREAD_STACK_SIZE, &(netstack.netstack_data->netstack_thread_id), &netstack);


		ytRegisterYetishellCommand("net", net_commands_func);	//Only register the command once, the next time it will return error as it is already registered
		PRINTF(">NETSTACK RUNNING\r\n");

		netstack_state = NS_STACK_INIT;
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}

/**
 *
 * @param netstack
 * @return
 */
retval_t netstack_deinit(void){
	if(netstack_state != NS_STACK_NO_INIT){
		if (deinit_transport_layer() != RET_OK){
			return RET_ERROR;
		}
		if(deinit_routing_layer() != RET_OK){
			return RET_ERROR;
		}
		if(deinit_mac_layer() != RET_OK){
			return RET_ERROR;
		}
		if(deinit_phy_layer() != RET_OK){
			return RET_ERROR;
		}

		tx_packetbuffer_deinit();
		rx_packetbuffer_deinit();
		ytReactorDelete(netstack.netstack_data->netstack_reactor);
		ytExitProcess(netstack.netstack_data->netstack_thread_id);

		ytFree(netstack.netstack_data);
		netstack.netstack_data = NULL;

		netstack_state = NS_STACK_NO_INIT;
		PRINTF(">NETSTACK DEINIT\r\n");
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
}
/* END INIT DE INIT FUNCTIONS*/



/* Functiones usadas solo internamente */
/**
 *
 * @param semph_id
 * @return
 */
retval_t netstack_op_done(uint32_t semph_id){
	if(ytSemaphoreRelease(semph_id) != RET_OK){
		return RET_ERROR;
	}
	return RET_OK;
}

/**
 *
 * @param tp_layer_funcs
 * @return
 */
retval_t netstack_set_tp_layer_funcs(tp_layer_funcs_t* tp_layer_funcs){
	netstack.tp_layer_funcs = tp_layer_funcs;
	return RET_OK;
}

/**
 *
 * @param rt_layer_funcs
 * @return
 */
retval_t netstack_set_rt_layer_funcs(rt_layer_funcs_t* rt_layer_funcs){
	netstack.rt_layer_funcs = rt_layer_funcs;
	return RET_OK;
}

/**
 *
 * @param mac_layer_funcs
 * @return
 */
retval_t netstack_set_mac_layer_funcs(mac_layer_funcs_t* mac_layer_funcs){
	netstack.mac_layer_funcs = mac_layer_funcs;
	return RET_OK;
}

/**
 *
 * @param phy_layer_funcs
 * @return
 */
retval_t netstack_set_phy_layer_funcs(struct phy_layer_funcs_* phy_layer_funcs){
	netstack.phy_layer_funcs = phy_layer_funcs;
	return RET_OK;
}
/* Funciones que pueden ser usadas por el desarrollador de capas*/
/**
 *
 * @param ev_handler
 * @param args
 * @return
 */
retval_t netstack_post_event(handle_event_func ev_handler_func, void* args){
	return ytPostEvent(netstack.netstack_data->netstack_reactor, ev_handler_func, args);
}

/**
 *
 * @return
 */
netstack_state_t get_netstack_state(void){
	return netstack_state;
}

/**
 *
 * @param priority
 * @return
 */
retval_t netstack_set_process_priority(process_class_t priority){
	if(netstack_state != NS_STACK_NO_INIT){
		return ytSetProcessPriority(netstack.netstack_data->netstack_thread_id, priority);
	}
	return RET_ERROR;
}

/**
 *
 * @return
 */
process_class_t netstack_get_process_priority(void){
	if(netstack_state != NS_STACK_NO_INIT){
		return ytGetProcessPriority(netstack.netstack_data->netstack_thread_id);
	}
	return VERY_LOW_PRIORITY_PROCESS;
}

retval_t net_commands_func(uint16_t argc, char** argv){
	if(argc<2){
		ytPrintf("Invalid 'net' command usage\r\n\r\n");
		goto CMD_INFO;
	}
	else{
		if(strcmp(argv[1], "-h") == 0){
			goto CMD_INFO;
		}
		else if(strcmp(argv[1], "start") == 0){
			if(netstack_state != NS_STACK_NO_INIT){
				ytPrintf("Netstack already initialized\r\n");
				return RET_ERROR;
			}
			else{
				if(netstack_init() == RET_OK){
					ytPrintf("Netstack successfully initialized\r\n");
				}
				else{
					ytPrintf("Unable to initialize netstack\r\n");
				}
			}
		}
		else if(strcmp(argv[1], "stop") == 0){
			if(netstack_state == NS_STACK_NO_INIT){
				ytPrintf("Netstack already stopped\r\n");
				return RET_ERROR;
			}
			else{
				if(netstack_deinit() == RET_OK){
					ytPrintf("Netstack succesfully stopped\r\n");
				}
				else{
					ytPrintf("Unable to stop netstack\r\n");
				}
			}
		}
		else if(strcmp(argv[1], "set_net_pri") == 0){
			if(argc != 3){
				ytPrintf("Wrong priority param\r\n");
				goto CMD_INFO;
				return RET_ERROR;
			}
			int32_t pri = (int32_t)atoi(argv[2]);
			process_class_t priority;
			if((pri<1) || (pri>5)){
				ytPrintf("Wrong priority param\r\n");
				goto CMD_INFO;
				return RET_ERROR;
			}
			switch(pri){
			case 1:
				priority = VERY_LOW_PRIORITY_PROCESS;
				break;
			case 2:
				priority = LOW_PRIORITY_PROCESS;
				break;
			case 3:
				priority = NORMAL_PRIORITY_PROCESS;
				break;
			case 4:
				priority = HIGH_PRIORITY_PROCESS;
				break;
			case 5:
				priority = VERY_HIGH_PRIORITY_PROCESS;
				break;
			default:
				return RET_ERROR;
			}

			netstack_set_process_priority(priority);
			ytPrintf("Netstack process priority set to %d\r\n", pri);
		}
		else if(strcmp(argv[1], "set_mac_pri") == 0){
			if(argc != 3){
				ytPrintf("Wrong priority param\r\n");
				goto CMD_INFO;
				return RET_ERROR;
			}
			int32_t pri = (int32_t)atoi(argv[2]);
			process_class_t priority;
			if((pri<1) || (pri>5)){
				ytPrintf("Wrong priority param\r\n");
				goto CMD_INFO;
				return RET_ERROR;
			}
			switch(pri){
			case 1:
				priority = VERY_LOW_PRIORITY_PROCESS;
				break;
			case 2:
				priority = LOW_PRIORITY_PROCESS;
				break;
			case 3:
				priority = NORMAL_PRIORITY_PROCESS;
				break;
			case 4:
				priority = HIGH_PRIORITY_PROCESS;
				break;
			case 5:
				priority = VERY_HIGH_PRIORITY_PROCESS;
				break;
			default:
				return RET_ERROR;
			}

			mac_set_process_priority(priority);
			ytPrintf("Mac process priority set to %d\r\n", pri);
		}
		else if(strcmp(argv[1], "set_phy_pri") == 0){
			if(argc != 3){
				ytPrintf("Wrong priority param\r\n");
				goto CMD_INFO;
				return RET_ERROR;
			}
			int32_t pri = (int32_t)atoi(argv[2]);
			process_class_t priority;
			if((pri<1) || (pri>5)){
				ytPrintf("Wrong priority param\r\n");
				goto CMD_INFO;
				return RET_ERROR;
			}
			switch(pri){
			case 1:
				priority = VERY_LOW_PRIORITY_PROCESS;
				break;
			case 2:
				priority = LOW_PRIORITY_PROCESS;
				break;
			case 3:
				priority = NORMAL_PRIORITY_PROCESS;
				break;
			case 4:
				priority = HIGH_PRIORITY_PROCESS;
				break;
			case 5:
				priority = VERY_HIGH_PRIORITY_PROCESS;
				break;
			default:
				return RET_ERROR;
			}

			phy_set_process_priority(priority);
			ytPrintf("Phy process priority set to %d\r\n", pri);
		}
		else if(strcmp(argv[1], "set_all_pri") == 0){
			if(argc != 3){
				ytPrintf("Wrong priority param\r\n");
				goto CMD_INFO;
				return RET_ERROR;
			}
			int32_t pri = (int32_t)atoi(argv[2]);
			process_class_t priority;
			if((pri<1) || (pri>5)){
				ytPrintf("Wrong priority param\r\n");
				goto CMD_INFO;
				return RET_ERROR;
			}
			switch(pri){
			case 1:
				priority = VERY_LOW_PRIORITY_PROCESS;
				break;
			case 2:
				priority = LOW_PRIORITY_PROCESS;
				break;
			case 3:
				priority = NORMAL_PRIORITY_PROCESS;
				break;
			case 4:
				priority = HIGH_PRIORITY_PROCESS;
				break;
			case 5:
				priority = VERY_HIGH_PRIORITY_PROCESS;
				break;
			default:
				return RET_ERROR;
			}

			netstack_set_process_priority(priority);
			mac_set_process_priority(priority);
			phy_set_process_priority(priority);

			ytPrintf("Net processes priorities set to %d\r\n", pri);

		}
		else if(strcmp(argv[1], "set_mac_dc") == 0){
			if(argc != 3){
				ytPrintf("Wrong duty cycle param\r\n");
				goto CMD_INFO;
				return RET_ERROR;
			}
			uint32_t dc = (uint32_t)atoi(argv[2]);
			if((dc<2) || (dc>35)){
				ytPrintf("Wrong Duty cycle param\r\n");
				goto CMD_INFO;
				return RET_ERROR;
			}
			mac_set_duty_cycle(dc);

			ytPrintf("MAC Duty Cycle set to %d\r\n", dc);

		}
		else if(strcmp(argv[1], "get_net_pri") == 0){
			process_class_t priority = netstack_get_process_priority();
			int32_t pri;
			switch(priority){
			case VERY_LOW_PRIORITY_PROCESS:
				pri = 1;
				break;
			case LOW_PRIORITY_PROCESS:
				pri = 2;
				break;
			case NORMAL_PRIORITY_PROCESS:
				pri = 3;
				break;
			case HIGH_PRIORITY_PROCESS:
				pri = 4;
				break;
			case VERY_HIGH_PRIORITY_PROCESS:
				pri = 5;
				break;
			default:
				return RET_ERROR;
			}

			ytPrintf("Net Process Priority: %d\r\n", pri);
		}
		else if(strcmp(argv[1], "get_mac_pri") == 0){
			process_class_t priority = mac_get_process_priority();
			int32_t pri;
			switch(priority){
			case VERY_LOW_PRIORITY_PROCESS:
				pri = 1;
				break;
			case LOW_PRIORITY_PROCESS:
				pri = 2;
				break;
			case NORMAL_PRIORITY_PROCESS:
				pri = 3;
				break;
			case HIGH_PRIORITY_PROCESS:
				pri = 4;
				break;
			case VERY_HIGH_PRIORITY_PROCESS:
				pri = 5;
				break;
			default:
				return RET_ERROR;
			}

			ytPrintf("Mac Process Priority: %d\r\n", pri);
		}
		else if(strcmp(argv[1], "get_phy_pri") == 0){
			process_class_t priority = netstack_get_process_priority();
			int32_t pri;
			switch(priority){
			case VERY_LOW_PRIORITY_PROCESS:
				pri = 1;
				break;
			case LOW_PRIORITY_PROCESS:
				pri = 2;
				break;
			case NORMAL_PRIORITY_PROCESS:
				pri = 3;
				break;
			case HIGH_PRIORITY_PROCESS:
				pri = 4;
				break;
			case VERY_HIGH_PRIORITY_PROCESS:
				pri = 5;
				break;
			default:
				return RET_ERROR;
			}

			ytPrintf("Phy Process Priority: %d\r\n", pri);
		}
		else{

CMD_INFO:
			ytPrintf("Usage:\t net [command] [param]\r\n");
			ytPrintf("Available commands:\r\n");
			ytPrintf("\t-h: displays this info\r\n");
			ytPrintf("\tstart: initializes the netstack\r\n");
			ytPrintf("\tstop: stops the netstack\r\n");
			ytPrintf("\tset_net_pri: sets netstack proccess priority. Param: Allowed values 1-5. (1 is the lowest priority)\r\n");
			ytPrintf("\tset_mac_pri: sets mac layer proccess priority. Param: Allowed values 1-5. (1 is the lowest priority)\r\n");
			ytPrintf("\tset_phy_pri: sets phy layer proccess priority. Param: Allowed values 1-5. (1 is the lowest priority)\r\n");
			ytPrintf("\tset_all_pri: sets all net proccesses priorities. Param: Allowed values 1-5. (1 is the lowest priority)\r\n");
			ytPrintf("\tget_net_pri: returns netstack proccess priority\r\n");
			ytPrintf("\tget_mac_pri: returns mac layer proccess priority\r\n");
			ytPrintf("\tget_phy_pri: returns phy layer proccess priority\r\n");
			ytPrintf("\r\n");
			ytPrintf("Example: net set_mac_pri 2\r\n");
			ytPrintf("\tSets mac layer process priority to 2\r\n\r\n");
			return RET_ERROR;
		}
		return RET_OK;
	}

}
#endif
