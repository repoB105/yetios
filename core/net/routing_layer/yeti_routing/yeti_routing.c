/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * yeti_routing.c
 *
 *  Created on: 11 de may. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file yeti_routing.c
 */


#include "routing_layer.h"
#include "mac_layer.h"
#include "transport_layer.h"
#include "yeti_routing.h"
#include "packetbuffer.h"
#include <stdlib.h>
#include <stdio.h>

#if ENABLE_NETSTACK_ARCH

#define BC_DEST_ADDR			0xFFFF
#define DEFAULT_NODE_ADDR		0x0000

#define TIMER_DISC_PERIOD		1000		//Routing layer timer timeout for periodic packet sending

#define NB_DISC_PERIOD			7000		//A neighbour discovery packet will be sent each 5000 ms
#define NB_DISC_FIRST_PERIOD	1000		//The time the first neighbour discovery packet will be sent

#define RT_DISC_PERIOD			15000		//A routing discovery packet will be sent each 15000 ms
#define RT_DISC_FIRST_PERIOD	4000		//The time the first routing discovery packet will be sent


typedef struct __packed yeti_net_addr_{
	uint16_t b_net_addr;
}yeti_net_addr_t;

typedef struct yeti_route_{
	yeti_net_addr_t dest_addr;
	yeti_net_addr_t next_addr;
	uint16_t num_hops;
}yeti_route_t;


typedef struct neighbour_data_{
	mac_addr_t mac_addr;
	yeti_net_addr_t net_addr;
}neighbour_data_t;

typedef struct yeti_rt_layer_data_{
	gen_list* route_list;
	yeti_net_addr_t node_addr;
	uint32_t disc_timer;
	gen_list* neighbour_table;
	uint8_t resending_flag;
	uint8_t nb_disc_time;
	uint8_t rt_disc_time;
}yeti_rt_layer_data_t;


yeti_rt_layer_data_t* yeti_rt_data = NULL;

#define PACKET_TYPE_RT_DEFAULT	0x00
#define PACKET_TYPE_NB_DISC		0x01
#define PACKET_TYPE_RT_DISC		0x02


static retval_t yeti_rt_layer_init();
static retval_t yeti_rt_layer_deinit();

static retval_t yeti_rt_send_packet(net_addr_t addr_fd, net_packet_t* packet);
static retval_t yeti_rt_rcv_packet();

static retval_t yeti_rt_send_packet_done(net_packet_t* packet);
static retval_t yeti_rt_rcv_packet_done(net_packet_t* packet, mac_addr_t mac_from_addr);

static retval_t yeti_rt_get_node_addr(uint16_t index, net_addr_t* node_addr);
static retval_t yeti_rt_add_node_addr(net_addr_t new_addr_fd);
static retval_t yeti_rt_remove_node_addr(net_addr_t addr_fd);
static retval_t yeti_rt_add_node_route(net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops);
static retval_t yeti_rt_remove_node_route(net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops);

static retval_t yeti_rt_new_net_addr(char* net_str_val, char format, net_addr_t* net_addr_fd);
static retval_t yeti_rt_delete_net_addr(net_addr_t net_addr_fd);
static retval_t yeti_rt_net_addr_to_string(net_addr_t net_addr_fd, char* net_str_val, char format);
static retval_t yeti_rt_net_addr_cpy(net_addr_t dest_addr_fd, net_addr_t from_addr_fd);
static retval_t yeti_rt_net_addr_cmp(net_addr_t a_addr_fd, net_addr_t b_addr_fd);

static retval_t yeti_rt_set_node_as_gw();

/* PRIVATE FUNCS */
static retval_t find_dest_addr_route(yeti_net_addr_t* dest_addr, yeti_net_addr_t* next_addr);
static retval_t yeti_add_neighbour(uint16_t from_net_addr, mac_addr_t mac_from_addr);
static uint16_t check_existing_mac_in_nb_table(mac_addr_t mac_addr);
static uint16_t check_existing_net_addr_in_nb_table(uint16_t net_addr);
static mac_addr_t get_neighbour_mac_addr_from_net(yeti_net_addr_t next_addr);

/* TIMER CALLBACKS*/
static void timer_disc_func(void const * argument);

rt_layer_funcs_t yeti_routing_funcs = {
		.rt_layer_init = yeti_rt_layer_init,
		.rt_layer_deinit = yeti_rt_layer_deinit,
		.rt_send_packet = yeti_rt_send_packet,
		.rt_rcv_packet = yeti_rt_rcv_packet,
		.rt_send_packet_done = yeti_rt_send_packet_done,
		.rt_rcv_packet_done = yeti_rt_rcv_packet_done,
		.rt_add_node_addr = yeti_rt_add_node_addr,
		.rt_remove_node_addr = yeti_rt_remove_node_addr,
		.rt_add_node_route = yeti_rt_add_node_route,
		.rt_remove_node_route = yeti_rt_remove_node_route,
		.rt_new_net_addr = yeti_rt_new_net_addr,
		.rt_delete_net_addr = yeti_rt_delete_net_addr,
		.rt_net_addr_to_string = yeti_rt_net_addr_to_string,
		.rt_net_addr_cpy = yeti_rt_net_addr_cpy,
		.rt_net_addr_cmp = yeti_rt_net_addr_cmp,
		.rt_get_node_addr = yeti_rt_get_node_addr,
		.rt_set_node_as_gw = yeti_rt_set_node_as_gw,
};

/**
 *
 * @param rt_layer_data
 * @return
 */
static retval_t yeti_rt_layer_init(){
	if(yeti_rt_data != NULL){
		return RET_ERROR;
	}

	yeti_rt_data = (yeti_rt_layer_data_t*) ytMalloc(sizeof(yeti_rt_layer_data_t));

	yeti_rt_data->route_list = gen_list_init();
	yeti_rt_data->neighbour_table = gen_list_init();
	yeti_rt_data->resending_flag = 0;
	yeti_rt_data->node_addr.b_net_addr = DEFAULT_NODE_ADDR;
	yeti_rt_data->nb_disc_time = ((NB_DISC_PERIOD-NB_DISC_FIRST_PERIOD)/TIMER_DISC_PERIOD);
	yeti_rt_data->rt_disc_time = ((RT_DISC_PERIOD-RT_DISC_FIRST_PERIOD)/TIMER_DISC_PERIOD);
	yeti_rt_data->disc_timer = ytTimerCreate(ytTimerPeriodic, timer_disc_func, NULL);
	mac_read_packet();	//Todos los nodos son routers asi que est�n siempre escuchando para poder rutar paquetes
	mac_read_packet();	//Duplico el numero de paquetes esperados. As� si se da la m�s que improbable situacion de recibir dos paquetes
									//seguidos, seguimos en modo reccepcion para poder rutar paquetes
	ytTimerStart(yeti_rt_data->disc_timer, TIMER_DISC_PERIOD);

	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @return
 */
static retval_t yeti_rt_layer_deinit(){

	ytTimerStop(yeti_rt_data->disc_timer);
	ytTimerDelete(yeti_rt_data->disc_timer);
	gen_list_remove_and_delete_all(yeti_rt_data->route_list);
	gen_list_remove_and_delete_all(yeti_rt_data->neighbour_table);
	ytFree(yeti_rt_data);
	yeti_rt_data = NULL;
	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @param addr_fd
 * @param packet
 * @return
 */
static retval_t yeti_rt_send_packet(net_addr_t addr_fd, net_packet_t* packet){
	yeti_net_addr_t* dest_addr = (yeti_net_addr_t*) addr_fd;
	yeti_net_addr_t next_addr;
	mac_addr_t dest_mac_addr;

	if(!mac_is_node_linked()){
		return RET_ERROR;
	}
	if(find_dest_addr_route(dest_addr, &next_addr) != RET_OK){	//No encontrado en la tabla de rutas el objetivo. En tal caso no envio el paquete
		return RET_ERROR;
	}
	else{	//He encontrado el next hop en la tabla de rutas y guardado dentro su valor

		if(!yeti_rt_data->resending_flag){	//Si no estoy reenviando pongo mi direccion como source y la que se pasa como destino
			packet->header.rt_hdr.src_addr = yeti_rt_data->node_addr.b_net_addr;
			packet->header.rt_hdr.dest_addr = dest_addr->b_net_addr;
		}
		else{	//Si reenvio ya est� la direccion de source puesta en el paquete que se ha copiado
			yeti_rt_data->resending_flag = 0;
		}
		packet->header.rt_hdr.rt_packet_type = PACKET_TYPE_RT_DEFAULT;

		if((dest_mac_addr = get_neighbour_mac_addr_from_net(next_addr)) == NULL){
			return RET_ERROR;
		}

		return mac_send_packet(dest_mac_addr, packet);

	}

	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @return
 */
static retval_t yeti_rt_rcv_packet(){	//Me pongo a escuchar
	if(!mac_is_node_linked()){
		return RET_ERROR;
	}
	mac_read_packet();
	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @param packet
 * @return
 */
static retval_t yeti_rt_send_packet_done(net_packet_t* packet){


	if(packet->header.rt_hdr.rt_packet_type  == PACKET_TYPE_RT_DEFAULT){
		if (packet->header.rt_hdr.src_addr == yeti_rt_data->node_addr.b_net_addr){	//I am the originator
			return tp_packet_sent(packet);
		}
		else{
			return tx_packetbuffer_release_packet(packet);
		}
	}
	else{
		return tx_packetbuffer_release_packet(packet);
	}

	return RET_ERROR;

}

/**
 *
 * @param rt_layer_data
 * @param packet
 * @param mac_from_addr
 * @return
 */
static retval_t yeti_rt_rcv_packet_done(net_packet_t* packet, mac_addr_t mac_from_addr){
	net_packet_t* re_send_packet;
	yeti_net_addr_t resend_dest_addr, next_addr;
	yeti_net_addr_t* net_from_addr;

	if(packet->header.rt_hdr.rt_packet_type  == PACKET_TYPE_RT_DEFAULT){
//		mac_delete_addr(mac_from_addr);//Elimino la direccion, ya que no llega mas arriba del stack

		if((yeti_rt_data->node_addr.b_net_addr == packet->header.rt_hdr.dest_addr) || (packet->header.rt_hdr.dest_addr == BC_DEST_ADDR)){	//Soy el destinatario

			net_from_addr = (yeti_net_addr_t*) ytMalloc(sizeof(yeti_net_addr_t));
			net_from_addr->b_net_addr = packet->header.rt_hdr.src_addr;
			tp_packet_received(packet, (net_addr_t) net_from_addr);

		}
		else{	//No soy el destinatario. Reenvio
			resend_dest_addr.b_net_addr = packet->header.rt_hdr.dest_addr;

			if((re_send_packet = tx_packetbuffer_get_free_packet())!= NULL){	//Capturo el primer paquete sin usar
				memcpy(re_send_packet, packet, FIXED_PACKET_SIZE);						//Copio el paquete

				mac_read_packet();	//Todos los nodos son routers asi que est�n siempre escuchando para poder rutar paquetes
				yeti_rt_data->resending_flag = 1;
				if(yeti_rt_send_packet((net_addr_t) &resend_dest_addr, re_send_packet) != RET_OK){	//Reenvio
					tx_packetbuffer_release_packet(re_send_packet);
				}
			}
//			rx_packetbuffer_release_packet(packet);
		}
	}

	else if(packet->header.rt_hdr.rt_packet_type  == PACKET_TYPE_NB_DISC){

		if(yeti_add_neighbour(packet->header.rt_hdr.src_addr, mac_from_addr) != RET_OK){
//			mac_delete_addr(mac_from_addr);//Elimino la direccion, ya que no llega mas arriba del stack
//			rx_packetbuffer_release_packet(packet);
			return RET_ERROR;
		}
		//If it is a neighbour add also the direct route to this node
		resend_dest_addr.b_net_addr = packet->header.rt_hdr.src_addr;
		yeti_rt_add_node_route((net_addr_t) &resend_dest_addr, (net_addr_t) &resend_dest_addr, 0);

//		rx_packetbuffer_release_packet(packet);
	}

	else if(packet->header.rt_hdr.rt_packet_type == PACKET_TYPE_RT_DISC){
//		mac_delete_addr(mac_from_addr);//Elimino la direccion, ya que no llega mas arriba del stack

		resend_dest_addr.b_net_addr = packet->header.rt_hdr.src_addr;
		memcpy(&(next_addr.b_net_addr), &packet->data[0], sizeof(uint16_t));

		yeti_rt_add_node_route((net_addr_t) &resend_dest_addr, (net_addr_t) &next_addr, 0);

		memcpy(&packet->data[0], &(yeti_rt_data->node_addr.b_net_addr),sizeof(uint16_t));

//		rx_packetbuffer_release_packet(packet);
	}
	else{
//		mac_delete_addr(mac_from_addr);
//		rx_packetbuffer_release_packet(packet);
		return RET_ERROR;
	}



	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @param index
 * @param node_addr
 * @return
 */
static retval_t yeti_rt_get_node_addr(uint16_t index, net_addr_t* node_addr){
	yeti_net_addr_t* yeti_node_addr;
	if(index != 0){
		return RET_ERROR;
	}
	(*node_addr) = (net_addr_t) mac_new_addr(NULL, 0);
	yeti_node_addr = (yeti_net_addr_t*) (*node_addr);
	yeti_node_addr->b_net_addr = yeti_rt_data->node_addr.b_net_addr;
	return RET_OK;

}
/**
 *
 * @param rt_layer_data
 * @param new_addr_fd
 * @return
 */
static retval_t yeti_rt_add_node_addr(net_addr_t new_addr_fd){	//Overwrites node addr
	yeti_net_addr_t* node_addr = (yeti_net_addr_t*) new_addr_fd;
	yeti_rt_data->node_addr.b_net_addr = node_addr->b_net_addr;
	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @param addr_fd
 * @return
 */
static retval_t yeti_rt_remove_node_addr(net_addr_t addr_fd){	//Only one node addr. Imposible to delete
	return RET_ERROR;
}
/**
 *
 * @param rt_layer_data
 * @param dest_addr_fd
 * @param next_addr_fd
 * @param hops
 * @return
 */
static retval_t yeti_rt_add_node_route(net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops){

	gen_list* current = yeti_rt_data->route_list;
	yeti_route_t* route;
	while(current->next != NULL){
		route = (yeti_route_t*) current->next->item;
		if(route->dest_addr.b_net_addr == ((yeti_net_addr_t*)dest_addr_fd)->b_net_addr){	//The destination address route already exists
			return RET_ERROR;
		}
		current = current->next;
	}
	yeti_route_t* new_route = (yeti_route_t*) ytMalloc(sizeof(yeti_route_t));
	new_route->dest_addr = *((yeti_net_addr_t*)dest_addr_fd);
	new_route->next_addr = *((yeti_net_addr_t*)next_addr_fd);
	new_route->num_hops = hops;
	gen_list_add(yeti_rt_data->route_list, new_route);
	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @param dest_addr_fd
 * @param next_addr_fd
 * @param hops
 * @return
 */
static retval_t yeti_rt_remove_node_route(net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops){
	yeti_route_t* route;
	yeti_net_addr_t* dest_addr = (yeti_net_addr_t*) dest_addr_fd;
	yeti_net_addr_t* next_addr = (yeti_net_addr_t*) next_addr_fd;
	gen_list* current = yeti_rt_data->route_list;

	while(current->next != NULL){
		route = (yeti_route_t*) current->next->item;
		if(route->dest_addr.b_net_addr == dest_addr->b_net_addr){
			if(route->next_addr.b_net_addr == next_addr->b_net_addr){
				if(route->num_hops == hops){
					gen_list* next = current->next->next;
					vPortFree(current->next);
					ytFree(route);
					current->next = next;
				}
				else{
					current = current->next;
				}
			}
			else{
				current = current->next;
			}
		}
		else{
			current = current->next;
		}

	}


	return RET_OK;
}
/**
 *
 * @param rt_layer_data
 * @param net_str_val
 * @param net_addr_fd
 * @return
 */
static retval_t yeti_rt_new_net_addr(char* net_str_val, char format, net_addr_t* net_addr_fd){
	if((net_addr_fd == NULL)){
		return RET_ERROR;
	}

	yeti_net_addr_t* yeti_net_addr;
	uint32_t addr_val;
	(*net_addr_fd) = NULL;
	if(net_addr_fd == NULL){
		return RET_ERROR;
	}
	if(format == 0){	//Empty new address
		yeti_net_addr = (yeti_net_addr_t*) ytMalloc(sizeof(yeti_net_addr_t));
		yeti_net_addr->b_net_addr = 0x0000;
		(*net_addr_fd) = (net_addr_t) yeti_net_addr;
		return RET_OK;
	}

	else{
		if(net_str_val == NULL){
			return RET_ERROR;
		}
		else if((format == 'D') || (format == 'd')){
			if(strlen(net_str_val)>5){	//Max addr value 65535
				return RET_ERROR;
			}
			addr_val = strtoul(net_str_val, NULL, 10);

			yeti_net_addr = (yeti_net_addr_t*) ytMalloc(sizeof(yeti_net_addr_t));
			yeti_net_addr->b_net_addr = (uint16_t)(addr_val & 0x0000FFFF);
			(*net_addr_fd) = (net_addr_t) yeti_net_addr;

		}
		else if((format == 'H') || (format == 'h')){
			if(strlen(net_str_val)>6){	//Max addr value 0xFFFF
				return RET_ERROR;
			}
			if((net_str_val[0] != '0') || (net_str_val[1] != 'x')){
				return RET_ERROR;
			}
			addr_val = strtoul(net_str_val+2, NULL, 16);

			yeti_net_addr = (yeti_net_addr_t*) ytMalloc(sizeof(yeti_net_addr_t));
			yeti_net_addr->b_net_addr = (uint16_t)(addr_val & 0x0000FFFF);
			(*net_addr_fd) = (mac_addr_t) yeti_net_addr;
		}
		else{
			return RET_ERROR;
		}

	}
	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @param net_addr_fd
 * @return
 */
static retval_t yeti_rt_delete_net_addr(net_addr_t net_addr_fd){
	if(net_addr_fd == NULL){
		return RET_ERROR;
	}

	ytFree(net_addr_fd);
	return RET_OK;

}

/**
 *
 * @param rt_layer_data
 * @param net_addr_fd
 * @param net_str_val
 * @return
 */
static retval_t yeti_rt_net_addr_to_string(net_addr_t net_addr_fd, char* net_str_val, char format){
	yeti_net_addr_t* yeti_net_addr = (yeti_net_addr_t*) net_addr_fd;
	if((net_addr_fd == NULL) || (net_str_val == NULL)){
		return RET_ERROR;
	}
	if((format == 'D') || (format == 'd')){
		sprintf(net_str_val, "%ul", (unsigned int) yeti_net_addr->b_net_addr);
	}
	else if((format == 'H') || (format == 'h')){
		sprintf(net_str_val, "0x%04X",(unsigned int) yeti_net_addr->b_net_addr);
	}
	else{
		net_str_val[0] = 'N';
		net_str_val[1] = 'a';
		net_str_val[2] = 'N';
		return RET_ERROR;
	}

	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @param dest_addr_fd
 * @param from_addr_fd
 * @return
 */
static retval_t yeti_rt_net_addr_cpy(net_addr_t dest_addr_fd, net_addr_t from_addr_fd){
	yeti_net_addr_t* yeti_net_addr_dest = (yeti_net_addr_t*) dest_addr_fd;
	yeti_net_addr_t* yeti_net_addr_from = (yeti_net_addr_t*) from_addr_fd;
	if((dest_addr_fd == NULL) || (from_addr_fd == NULL)){
		return RET_ERROR;
	}
	yeti_net_addr_dest->b_net_addr = yeti_net_addr_from->b_net_addr;
	return RET_OK;

	return mac_addr_cpy((mac_addr_t) dest_addr_fd, (mac_addr_t) from_addr_fd);
}

/**
 *
 * @param rt_layer_data
 * @param a_addr_fd
 * @param b_addr_fd
 * @return
 */
static retval_t yeti_rt_net_addr_cmp(net_addr_t a_addr_fd, net_addr_t b_addr_fd){
	yeti_net_addr_t* yeti_net_addr_a = (yeti_net_addr_t*) a_addr_fd;
	yeti_net_addr_t* yeti_net_addr_b = (yeti_net_addr_t*) b_addr_fd;
	if((a_addr_fd == NULL) || (b_addr_fd == NULL)){
		return RET_ERROR;
	}

	if(yeti_net_addr_a->b_net_addr == yeti_net_addr_b->b_net_addr){
		return RET_OK;
	}
	return RET_ERROR;

}

/**
 *
 * @param rt_layer_data
 * @return
 */
static retval_t yeti_rt_set_node_as_gw(){
	mac_set_node_as_gw();

	return RET_OK;
}

/**
 *
 * @param rt_layer_data
 * @param dest_addr
 * @param next_addr
 * @return
 */
static retval_t find_dest_addr_route(yeti_net_addr_t* dest_addr, yeti_net_addr_t* next_addr){
	yeti_route_t* route;
	gen_list* current = yeti_rt_data->route_list;

	if(dest_addr->b_net_addr == BC_DEST_ADDR){
		next_addr->b_net_addr = BC_DEST_ADDR;
		return RET_OK;
	}

	while(current->next != NULL){
		route = (yeti_route_t*) current->next->item;
		if(route->dest_addr.b_net_addr == dest_addr->b_net_addr){

			next_addr->b_net_addr = route->next_addr.b_net_addr;
			return RET_OK;

		}
		else{
			current = current->next;
		}

	}

	return RET_ERROR;
}

/**
 *
 * @param rt_layer_data
 * @param packet
 * @return
 */
static retval_t yeti_add_neighbour(uint16_t from_net_addr, mac_addr_t mac_from_addr){
	neighbour_data_t* new_neighbour;
	mac_addr_t new_mac_addr;
	if(from_net_addr == 0){	//Source net address is 0 is not a valid address and cannot be asigned to a neighbour
		return RET_ERROR;
	}

	if(check_existing_mac_in_nb_table(mac_from_addr)){
		return RET_ERROR;
	}
	if(check_existing_net_addr_in_nb_table(from_net_addr)){
		return RET_ERROR;
	}
	new_neighbour = (neighbour_data_t*) ytMalloc(sizeof(neighbour_data_t));
	new_mac_addr = mac_new_addr(DEFAULT_MAC_BC_ADDR_STR, 'H');
	mac_addr_cpy(new_mac_addr, mac_from_addr);
	new_neighbour->mac_addr = new_mac_addr;
	new_neighbour->net_addr.b_net_addr = from_net_addr;

	gen_list_add(yeti_rt_data->neighbour_table, (void*) new_neighbour);

	return RET_OK;
}


/**
 *
 * @param layer_data
 * @param mac_addr
 * @return
 */
static uint16_t check_existing_mac_in_nb_table(mac_addr_t mac_addr){
	gen_list* current = yeti_rt_data->neighbour_table;
	neighbour_data_t* nb_data;
	while(current->next != NULL){
		nb_data = (neighbour_data_t*) current->next->item;
		if(mac_addr_cmp(mac_addr, nb_data->mac_addr)){
			return 1;
		}
		current = current->next;
	}
	return 0;
}

/**
 *
 * @param layer_data
 * @param net_addr
 * @return
 */
static uint16_t check_existing_net_addr_in_nb_table(uint16_t net_addr){
	gen_list* current = yeti_rt_data->neighbour_table;
	neighbour_data_t* nb_data;
	while(current->next != NULL){
		nb_data = (neighbour_data_t*) current->next->item;
		if(net_addr == nb_data->net_addr.b_net_addr){
			return 1;
		}
		current = current->next;
	}
	return 0;

}

/**
 *
 * @param layer_data
 * @param net_addr
 * @return
 */
static mac_addr_t get_neighbour_mac_addr_from_net(yeti_net_addr_t net_addr){
	gen_list* current = yeti_rt_data->neighbour_table;
	neighbour_data_t* nb_data;
	while(current->next != NULL){
		nb_data = (neighbour_data_t*) current->next->item;
		if(net_addr.b_net_addr == nb_data->net_addr.b_net_addr){
			return nb_data->mac_addr;
		}
		current = current->next;
	}
	return NULL;
}

/**
 *
 * @param argument
 */
static void timer_disc_func(void const * argument){
	net_packet_t* packet;
	mac_addr_t mac_addr;
	yeti_rt_data->nb_disc_time++;
	if(yeti_rt_data->nb_disc_time * TIMER_DISC_PERIOD >= NB_DISC_PERIOD){
		yeti_rt_data->nb_disc_time = 0;

		if((packet = tx_packetbuffer_get_free_packet()) == NULL){				//SEND NB_DISCOVERY_PACKET
			return;
		}
		if((mac_addr = mac_new_addr(DEFAULT_MAC_BC_ADDR_STR, 'H')) == NULL){
			tx_packetbuffer_release_packet(packet);
			return;
		}
		packet->header.rt_hdr.src_addr = yeti_rt_data->node_addr.b_net_addr;
		packet->header.rt_hdr.dest_addr = BC_DEST_ADDR;
		packet->header.rt_hdr.rt_packet_type  = (uint8_t) PACKET_TYPE_NB_DISC;
		if(mac_send_packet(mac_addr, packet) != RET_OK){
			tx_packetbuffer_release_packet(packet);
		}
		mac_delete_addr(mac_addr);

	}

	yeti_rt_data->rt_disc_time++;
	if(yeti_rt_data->rt_disc_time * TIMER_DISC_PERIOD >= RT_DISC_PERIOD){
		yeti_rt_data->rt_disc_time = 0;

		if((packet = tx_packetbuffer_get_free_packet()) == NULL){				//SEND RT_DISCOVERY_PACKET
			return;
		}
		if((mac_addr = mac_new_addr(DEFAULT_MAC_FLOOD_ADDR_STR, 'H')) == NULL){
			tx_packetbuffer_release_packet(packet);
			return;
		}
		packet->header.rt_hdr.src_addr = yeti_rt_data->node_addr.b_net_addr;
		packet->header.rt_hdr.dest_addr = BC_DEST_ADDR;
		packet->header.rt_hdr.rt_packet_type  = (uint8_t) PACKET_TYPE_RT_DISC;

		memcpy(&packet->data[0], &(yeti_rt_data->node_addr.b_net_addr), sizeof(uint16_t));

		if(mac_send_packet(mac_addr, packet) != RET_OK){
			tx_packetbuffer_release_packet(packet);
		}
		mac_delete_addr(mac_addr);

	}
}

#endif
