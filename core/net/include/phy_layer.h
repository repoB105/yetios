/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * phy_layer.h
 *
 *  Created on: 22 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file phy_layer.h
 */
#ifndef APPLICATION_CORE_NET_INCLUDE_PHY_LAYER_H_
#define APPLICATION_CORE_NET_INCLUDE_PHY_LAYER_H_


#include "system_api.h"
#include "packetbuffer.h"
#include "netstack.h"


typedef struct phy_layer_funcs_{
	retval_t (*phy_layer_init)();
	retval_t (*phy_layer_deinit)();

	retval_t (*phy_send_packet)(net_packet_t* packet, uint16_t size);

	retval_t (*phy_send_packet_done)(net_packet_t* packet, uint16_t size);
	retval_t (*phy_rcv_packet_done)(net_packet_t* packet, uint16_t size);

//	retval_t (*phy_set_pckt_length)(uint16_t pckt_length);

	//Funciones llamadas inmediatamente. No son posteadas en modo evento. Se puede hacer as� ya que solo deber�an ser llamadas desde la capa MAC
	retval_t (*phy_set_mode_receiving)();
	retval_t (*phy_set_mode_idle)();
	retval_t (*phy_set_mode_sleep)();

	retval_t (*phy_check_channel_rssi)(float32_t* read_rssi);
	retval_t (*phy_get_last_rssi)(float32_t* read_rssi);

	retval_t (*phy_set_base_freq)(uint32_t base_freq);
	retval_t (*phy_get_base_freq)(uint32_t* base_freq);

	retval_t (*phy_get_channel_num)(uint32_t* channel_num);
	retval_t (*phy_set_freq_channel)(uint32_t channel_num);
	retval_t (*phy_get_freq_channel)(uint32_t* channel_num);	//Get the current freq channel

	retval_t (*phy_set_baud_rate)(uint32_t baud_rate);
	retval_t (*phy_set_out_power)(int16_t out_power);

	retval_t (*phy_encrypt_packet)(net_packet_t* packet, uint8_t* key, uint16_t size);
	retval_t (*phy_decrypt_packet)(net_packet_t* packet, uint8_t* key, uint16_t size);

	retval_t (*phy_set_process_priority)(process_class_t priority);
	process_class_t (*phy_get_process_priority)(void);
}phy_layer_funcs_t;

typedef struct phy_layer_func_args_{
	net_packet_t* packet;
	uint16_t size;
}phy_layer_func_args_t;


retval_t init_phy_layer(phy_layer_funcs_t* phy_layer_funcs);
retval_t deinit_phy_layer(void);

#if ENABLE_NETSTACK_ARCH
//* Called from mac layer
NET_INLINE retval_t phy_send_packet(net_packet_t* packet, uint16_t size);

//* Called from phy layer
NET_INLINE retval_t phy_post_event_packet_sent(net_packet_t* packet, uint16_t size);
NET_INLINE retval_t phy_post_event_packet_received(net_packet_t* packet, uint16_t size);

//NET_INLINE retval_t phy_set_pckt_length(uint16_t pckt_length);

//* Called from mac layer. Inmediate functions: No event posted
NET_INLINE retval_t phy_set_mode_receiving(void);
NET_INLINE retval_t phy_set_mode_idle(void);
NET_INLINE retval_t phy_set_mode_sleep(void);

NET_INLINE float32_t phy_check_channel_rssi(void);
NET_INLINE float32_t phy_get_last_rssi(void);

NET_INLINE retval_t phy_set_base_freq(uint32_t base_freq);
NET_INLINE uint32_t phy_get_base_freq(void);

NET_INLINE uint32_t phy_get_channel_num(void);
NET_INLINE retval_t phy_set_freq_channel(uint32_t channel_num);
NET_INLINE uint32_t phy_get_freq_channel(void);

NET_INLINE retval_t phy_set_baud_rate(uint32_t baud_rate);
NET_INLINE retval_t phy_set_out_power(int16_t out_power);

NET_INLINE retval_t phy_encrypt_packet(net_packet_t* packet, uint8_t* key, uint16_t size);
NET_INLINE retval_t phy_decrypt_packet(net_packet_t* packet, uint8_t* key, uint16_t size);

NET_INLINE retval_t phy_set_process_priority(process_class_t priority);
NET_INLINE process_class_t phy_get_process_priority(void);
#endif

retval_t packet_sent_event(void* args);
retval_t packet_received_event(void* args);

#include "phy_layer_def.h"

#endif /* APPLICATION_CORE_NET_INCLUDE_PHY_LAYER_H_ */
