/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * mac_layer_def.h
 *
 *  Created on: 11 sept. 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file mac_layer_def.h
 */
#ifndef YETIOS_CORE_NET_INCLUDE_MAC_LAYER_DEF_H_
#define YETIOS_CORE_NET_INCLUDE_MAC_LAYER_DEF_H_

#if ENABLE_NETSTACK_ARCH


#ifdef DEBUG
#undef DEBUG
#undef PRINTF
#endif
#define DEBUG 0
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif


//* Called from Routing layer
/**
 *
 * @param dest_mac_addr_fd
 * @param packet
 * @return
 */
NET_INLINE retval_t mac_send_packet(mac_addr_t dest_mac_addr_fd, net_packet_t* packet){
	netstack_state = NS_MAC_SEND_PACKET;
	PRINTF("MAC EV SEND\r\n");
	return netstack.mac_layer_funcs->mac_send_packet(dest_mac_addr_fd, packet);
}
/**
 *
 * @return
 */
NET_INLINE retval_t mac_read_packet(void){
	netstack_state = NS_MAC_RCV_PACKET;
	PRINTF("MAC EV READ\r\n");
	return netstack.mac_layer_funcs->mac_rcv_packet();
}

//* Called from phy layer
/**
 *
 * @param packet
 * @return
 */
NET_INLINE retval_t mac_packet_sent(net_packet_t* packet, uint16_t size){
	netstack_state = NS_MAC_PACKET_SENT;
	PRINTF("MAC EV SENT\r\n");
	return netstack.mac_layer_funcs->mac_send_packet_done(packet, size);
}
/**
 *
 * @param packet
 * @return
 */
NET_INLINE retval_t mac_packet_received(net_packet_t* packet, uint16_t size){
	netstack_state = NS_MAC_PACKET_RECEIVED;
	PRINTF("MAC EV RCV\r\n");
	return netstack.mac_layer_funcs->mac_rcv_packet_done(packet, size);

}

//* Called from user space or same layer
NET_INLINE retval_t mac_set_node_addr(mac_addr_t mac_addr_fd){
	PRINTF("MAC SET ADDR\r\n");
	return netstack.mac_layer_funcs->mac_set_addr(mac_addr_fd);
}

//* Called from any layer. Inmediate Functions
/**
 *
 * @param mac_str_val
 * @return
 */
NET_INLINE mac_addr_t mac_new_addr(char* mac_str_val, char format){
	mac_addr_t mac_addr;
	PRINTF("MAC NEW ADDR\r\n");
	netstack.mac_layer_funcs->mac_new_addr(mac_str_val, format, &mac_addr);
	return mac_addr;
}
/**
 *
 * @param mac_addr
 * @return
 */
NET_INLINE retval_t mac_delete_addr(mac_addr_t mac_addr){
	PRINTF("MAC DEL ADDR\r\n");
	return netstack.mac_layer_funcs->mac_delete_addr(mac_addr);
}
/**
 *
 * @param mac_addr
 * @return
 */
NET_INLINE retval_t mac_addr_to_string(mac_addr_t mac_addr, char* mac_str_val, char format){
	if((mac_addr == NULL) || (mac_str_val == NULL)){
		return RET_ERROR;
	}
	return netstack.mac_layer_funcs->mac_addr_to_string(mac_addr, mac_str_val, format);
}

/**
 *
 * @param dest_addr
 * @param from_addr
 * @return
 */
NET_INLINE retval_t mac_addr_cpy(mac_addr_t dest_addr, mac_addr_t from_addr){
	if((dest_addr == NULL) || (from_addr == NULL)){
		return RET_ERROR;
	}

	return netstack.mac_layer_funcs->mac_addr_cpy(dest_addr, from_addr);
}

/**
 *
 * @param a_addr
 * @param b_addr
 * @return
 */
NET_INLINE uint16_t mac_addr_cmp(mac_addr_t a_addr, mac_addr_t b_addr){
	if((a_addr == NULL) || (b_addr == NULL)){
		return 0;
	}

	return netstack.mac_layer_funcs->mac_addr_cmp(a_addr, b_addr);
}


/**
 *
 * @return
 */
NET_INLINE retval_t mac_set_node_as_gw(void){
	return netstack.mac_layer_funcs->mac_set_node_as_gw();
}


/**
 *
 */
NET_INLINE uint16_t mac_is_node_linked(void){
	return netstack.mac_layer_funcs->mac_is_node_linked();
}

/**
 *
 */
NET_INLINE retval_t mac_set_duty_cycle(uint32_t duty_cycle){
	return netstack.mac_layer_funcs->mac_set_duty_cycle(duty_cycle);
}

/**
 *
 */
NET_INLINE retval_t mac_set_process_priority(process_class_t priority){
	return netstack.mac_layer_funcs->mac_set_process_priority(priority);
}

/**
 *
 */
NET_INLINE process_class_t mac_get_process_priority(void){
	return netstack.mac_layer_funcs->mac_get_process_priority();
}

#endif

#endif /* YETIOS_CORE_NET_INCLUDE_MAC_LAYER_DEF_H_ */
