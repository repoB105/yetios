/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * netstack.h
 *
 *  Created on: 20 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file netstack.h
 */
#ifndef APPLICATION_CORE_NET_INCLUDE_NETSTACK_H_
#define APPLICATION_CORE_NET_INCLUDE_NETSTACK_H_

#define NET_INLINE	inline __attribute__((always_inline))

#include "system_api.h"
#include "packetbuffer.h"
//#include "transport_layer.h"
//#include "routing_layer.h"
//#include "phy_layer.h"

typedef enum netstack_state_{
	NS_STACK_NO_INIT,
	NS_STACK_INIT,
	NS_TP_U_SEND_DONE,
	NS_TP_U_RCV_DONE,
	NS_TP_R_SEND_DONE,
	NS_TP_R_RCV_DONE,
	NS_TP_R_ACEPTED_CONNECTION,
	NS_TP_R_CONNECTED,
	NS_TP_R_DISCONNECTED,
	NS_TP_U_OPEN,
	NS_TP_U_CLOSE,
	NS_TP_U_SEND,
	NS_TP_U_RCV,
	NS_TP_R_CREATE_SERVER,
	NS_TP_R_DELETE_SERVER,
	NS_TP_R_ACCEPT_CONN,
	NS_TP_R_CONNECT_TO,
	NS_TP_R_CLOSE_CONN,
	NS_TP_R_SEND,
	NS_TP_R_RCV,
	NS_TP_R_CHECK_CONN,
	NS_TP_PACKET_SENT,
	NS_TP_PACKET_RECEIVED,
	NS_RT_SEND_PACKET,
	NS_RT_READ_PACKET,
	NS_RT_PACKET_SENT,
	NS_RT_PACKET_RECEIVED,
	NS_MAC_SEND_PACKET,
	NS_MAC_RCV_PACKET,
	NS_MAC_PACKET_SENT,
	NS_MAC_PACKET_RECEIVED,
	NS_PHY_SEND_PACKET,
	NS_PHY_PACKET_SENT,
	NS_PHY_PACKET_RECEIVED,
	NS_PHY_SET_RECEIVING,
	NS_PHY_SET_IDLE,
	NS_PHY_SET_SLEEP,
	NS_PHY_CHECK_RSSI,
}netstack_state_t;

typedef struct netstack_data_{
	ytReactor_t* netstack_reactor;
	uint16_t netstack_thread_id;
}netstack_data_t;

/*Structs forward declaration*/
struct transport_layer_;
struct routing_layer_;
struct mac_layer_;
struct phy_layer_;

struct tp_layer_funcs_;
struct rt_layer_funcs_;
struct mac_layer_funcs_;
struct phy_layer_funcs_;

typedef struct netstack_{
	struct tp_layer_funcs_* tp_layer_funcs;
	struct rt_layer_funcs_*	rt_layer_funcs;
	struct mac_layer_funcs_* mac_layer_funcs;
	struct phy_layer_funcs_* phy_layer_funcs;
	netstack_data_t* netstack_data;
}netstack_t;

netstack_t netstack;

netstack_state_t netstack_state;

retval_t netstack_init(void);
retval_t netstack_deinit(void);

retval_t netstack_post_event(handle_event_func ev_handler_func, void* args);


/* Functiones usadas solo internamente */
retval_t netstack_op_done(uint32_t semph_id);

retval_t netstack_set_tp_layer_funcs(struct tp_layer_funcs_* transport_layer_funcs);
retval_t netstack_set_rt_layer_funcs(struct rt_layer_funcs_* routing_layer_funcs);
retval_t netstack_set_mac_layer_funcs(struct mac_layer_funcs_* mac_layer_funcs);
retval_t netstack_set_phy_layer_funcs(struct phy_layer_funcs_* phy_layer_funcs);

/*Funcion de estado de netstack*/

netstack_state_t get_netstack_state(void);

retval_t netstack_set_process_priority(process_class_t priority);
process_class_t netstack_get_process_priority(void);

/* *************************************************/

#endif /* APPLICATION_CORE_NET_INCLUDE_NETSTACK_H_ */
