/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * routing_layer.h
 *
 *  Created on: 22 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file routing_layer.h
 */
#ifndef APPLICATION_CORE_NET_INCLUDE_ROUTING_LAYER_H_
#define APPLICATION_CORE_NET_INCLUDE_ROUTING_LAYER_H_

#include "system_api.h"
#include "packetbuffer.h"
#include "netstack.h"
#include "mac_layer.h"

typedef void* net_addr_t;



typedef struct rt_layer_funcs_{
	retval_t (*rt_layer_init)();
	retval_t (*rt_layer_deinit)();

	retval_t (*rt_send_packet)(net_addr_t addr_fd, net_packet_t* packet);
	retval_t (*rt_rcv_packet)();

	retval_t (*rt_send_packet_done)(net_packet_t* packet);
	retval_t (*rt_rcv_packet_done)(net_packet_t* packet, mac_addr_t mac_from_addr);

	retval_t (*rt_get_node_addr)(uint16_t index, net_addr_t* node_addr);
	retval_t (*rt_add_node_addr)(net_addr_t new_addr_fd);
	retval_t (*rt_remove_node_addr)(net_addr_t new_addr_fd);
	retval_t (*rt_add_node_route)(net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops);
	retval_t (*rt_remove_node_route)(net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops);

	retval_t (*rt_new_net_addr)(char* net_str_val, char format, net_addr_t* net_addr_fd);
	retval_t (*rt_delete_net_addr)(net_addr_t net_addr_fd);
	retval_t (*rt_net_addr_to_string)(net_addr_t net_addr_fd, char* net_str_val, char format);
	retval_t (*rt_net_addr_cpy)(net_addr_t dest_addr_fd, net_addr_t from_addr_fd);
	retval_t (*rt_net_addr_cmp)(net_addr_t a_addr_fd, net_addr_t b_addr_fd);

	retval_t (*rt_set_node_as_gw)();
}rt_layer_funcs_t;


//* Init Deinit functions
retval_t init_routing_layer(rt_layer_funcs_t* rt_layer_funcs);
retval_t deinit_routing_layer(void);

#if ENABLE_NETSTACK_ARCH
//* Called from transport layer
NET_INLINE retval_t rt_send_packet(net_addr_t dest_addr_fd, net_packet_t* packet);
NET_INLINE retval_t rt_read_packet(void);

//* Called from mac layer
NET_INLINE retval_t rt_packet_sent(net_packet_t* packet);
NET_INLINE retval_t rt_packet_received(net_packet_t* packet, mac_addr_t mac_from_addr);

//* Called from user space or same layer. Inmediate functions
NET_INLINE net_addr_t rt_get_node_addr(uint16_t index);
NET_INLINE retval_t rt_add_node_addr(net_addr_t addr_fd);
NET_INLINE retval_t rt_remove_node_addr(net_addr_t addr_fd);
NET_INLINE retval_t rt_add_node_route(net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops);
NET_INLINE retval_t rt_remove_node_route(net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops);


//* Called from any layer. Inmediate Functions
NET_INLINE net_addr_t rt_new_empty_net_addr(void);
NET_INLINE net_addr_t rt_new_net_addr(char* net_str_val, char format);
NET_INLINE retval_t rt_delete_net_addr(net_addr_t net_addr);
NET_INLINE retval_t rt_net_addr_to_string(net_addr_t net_addr, char* net_addr_str_val, char format);
NET_INLINE retval_t rt_net_addr_cpy(net_addr_t dest_addr, net_addr_t from_addr);
NET_INLINE uint16_t rt_net_addr_cmp(net_addr_t a_addr, net_addr_t b_addr);
NET_INLINE retval_t rt_set_node_as_gw(void);
#endif

#include "routing_layer_def.h"

#endif /* APPLICATION_CORE_NET_INCLUDE_ROUTING_LAYER_H_ */
