/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * routing_layer_def.h
 *
 *  Created on: 9 oct. 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file routing_layer_def.h
 */
#ifndef YETIOS_CORE_NET_INCLUDE_ROUTING_LAYER_DEF_H_
#define YETIOS_CORE_NET_INCLUDE_ROUTING_LAYER_DEF_H_


#if ENABLE_NETSTACK_ARCH


#ifdef DEBUG
#undef DEBUG
#undef PRINTF
#endif
#define DEBUG 0
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif


//* Called from transport layer
/**
 *
 * @param packet
 * @return
 */
NET_INLINE retval_t rt_send_packet(net_addr_t dest_addr_fd, net_packet_t* packet){

	netstack_state = NS_RT_SEND_PACKET;
	PRINTF("RT EV SEND\r\n");
	return netstack.rt_layer_funcs->rt_send_packet(dest_addr_fd, packet);
}

/**
 *
 * @return
 */
NET_INLINE retval_t rt_read_packet(void){
	netstack_state = NS_RT_READ_PACKET;
	PRINTF("RT EV READ\r\n");
	return netstack.rt_layer_funcs->rt_rcv_packet();

}

//* Called from mac layer
/**
 *
 * @param packet
 * @return
 */
NET_INLINE retval_t rt_packet_sent(net_packet_t* packet){

	netstack_state = NS_RT_PACKET_SENT;
	PRINTF("RT EV SENT\r\n");
	return netstack.rt_layer_funcs->rt_send_packet_done(packet);

}
/**
 *
 * @param packet
 * @return
 */
NET_INLINE retval_t rt_packet_received(net_packet_t* packet, mac_addr_t mac_from_addr){

	netstack_state = NS_RT_PACKET_RECEIVED;
	PRINTF("RT EV READ DONE\r\n");
	return netstack.rt_layer_funcs->rt_rcv_packet_done(packet, mac_from_addr);

}


//* Called from user space or same layer
/**
 *
 * @param index
 * @return
 */
NET_INLINE net_addr_t rt_get_node_addr(uint16_t index){
	net_addr_t node_addr;

	PRINTF("RT GET ADDR\r\n");

	if((netstack.rt_layer_funcs->rt_get_node_addr(index, &node_addr)) == RET_OK){
		 return node_addr;
	}
	else{
		return NULL;
	}
}

/**
 *
 * @param addr_fd
 * @return
 */
NET_INLINE retval_t rt_add_node_addr(net_addr_t addr_fd){

	PRINTF("RT ADD ADDR\r\n");
	return netstack.rt_layer_funcs->rt_add_node_addr(addr_fd);

}

/**
 *
 * @param addr_fd
 * @return
 */
NET_INLINE retval_t rt_remove_node_addr(net_addr_t addr_fd){

	PRINTF("RT RMV ADDR\r\n");
	return netstack.rt_layer_funcs->rt_remove_node_addr(addr_fd);
}
/**
 *
 * @param dest_addr_fd
 * @param next_addr_fd
 * @param hops
 * @return
 */
NET_INLINE retval_t rt_add_node_route(net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops){

	PRINTF("RT ADD ROUTE\r\n");
	return netstack.rt_layer_funcs->rt_add_node_route(dest_addr_fd, next_addr_fd, hops);
}

/**
 *
 * @param dest_addr_fd
 * @param next_addr_fd
 * @param hops
 * @return
 */
NET_INLINE retval_t rt_remove_node_route(net_addr_t dest_addr_fd, net_addr_t next_addr_fd, uint16_t hops){

	PRINTF("RT REMOVE ROUTE\r\n");
	return netstack.rt_layer_funcs->rt_remove_node_route(dest_addr_fd, next_addr_fd, hops);
}
/**
 *
 * @param net_str_val
 * @return
 */
NET_INLINE net_addr_t rt_new_net_addr(char* net_str_val, char format){
	net_addr_t net_addr;
	if(net_str_val == NULL){
		return NULL;
	}
	PRINTF("RT NEW ADDR\r\n");
	netstack.rt_layer_funcs->rt_new_net_addr(net_str_val, format, &net_addr);
	return net_addr;
}

/**
 *
 * @return
 */
NET_INLINE net_addr_t rt_new_empty_net_addr(void){
	net_addr_t net_addr;
	PRINTF("RT NEW ADDR\r\n");
	netstack.rt_layer_funcs->rt_new_net_addr(NULL, 0, &net_addr);
	return net_addr;
}

/**
 *
 * @param net_addr
 * @return
 */
NET_INLINE retval_t rt_delete_net_addr(net_addr_t net_addr){
	if(net_addr == NULL){
		return RET_ERROR;
	}
	PRINTF("RT DELETE ADDR\r\n");
	return netstack.rt_layer_funcs->rt_delete_net_addr(net_addr);
}
/**
 *
 * @param net_addr
 * @return
 */
NET_INLINE retval_t rt_net_addr_to_string(net_addr_t net_addr, char* net_addr_str_val, char format){
	if((net_addr == NULL) || (net_addr_str_val == NULL)){
		return RET_ERROR;
	}
	return netstack.rt_layer_funcs->rt_net_addr_to_string(net_addr, net_addr_str_val, format);
}
/**
 *
 * @param dest_addr
 * @param from_addr
 * @return
 */
NET_INLINE retval_t rt_net_addr_cpy(net_addr_t dest_addr, net_addr_t from_addr){
	if((dest_addr == NULL) || (from_addr == NULL)){
		return RET_ERROR;
	}
	return netstack.rt_layer_funcs->rt_net_addr_cpy(dest_addr, from_addr);
}

/**
 *
 * @param a_addr
 * @param b_addr
 * @return
 */
NET_INLINE uint16_t rt_net_addr_cmp(net_addr_t a_addr, net_addr_t b_addr){

	if((a_addr == NULL) || (b_addr == NULL)){
		return 0;
	}
	if(netstack.rt_layer_funcs->rt_net_addr_cmp(a_addr, b_addr) == RET_OK){
		return 1;
	}
	return 0;
}

/**
 *
 * @return
 */
NET_INLINE retval_t rt_set_node_as_gw(void){

	return netstack.rt_layer_funcs->rt_set_node_as_gw();
}

#endif

#endif /* YETIOS_CORE_NET_INCLUDE_ROUTING_LAYER_DEF_H_ */
