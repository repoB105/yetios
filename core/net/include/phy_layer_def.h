/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * phy_layer_def.h
 *
 *  Created on: 9 oct. 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file phy_layer_def.h
 */
#ifndef YETIOS_CORE_NET_INCLUDE_PHY_LAYER_DEF_H_
#define YETIOS_CORE_NET_INCLUDE_PHY_LAYER_DEF_H_

#if ENABLE_NETSTACK_ARCH


#ifdef DEBUG
#undef DEBUG
#undef PRINTF
#endif
#define DEBUG 0
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif



//* Called from mac layer
/**
 *
 * @param packet
 * @return
 */
NET_INLINE retval_t phy_send_packet(net_packet_t* packet, uint16_t size){
	netstack_state = NS_PHY_SEND_PACKET;
	PRINTF("PH EV SEND\r\n");
	return netstack.phy_layer_funcs->phy_send_packet(packet, size);

}

//* Called from phy layer
/**
 *
 * @param packet
 * @return
 */
NET_INLINE retval_t phy_post_event_packet_sent(net_packet_t* packet, uint16_t size){

	phy_layer_func_args_t* phy_layer_args = (phy_layer_func_args_t*) ytMalloc(sizeof(phy_layer_func_args_t));

	phy_layer_args->packet = packet;
	phy_layer_args->size = size;

	PRINTF("PH POST SENT\r\n");
	netstack_post_event(packet_sent_event, phy_layer_args);

	return RET_OK;
}
/**
 *
 * @param packet
 * @return
 */
NET_INLINE retval_t phy_post_event_packet_received(net_packet_t* packet, uint16_t size){

	phy_layer_func_args_t* phy_layer_args = (phy_layer_func_args_t*) ytMalloc(sizeof(phy_layer_func_args_t));

	phy_layer_args->packet = packet;
	phy_layer_args->size = size;
	PRINTF("PH POST RCV\r\n");
	netstack_post_event(packet_received_event, phy_layer_args);

	return RET_OK;
}


///**
// *
// */
//NET_INLINE retval_t phy_set_pckt_length(uint16_t pckt_length){
//	return netstack.phy_layer_funcs->phy_set_pckt_length(pckt_length);
//}
//* Called from mac layer. Inmediate functions: No event posted
/**
 *
 * @return
 */
NET_INLINE retval_t phy_set_mode_receiving(void){
	netstack_state = NS_PHY_SET_RECEIVING;
	PRINTF("PH SET RX\r\n");
	return netstack.phy_layer_funcs->phy_set_mode_receiving();
}
/**
 *
 * @return
 */
NET_INLINE retval_t phy_set_mode_idle(void){

	netstack_state = NS_PHY_SET_IDLE;
	PRINTF("PH SET IDLE\r\n");
	return netstack.phy_layer_funcs->phy_set_mode_idle();
}
/**
 *
 * @return
 */
NET_INLINE retval_t phy_set_mode_sleep(void){
	netstack_state = NS_PHY_SET_SLEEP;
	PRINTF("PH SET SLEEP\r\n");
	return netstack.phy_layer_funcs->phy_set_mode_sleep();
}

/**
 *
 * @return
 */
NET_INLINE float32_t phy_check_channel_rssi(void){
	float32_t ret_rssi;

	netstack_state = NS_PHY_CHECK_RSSI;
	PRINTF("PH SET RSSI\r\n");
	netstack.phy_layer_funcs->phy_check_channel_rssi(&ret_rssi);

	return ret_rssi;

}

/**
 *
 * @return
 */
NET_INLINE float32_t phy_get_last_rssi(void){

	float32_t ret_rssi;

	netstack.phy_layer_funcs->phy_get_last_rssi(&ret_rssi);

	return ret_rssi;
}

/**
 *
 * @param base_freq
 * @return
 */
NET_INLINE retval_t phy_set_base_freq(uint32_t base_freq){

	return netstack.phy_layer_funcs->phy_set_base_freq(base_freq);
}
/**
 *
 * @return
 */
NET_INLINE uint32_t phy_get_base_freq(void){
	uint32_t ret_base_freq;
	if(netstack.phy_layer_funcs->phy_get_base_freq(&ret_base_freq) != RET_OK){
		return 0;
	}
	return ret_base_freq;
}

/**
 *
 * @param base_freq
 * @return
 */
NET_INLINE uint32_t phy_get_channel_num(void){
	uint32_t ret_channel_num;
	if(netstack.phy_layer_funcs->phy_get_channel_num(&ret_channel_num) != RET_OK){
		return 0xFFFFFFFF;
	}
	return ret_channel_num;
}
/**
 *
 * @param channel_num
 * @return
 */
NET_INLINE retval_t phy_set_freq_channel(uint32_t channel_num){
	return netstack.phy_layer_funcs->phy_set_freq_channel(channel_num);
}
/**
 *
 * @return
 */
NET_INLINE uint32_t phy_get_freq_channel(void){
	uint32_t ret_channel;
	if(netstack.phy_layer_funcs->phy_get_freq_channel(&ret_channel) != RET_OK){
		return 0;
	}
	return ret_channel;
}

/**
 *
 * @param baud_rate
 * @return
 */
NET_INLINE retval_t phy_set_baud_rate(uint32_t baud_rate){
	return netstack.phy_layer_funcs->phy_set_baud_rate(baud_rate);
}

/**
 *
 * @param out_power
 * @return
 */
NET_INLINE retval_t phy_set_out_power(int16_t out_power){
	return netstack.phy_layer_funcs->phy_set_out_power(out_power);
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @param key
 * @return
 */
NET_INLINE retval_t phy_encrypt_packet(net_packet_t* packet, uint8_t* key, uint16_t size){

	return netstack.phy_layer_funcs->phy_encrypt_packet(packet, key, size);
}

/**
 *
 * @param phy_layer_data
 * @param packet
 * @param key
 * @return
 */
NET_INLINE retval_t phy_decrypt_packet(net_packet_t* packet, uint8_t* key, uint16_t size){

	return netstack.phy_layer_funcs->phy_decrypt_packet(packet, key, size);
}

/**
 *
 */
NET_INLINE retval_t phy_set_process_priority(process_class_t priority){
	return netstack.phy_layer_funcs->phy_set_process_priority(priority);
}

/**
 *
 */
NET_INLINE process_class_t phy_get_process_priority(void){
	return netstack.phy_layer_funcs->phy_get_process_priority();
}


#endif

#endif /* YETIOS_CORE_NET_INCLUDE_PHY_LAYER_DEF_H_ */
