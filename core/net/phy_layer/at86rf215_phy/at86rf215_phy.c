/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * at86rf215_phy.c
 *
 *  Created on: 21/09/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file at86rf215_phy.c
 */

#include "phy_layer.h"
#include "mac_layer.h"
#include "null_phy.h"
#include "packetbuffer.h"
#include "at86rf215_core.h"

#if ENABLE_NETSTACK_ARCH
#if ENABLE_AT86RF215_PHY_LAYER

#include "platform_migou_phy.h"

#define DEBUG 0
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#define SEND_PACKET_TIMEOUT					200		// A packet of 64 bytes should not take more than 5ms to be sent at 250 kbps


// AT86RF215 DEFAULT CONFIG ----------------------------------------------------

#define AT86RF215_DEFAULT_BAND				RF24				// 2.4 GHz Transceiver

// CHANNEL CONFIGURATION
// IEEE 802.15.4-2012 PHY: MR-FSK operating mode #3 in 2400-2483.5 MHz band
//      				   CS 0.4 MHz; TotalNumCh 207; CCF0 2400.4 MHz
#define AT86RF215_DEFAULT_CH_MODE			MODE_IEEE			// Channel Mode: IEEE 802.15.4g-2012 compliant channel scheme
#define AT86RF215_DEFAULT_CH0_CENTER_FREQ	2400400				// Channel 0 Center Frequency in kHz
#define AT86RF215_DEFAULT_CH_NUMBER			0					// Channel Number
#define AT86RF215_DEFAULT_CH_SPACING		400e3				// Channel Spacing in Hz

// BASEBAND CONFIGURATION
#define AT86RF215_DEFAULT_MODULATION		BFSK				// 2-FSK
#define AT86RF215_DEFAULT_BAUD_RATE			200e3				// 200 kbps
#define AT86RF215_DEFAULT_OUT_POWER			ZERO_DBM			// Output Power: 0 dBm
#define AT86RF215_DEFAULT_PACKET_LENGTH		FIXED_PACKET_SIZE

// AT86RF215 DEFAULT CONFIG ----------------------------------------------------


#define PROCESS_CHECK_TIMEOUT				2000


typedef struct __packed at86rf215_phy_layer_data_{
	at86rf215_data_t* at86rf215_data;
	uint32_t last_interrupt_time;
	uint32_t proc_semph_id;
	uint32_t at86rf215_mutex_id;
	net_packet_t* sending_packet;
	uint32_t send_time;
	uint16_t last_send_pckt_length;
	uint16_t at86rf215_proc_id;
	uint16_t pending_int;
	uint8_t posted_packet_sent;
}at86rf215_phy_layer_data_t;

static at86rf215_phy_layer_data_t* at86rf215_phy_data = NULL;

// -----------------------------------------------------------------------------
static retval_t at86rf215_phy_layer_init();
static retval_t at86rf215_phy_layer_deinit();

static retval_t at86rf215_phy_send_packet(net_packet_t* packet, uint16_t size);

static retval_t at86rf215_phy_send_packet_done(net_packet_t* packet, uint16_t size);
static retval_t at86rf215_phy_rcv_packet_done(net_packet_t* packet, uint16_t size);

// Functions called immediately. They are not posted in event mode.  It can be done like this since they should only be called from the MAC layer.
static retval_t at86rf215_phy_set_mode_receiving();
static retval_t at86rf215_phy_set_mode_idle();
static retval_t at86rf215_phy_set_mode_sleep();

static retval_t at86rf215_phy_check_channel_rssi(float32_t* read_rssi);
static retval_t at86rf215_phy_get_last_rssi(float32_t* read_rssi);

static retval_t at86rf215_phy_set_base_freq(uint32_t base_freq);
static retval_t at86rf215_phy_get_base_freq(uint32_t* base_freq);

static retval_t at86rf215_phy_get_channel_num(uint32_t* channel_num);		// Returns the maximum available channels in a freq_base
static retval_t at86rf215_phy_set_freq_channel(uint32_t channel_num);
static retval_t at86rf215_phy_get_freq_channel(uint32_t* channel_num);	// Get the current freq. channel

static retval_t at86rf215_phy_set_baud_rate(uint32_t baud_rate);
static retval_t at86rf215_phy_set_out_power(int16_t baud_rate);

static retval_t at86rf215_phy_encrypt_packet(net_packet_t* packet, uint8_t* key, uint16_t size);
static retval_t at86rf215_phy_decrypt_packet(net_packet_t* packet, uint8_t* key, uint16_t size);
/* ****************************************************/

// AT86RF215 PROCESS FUNC ------------------------------------------------------
static void at86rf215_process_func(const void* args);
// -----------------------------------------------------------------------------

// AT86RF215 PRIVATE FUNCS -----------------------------------------------------
#if ENABLE_AT86RF215
static retval_t at86rf215_init(at86rf215_data_t* at86rf215_data, char* spi_dev);
static void at86rf215_irq_cb(const void* args);
#endif
// -----------------------------------------------------------------------------

phy_layer_funcs_t at86rf215_phy_funcs = {
		.phy_layer_init = at86rf215_phy_layer_init,
		.phy_layer_deinit = at86rf215_phy_layer_deinit,
		.phy_send_packet = at86rf215_phy_send_packet,
		.phy_send_packet_done = at86rf215_phy_send_packet_done,
		.phy_rcv_packet_done = at86rf215_phy_rcv_packet_done,
		.phy_set_mode_receiving = at86rf215_phy_set_mode_receiving,
		.phy_set_mode_idle = at86rf215_phy_set_mode_idle,
		.phy_set_mode_sleep = at86rf215_phy_set_mode_sleep,
		.phy_check_channel_rssi = at86rf215_phy_check_channel_rssi,
		.phy_get_last_rssi = at86rf215_phy_get_last_rssi,
		.phy_set_base_freq = at86rf215_phy_set_base_freq,
		.phy_get_base_freq = at86rf215_phy_get_base_freq,
		.phy_get_channel_num = at86rf215_phy_get_channel_num,
		.phy_set_freq_channel = at86rf215_phy_set_freq_channel,
		.phy_get_freq_channel = at86rf215_phy_get_freq_channel,
		.phy_set_baud_rate = at86rf215_phy_set_baud_rate,
		.phy_set_out_power = at86rf215_phy_set_out_power,
		.phy_encrypt_packet = at86rf215_phy_encrypt_packet,
		.phy_decrypt_packet = at86rf215_phy_decrypt_packet,
};



/**
 *
 * @param phy_layer_data
 * @return
 */
static retval_t at86rf215_phy_layer_init(){

	if(at86rf215_phy_data != NULL){
		return RET_ERROR;
	}

	if ((at86rf215_phy_data = (at86rf215_phy_layer_data_t*) ytMalloc(sizeof(at86rf215_phy_layer_data_t))) == NULL){
		return RET_ERROR;
	}

#if ENABLE_AT86RF215
	// GENERAL CONFIGURATION
	if((at86rf215_phy_data->at86rf215_data = new_at86rf215_data(AT86RF215_CS_PIN, AT86RF215_IRQ_PIN, AT86RF215_DEFAULT_BAND, at86rf215_irq_cb, NULL)) == NULL){
		ytFree(at86rf215_phy_data);
		at86rf215_phy_data = NULL;
		return RET_ERROR;
	}

	if(at86rf215_init(at86rf215_phy_data->at86rf215_data, AT86RF215_SPI_DEV) != RET_OK){
		delete_at86rf215_data(at86rf215_phy_data->at86rf215_data);
		ytFree(at86rf215_phy_data);
		at86rf215_phy_data = NULL;
		return RET_ERROR;
	}
#endif

	at86rf215_phy_data->pending_int = 0;
	at86rf215_phy_data->posted_packet_sent = 0;
	at86rf215_phy_data->send_time = 0;
	at86rf215_phy_data->proc_semph_id = ytSemaphoreCreate(1);
	at86rf215_phy_data->at86rf215_mutex_id = ytMutexCreate();
	at86rf215_phy_data->sending_packet = NULL;
	at86rf215_phy_data->last_interrupt_time = 0;
	ytSemaphoreWait(at86rf215_phy_data->proc_semph_id, YT_WAIT_FOREVER);
	ytStartProcess("at86rf215_phy_proc", at86rf215_process_func, PHY_LAYER_PROCESS_PRIORITY, YT_MINIMAL_STACK_SIZE*2, &(at86rf215_phy_data->at86rf215_proc_id), NULL);

	return RET_OK;;
}


/**
 *
 * @param phy_layer_data
 * @return
 */
static retval_t at86rf215_phy_layer_deinit(){

	if(at86rf215_phy_data == NULL){
		return RET_ERROR;
	}

	ytMutexDelete(at86rf215_phy_data->at86rf215_mutex_id);
	ytSemaphoreDelete(at86rf215_phy_data->proc_semph_id);
	ytExitProcess(at86rf215_phy_data->at86rf215_proc_id);


#if ENABLE_AT86RF215
	if(at86rf215_hw_deInit(at86rf215_phy_data->at86rf215_data) != RET_OK){
		return RET_ERROR;
	}
	if(delete_at86rf215_data(at86rf215_phy_data->at86rf215_data) != RET_OK){
		return RET_ERROR;
	}

#endif
	ytFree(at86rf215_phy_data);
	at86rf215_phy_data = NULL;

	return RET_OK;
}


/**
 *
 * @param phy_layer_data
 * @param packet
 * @return
 */
static retval_t at86rf215_phy_send_packet(net_packet_t* packet, uint16_t size){

	PRINTF("AT86 SENDING\r\n");

	ytMutexWait(at86rf215_phy_data->at86rf215_mutex_id, YT_WAIT_FOREVER);
	if(at86rf215_phy_data->sending_packet != NULL){
		ytMutexRelease(at86rf215_phy_data->at86rf215_mutex_id);
		return RET_ERROR;
	}

#if ENABLE_AT86RF215

	at86rf215_phy_data->sending_packet = packet;
	at86rf215_phy_data->last_send_pckt_length = size;
	if(at86rf215_send_data(at86rf215_phy_data->at86rf215_data, (uint8_t*) packet, size) != RET_OK){
		at86rf215_phy_data->sending_packet = NULL;
		ytMutexRelease(at86rf215_phy_data->at86rf215_mutex_id);
		return RET_ERROR;
	}
	at86rf215_phy_data->send_time = ytGetSysTickMilliSec();
	ytMutexRelease(at86rf215_phy_data->at86rf215_mutex_id);
	return RET_OK;

#endif

	ytMutexRelease(at86rf215_phy_data->at86rf215_mutex_id);
	return RET_ERROR;
}


/**
 *
 * @param phy_layer_data
 * @param packet
 * @return
 */
static retval_t at86rf215_phy_send_packet_done(net_packet_t* packet, uint16_t size){

	PRINTF("AT86 SENT\r\n");
	ytMutexWait(at86rf215_phy_data->at86rf215_mutex_id, YT_WAIT_FOREVER);
	mac_packet_sent(packet, size);
	at86rf215_phy_data->sending_packet = NULL;
	at86rf215_phy_data->posted_packet_sent = 0;
	ytMutexRelease(at86rf215_phy_data->at86rf215_mutex_id);
	return RET_OK;
}


/**
 *
 * @param phy_layer_data
 * @param packet
 * @return
 */
static retval_t at86rf215_phy_rcv_packet_done(net_packet_t* packet, uint16_t size){

	float32_t rssi_val;

	at86rf215_phy_get_last_rssi(&rssi_val);
	PRINTF("RSSI: %.2f\r\n", rssi_val);
	mac_packet_received(packet, size);
	// TODO
	// The transceiver continues in state RX since the reception is persistent.
	// Exiting the state RX must be done explicitly by calling at86rf215_set_mode_sleep or at86rf215_set_mode_idle.
	return RET_OK;
}


// Functions called immediately. They are not posted in event mode.  It can be done like this since they should only be called from the MAC layer.
/**
 *
 * @param phy_layer_data
 * @return
 */
static retval_t at86rf215_phy_set_mode_receiving(){

	if(at86rf215_phy_data == NULL){
		return RET_ERROR;
	}
#if ENABLE_AT86RF215
	return at86rf215_set_mode_rx(at86rf215_phy_data->at86rf215_data);
#endif
	return RET_ERROR;
}


/**
 *
 * @param phy_layer_data
 * @return
 */
static retval_t at86rf215_phy_set_mode_idle(){


	if(at86rf215_phy_data == NULL){
		return RET_ERROR;
	}
#if ENABLE_AT86RF215
	return at86rf215_set_mode_idle(at86rf215_phy_data->at86rf215_data);
#endif
	return RET_ERROR;

}


/**
 *
 * @param phy_layer_data
 * @return
 */
static retval_t at86rf215_phy_set_mode_sleep(){


	if(at86rf215_phy_data == NULL){
		return RET_ERROR;
	}
#if ENABLE_AT86RF215
	return at86rf215_set_mode_sleep(at86rf215_phy_data->at86rf215_data);
#endif
	return RET_ERROR;
}


/**
 *
 * @param phy_layer_data
 * @param read_rssi
 * @return
 */
static retval_t at86rf215_phy_check_channel_rssi(float32_t* read_rssi){

	if(at86rf215_phy_data == NULL){
		return RET_ERROR;
	}
#if ENABLE_AT86RF215
	(*read_rssi) = (float32_t)at86rf215_get_last_rssi(at86rf215_phy_data->at86rf215_data);
#endif

	return RET_OK;
}


/**
 *
 * @param phy_layer_data
 * @param read_rssi
 * @return
 */
static retval_t at86rf215_phy_get_last_rssi(float32_t* read_rssi){

	if(at86rf215_phy_data == NULL){
		return RET_ERROR;
	}
#if ENABLE_AT86RF215
	(*read_rssi) = (float32_t)at86rf215_get_last_edv(at86rf215_phy_data->at86rf215_data);
#endif
	return RET_OK;
}


/**
 *
 * @param phy_layer_data
 * @param base_freq
 * @return
 */
static retval_t at86rf215_phy_set_base_freq(uint32_t base_freq){
	return RET_ERROR;
}


/**
 *
 * @param phy_layer_data
 * @param base_freq
 * @return
 */
static retval_t at86rf215_phy_get_base_freq(uint32_t* base_freq){
	return RET_ERROR;
}


/**
 *
 * @param phy_layer_data
 * @param channel_num
 * @return
 */
static retval_t at86rf215_phy_get_channel_num(uint32_t* channel_num){
	return RET_ERROR;
}


/**
 *
 * @param phy_layer_data
 * @param channel_num
 * @return
 */
static retval_t at86rf215_phy_set_freq_channel(uint32_t channel_num){
	return RET_ERROR;
}


/**
 *
 * @param phy_layer_data
 * @param channel_num
 * @return
 */
static retval_t at86rf215_phy_get_freq_channel(uint32_t* channel_num){
	return RET_ERROR;
}


/**
 *
 * @param phy_layer_data
 * @param baud_rate
 * @return
 */
static retval_t at86rf215_phy_set_baud_rate(uint32_t baud_rate){
	return RET_ERROR;
}


/**
 *
 * @param phy_layer_data
 * @param baud_rate
 * @return
 */
static retval_t at86rf215_phy_set_out_power(int16_t baud_rate){
	return RET_ERROR;
}


/**
 *
 * @param phy_layer_data
 * @param packet
 * @param key
 * @return
 */
static retval_t at86rf215_phy_encrypt_packet(net_packet_t* packet, uint8_t* key, uint16_t size){
	return RET_ERROR;
}


/**
 *
 * @param phy_layer_data
 * @param packet
 * @param key
 * @return
 */
static retval_t at86rf215_phy_decrypt_packet(net_packet_t* packet, uint8_t* key, uint16_t size){
	return RET_ERROR;
}


// AT86RF215 process Func ------------------------------------------------------
/**
 *
 * @param args
 */
static void at86rf215_process_func(const void* args){

#if ENABLE_AT86RF215
	net_packet_t* rcv_packet;
	uint16_t ret;
	uint8_t num_rcv_bytes = 0;
#endif

	while(1){
		if(!at86rf215_phy_data->pending_int){
			if(ytSemaphoreWait(at86rf215_phy_data->proc_semph_id, PROCESS_CHECK_TIMEOUT) != RET_OK){
			}
		}
		PRINTF("AT86 CB\r\n");

		if(at86rf215_phy_data->pending_int){
			at86rf215_phy_data->pending_int--;
		}

		ytMutexWait(at86rf215_phy_data->at86rf215_mutex_id, YT_WAIT_FOREVER);
		if(at86rf215_phy_data->sending_packet != NULL){
			if(ytGetSysTickMilliSec() - at86rf215_phy_data->send_time > SEND_PACKET_TIMEOUT){		//Free the packet to the upper layers
				if(!at86rf215_phy_data->posted_packet_sent){			//Only one packet can be sent at a time. Prevent calling packet_sent twice
					at86rf215_phy_data->posted_packet_sent = 1;
					phy_post_event_packet_sent(at86rf215_phy_data->sending_packet, at86rf215_phy_data->last_send_pckt_length);
				}
			}
		}
		/* Process at86rf215 interrupt */
#if ENABLE_AT86RF215
		ret = at86rf215_irq_routine(at86rf215_phy_data->at86rf215_data);

		if(ret & RET_PCKT_SENT){
			if(at86rf215_phy_data->sending_packet != NULL){
				if(!at86rf215_phy_data->posted_packet_sent){			//Only one packet can be sent at a time. Prevent calling packet_sent twice
					at86rf215_phy_data->posted_packet_sent = 1;
					phy_post_event_packet_sent(at86rf215_phy_data->sending_packet, at86rf215_phy_data->last_send_pckt_length);
				}
			}

		}
		else if(ret & RET_PCKT_RCV){
			if((rcv_packet = rx_packetbuffer_get_free_packet()) != NULL){
				at86rf215_read_num_rcv_bytes(at86rf215_phy_data->at86rf215_data, &num_rcv_bytes);
				if(num_rcv_bytes){
					at86rf215_read_rcv_data(at86rf215_phy_data->at86rf215_data, (uint8_t*) rcv_packet, (uint16_t) num_rcv_bytes);
					rcv_packet->pckt_rssi = at86rf215_get_last_rssi(at86rf215_phy_data->at86rf215_data);
					rcv_packet->rcv_tick_time = at86rf215_phy_data->last_interrupt_time;
					phy_post_event_packet_received(rcv_packet, (uint16_t)num_rcv_bytes);
				}
				else{
					at86rf215_flush_last_rcv_data(at86rf215_phy_data->at86rf215_data);
					rx_packetbuffer_release_packet(rcv_packet);
				}
			}
			else{
				at86rf215_flush_last_rcv_data(at86rf215_phy_data->at86rf215_data);	// Discard the received packet if there's not enough space in the packetbuff
			}
			at86rf215_phy_set_mode_receiving();
		}
		else{

		}
#endif

		ytMutexRelease(at86rf215_phy_data->at86rf215_mutex_id);
	}
}
// -----------------------------------------------------------------------------

#if ENABLE_AT86RF215
/**
 *
 * @param at86rf215_data
 * @param spi_dev
 * @return
 */
static retval_t at86rf215_init(at86rf215_data_t* at86rf215_data, char* spi_dev){

	at86rf215_config_t* at86rf215_init_config;
	at86rf215_init_config = (at86rf215_config_t*) ytMalloc(sizeof(at86rf215_config_t));

	// CHANNEL CONFIGURATION
	at86rf215_init_config->ch_mode 			= AT86RF215_DEFAULT_CH_MODE;
	at86rf215_init_config->ch0_center_freq	= AT86RF215_DEFAULT_CH0_CENTER_FREQ;
	at86rf215_init_config->ch_number 		= AT86RF215_DEFAULT_CH_NUMBER;
	at86rf215_init_config->ch_spacing		= AT86RF215_DEFAULT_CH_SPACING;

	// BASEBAND CONFIGURATION
	at86rf215_init_config->modulation 		= AT86RF215_DEFAULT_MODULATION;
	at86rf215_init_config->baud_rate 		= AT86RF215_DEFAULT_BAUD_RATE;
	at86rf215_init_config->output_pwr 		= AT86RF215_DEFAULT_OUT_POWER;

	if(at86rf215_hw_init(at86rf215_data, at86rf215_init_config, spi_dev) != RET_OK){
		ytFree(at86rf215_init_config);
		return RET_ERROR;
	}

	ytFree(at86rf215_init_config);
	return RET_OK;
}

/**
 *
 * @param args
 */
static void at86rf215_irq_cb(void const* args){
	at86rf215_phy_data->last_interrupt_time = ytGetSysTickMilliSec();
	at86rf215_phy_data->pending_int++;
	ytSemaphoreRelease(at86rf215_phy_data->proc_semph_id);
}
#endif

#endif
#endif	// ENABLE_NETSTACK_ARCH
