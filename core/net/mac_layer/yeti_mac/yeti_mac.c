/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * yeti_mac.c
 *
 *  Created on: 11 de may. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file yeti_mac.c
 */

#include "mac_layer.h"
#include "phy_layer.h"
#include "routing_layer.h"
#include "yeti_mac.h"
#include "unique_id.h"
#include "packetbuffer.h"
#include <stdlib.h>

#if ENABLE_NETSTACK_ARCH

#define DEFAULT_MAC_ADDR				0x0000b105

#define GW_ALLWAYS_ON					1



#define MAX_FLOOD_HOPS					6
#define FLOOD_MAX_RANDOM_DELAY			50

#define SYNC_PERIOD						10000		//A sync packet will be sent each 9000 ms to synchronize

#define SENDING_SYNC_TIMEOUT			400

#define SYNC_PACKET_CODE				0xAAAABB12
#define SYNC_MSG_DISC					1
#define SYNC_MSG_ACK					2
#define SYNC_MSG_SET_TIME				3

#define NO_SYNC_TIME_OFFSET				0xFFFFFFFF		//Offset not set
#define DEFAULT_SYNC_TIME_OFFSET		0

#define MAX_DELAY_ALLOWED_SYNC_PACKET	1000
//#define SYNC_PACKET_CODE_OFFSET			0			//Offset bytes of each part of a sync packet
//#define SYNC_PACKET_TIMESTAMP_OFFSET	0			//Offset bytes of each part of a sync packet
//#define SYNC_PACKET_DELAY_TIME_OFFSET	8
//#define SYNC_PACKET_SYNC_LVL_OFFSET		12

#define MAX_UNSYNC_TIMES				2			//Number of Sync_Periods a node is considered unsynchronized if it has not received any sync packet

#define TIMESTAMP_SYNC_DIFF				2			//Max time difference (ms) a received sync packet is allowed

#define MAC_RDC_GUARD						2		//Guard time used to ensure the receiver is in listen state with small un-synchronization

#define NUM_TX_SLOTS						PACKET_BUFFER_SIZE			//Number of tx beacon and packet slots. There is an extra slot for transmitting beacons

/* YETIMAC TIME CONF */
#define MAC_BEACON_TX_SLOT_TIME				2			//Time of TX beacon slot. The beacons do not contain data, only orders the nodes to listen. Empirical minimun value of 2.WITH OPTIMIZATION, otherwise 3 ms. Larger values will result in higher consumption as node are listening for beacons larger
#define MAC_MIN_PACKET_TX_SLOT_TIME			6			//Minimun time for TX a packet 6 ms measured with optimization

#define MAC_DEFAULT_RADIO_DUTY_CYCLE				10				//RADIO DUTY CYCLE IN PERCENTAGE %

#define PACKETS_TO_SEND_BUFFER_SIZE			NUM_TX_SLOTS

#define YETIMAC_BEACON_ID					0xABCD7398


typedef struct __packed yeti_mac_addr_{
	uint32_t mac_val;
}yeti_mac_addr_t;

typedef enum yetimac_state_{
	NO_RX = 0,
	RX_BEACONS = 1,
	RX_TX_BEACONS = 2,
	RX_PACKETS = 3,
	RX_TX_PACKETS = 4,
}yetimac_state_t;

typedef struct yeti_mac_config_{
	uint32_t mac_duty_cycle;
	uint32_t mac_beacon_listen_on_time;
	uint32_t mac_rdc_period;
	uint32_t mac_packet_tx_slot_time;
}yeti_mac_config_t;

typedef struct __packed yeti_mac_layer_data_{
	yeti_mac_addr_t node_mac_addr;
#if USE_SYNC_MAC
	yeti_mac_config_t yeti_mac_config;
	uint32_t sync_disc_timer;
	uint64_t last_sync_timestamp;

	uint32_t yetimac_mutex_id;
	uint32_t tx_mutex_id;

	uint32_t sync_semaphore_id;
	uint32_t sync_time_offset;
	uint32_t last_sync_proc_delay;
	uint32_t sending_sync_time;

	net_packet_t*  tx_packets_buffer[PACKETS_TO_SEND_BUFFER_SIZE];
	net_packet_t** tx_packets_buffer_head_ptr;	//First free packet ptr
	uint16_t num_packets_in_buffer;
	uint16_t current_slot_number;
	yeti_mac_addr_t beacon_addresses_sent[NUM_TX_SLOTS];
	uint16_t num_beacon_addresses_sent;

	uint16_t apply_timestamp_proc_id;
	uint16_t num_times_func;
	uint16_t yetimac_process_id;

	uint8_t go_to_receive;

	yetimac_state_t current_state;

	uint8_t is_node_gw;
	uint8_t node_sync_level;
	uint8_t is_node_sync;
#endif
}yeti_mac_layer_data_t;


typedef struct __packed flood_packet_{
	yeti_mac_addr_t src_addreses[MAX_FLOOD_HOPS];
	uint16_t num_hops;
}flood_packet_t;

typedef struct __packed beacon_packet_{
	uint32_t beacon_id;
	uint32_t dest_mac_addr;
}beacon_packet_t;

typedef struct __packed sync_packet_{
	uint32_t sync_packet_code;
	uint64_t sync_timestamp;
	uint32_t sync_tx_offset_time;
	uint8_t sync_level;
	uint8_t	sync_packet_type;
}sync_packet_t;

#define BEACON_PCKT_LENGTH		sizeof(beacon_packet_t)
#define STANDARD_PCKT_LENGTH	FIXED_PACKET_SIZE


/* Yetimac private data */
static yeti_mac_layer_data_t* yeti_mac_data = NULL;

/*Yetimac layer driver functions*/
static retval_t yeti_mac_layer_init(void);
static retval_t yeti_mac_layer_deinit(void);

static retval_t yeti_mac_send_packet(mac_addr_t mac_addr_dest_fd, net_packet_t* packet);
static retval_t yeti_mac_rcv_packet(void);

static retval_t yeti_mac_send_packet_done(net_packet_t* packet, uint16_t size);
static retval_t yeti_mac_rcv_packet_done(net_packet_t* packet, uint16_t size);

static retval_t yeti_mac_set_addr(mac_addr_t mac_addr_fd);

static retval_t yeti_mac_new_addr(char* mac_str_val, char format, mac_addr_t* mac_addr_fd);
static retval_t yeti_mac_delete_addr(mac_addr_t mac_addr_fd);
static retval_t yeti_mac_addr_to_string(mac_addr_t mac_addr_fd, char* mac_str_val, char format);
static retval_t yeti_mac_addr_cpy(mac_addr_t dest_addr_fd, mac_addr_t from_addr_fd);
static uint16_t yeti_mac_addr_cmp(mac_addr_t a_addr_fd, mac_addr_t b_addr_fd);

static retval_t yeti_mac_set_node_as_gw(void);

static uint16_t yeti_mac_is_node_linked(void);
static retval_t yeti_mac_set_duty_cycle(uint32_t duty_cycle);
static retval_t yeti_mac_set_process_priority(process_class_t priority);
static process_class_t yeti_mac_get_process_priority(void);

#if USE_SYNC_MAC
/*Timers callback functions*/
static void time_apply_sync_func(void const * argument);
static void sync_disc_func(void const * argument);

/* Yetimac process */
static void yetimac_process(void const * argument);

/*Internal aux functions*/
static retval_t rcv_sync_packet(net_packet_t* packet);
static retval_t send_packet_from_buff(uint16_t packet_num);
static retval_t send_packet_beacon_from_buff(uint16_t packet_num);
static uint16_t check_packet_beacon_sent(net_packet_t* packet);
static uint16_t is_packet_beacon(net_packet_t* net_packet, uint16_t size);
static retval_t yeti_mac_resend_flood_packet(net_packet_t* packet);
#endif

mac_layer_funcs_t yeti_mac_funcs = {
		.mac_layer_init = yeti_mac_layer_init,
		.mac_layer_deinit = yeti_mac_layer_deinit,
		.mac_send_packet = yeti_mac_send_packet,
		.mac_rcv_packet = yeti_mac_rcv_packet,
		.mac_send_packet_done = yeti_mac_send_packet_done,
		.mac_rcv_packet_done = yeti_mac_rcv_packet_done,
		.mac_set_addr = yeti_mac_set_addr,
		.mac_new_addr = yeti_mac_new_addr,
		.mac_delete_addr = yeti_mac_delete_addr,
		.mac_addr_to_string = yeti_mac_addr_to_string,
		.mac_addr_cpy = yeti_mac_addr_cpy,
		.mac_addr_cmp = yeti_mac_addr_cmp,
		.mac_set_node_as_gw = yeti_mac_set_node_as_gw,
		.mac_is_node_linked = yeti_mac_is_node_linked,
		.mac_set_duty_cycle = yeti_mac_set_duty_cycle,
		.mac_set_process_priority = yeti_mac_set_process_priority,
		.mac_get_process_priority = yeti_mac_get_process_priority,
};




/**
 *
 * @return
 */
static retval_t yeti_mac_layer_init(void){
	if(yeti_mac_data != NULL){
		return RET_ERROR;
	}
	if((yeti_mac_data = (yeti_mac_layer_data_t*) ytMalloc(sizeof(yeti_mac_layer_data_t))) == NULL){
		return RET_ERROR;
	}

#if USE_32_BITS_UNIQUE_ID
	yeti_mac_data->node_mac_addr.mac_val = get_uinque_32_bits_id();
#else
	new_data->node_mac_addr.mac_val = DEFAULT_MAC_ADDR;
#endif
#if USE_SYNC_MAC
	uint16_t i;

	yeti_mac_data->yeti_mac_config.mac_beacon_listen_on_time = ((MAC_BEACON_TX_SLOT_TIME*NUM_TX_SLOTS)+(2*MAC_RDC_GUARD));
	yeti_mac_data->yeti_mac_config.mac_duty_cycle = MAC_DEFAULT_RADIO_DUTY_CYCLE;
	yeti_mac_data->yeti_mac_config.mac_rdc_period = ((yeti_mac_data->yeti_mac_config.mac_beacon_listen_on_time*100)/yeti_mac_data->yeti_mac_config.mac_duty_cycle);
	yeti_mac_data->yeti_mac_config.mac_packet_tx_slot_time = (yeti_mac_data->yeti_mac_config.mac_rdc_period - yeti_mac_data->yeti_mac_config.mac_beacon_listen_on_time - (2*MAC_RDC_GUARD))/NUM_TX_SLOTS;
	yeti_mac_data->sync_disc_timer = ytTimerCreate(ytTimerPeriodic, sync_disc_func, NULL);
	yeti_mac_data->last_sync_timestamp = 0;
	yeti_mac_data->is_node_gw = 0;
	yeti_mac_data->node_sync_level = 0;
	yeti_mac_data->sync_time_offset = NO_SYNC_TIME_OFFSET;
	yeti_mac_data->sending_sync_time = 0;
	ytTimerStart(yeti_mac_data->sync_disc_timer, SYNC_PERIOD);


	yeti_mac_data->tx_packets_buffer_head_ptr = ((&(yeti_mac_data->tx_packets_buffer[1])) + (rand()%(PACKETS_TO_SEND_BUFFER_SIZE-1)));		//Initially sets the buffer head randomly
	yeti_mac_data->num_packets_in_buffer = 0;
	yeti_mac_data->current_slot_number = 0;
	yeti_mac_data->num_beacon_addresses_sent = 0;

	yeti_mac_data->current_state = RX_PACKETS;
	yeti_mac_data->is_node_sync = 0;

	yeti_mac_data->tx_mutex_id = ytMutexCreate();
	yeti_mac_data->sync_semaphore_id = ytSemaphoreCreate(1);
	ytSemaphoreWait(yeti_mac_data->sync_semaphore_id, YT_WAIT_FOREVER);
	yeti_mac_data->num_times_func = 0;

	for(i=0; i<PACKETS_TO_SEND_BUFFER_SIZE; i++){
		yeti_mac_data->tx_packets_buffer[i] = NULL;
	}

	ytStartProcess("apply_sync", time_apply_sync_func, VERY_HIGH_PRIORITY_PROCESS, 160, &yeti_mac_data->apply_timestamp_proc_id, NULL);
	ytStartProcess("Yetimac_Process", yetimac_process, YETIMAC_LAYER_PROCESS_PRIORITY, 256, &yeti_mac_data->yetimac_process_id, NULL);

#endif
	phy_set_mode_receiving();			//Initially MAC allways receiving
	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @return
 */
static retval_t yeti_mac_layer_deinit(void){
	if(yeti_mac_data == NULL){
		return RET_ERROR;
	}
#if USE_SYNC_MAC
	ytExitProcess(yeti_mac_data->yetimac_process_id);
	ytExitProcess(yeti_mac_data->apply_timestamp_proc_id);
	ytSemaphoreDelete(yeti_mac_data->sync_semaphore_id);
	ytTimerStop(yeti_mac_data->sync_disc_timer);
	ytTimerDelete(yeti_mac_data->sync_disc_timer);
	ytMutexDelete(yeti_mac_data->tx_mutex_id);
#endif
	ytFree(yeti_mac_data);
	yeti_mac_data = NULL;
	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @param mac_addr_dest_fd
 * @param packet
 * @return
 */
static retval_t yeti_mac_send_packet(mac_addr_t mac_addr_dest_fd, net_packet_t* packet){
	uint16_t i;
	sync_packet_t* sync_packet = (sync_packet_t*) &(packet->data[0]);
	yeti_mac_addr_t*  dest_mac_addr = (yeti_mac_addr_t*) mac_addr_dest_fd;
	if(yeti_mac_data == NULL){
		return RET_ERROR;
	}
	if(dest_mac_addr->mac_val == DEFAULT_MAC_FLOOD_ADDR){
		flood_packet_t* flood_packet = (flood_packet_t*) &(packet->data[PACKET_DATA_SIZE-sizeof(flood_packet_t)]);
		flood_packet->num_hops = 0;
		flood_packet->src_addreses[flood_packet->num_hops].mac_val = yeti_mac_data->node_mac_addr.mac_val;
	}
	packet->header.mac_hdr.dest_mac_addr = dest_mac_addr->mac_val;
	packet->header.mac_hdr.src_mac_addr = yeti_mac_data->node_mac_addr.mac_val;

#if USE_SYNC_MAC
	if((!yeti_mac_data->is_node_sync) && (!yeti_mac_data->is_node_gw)){			//If the node is not synchronized in the network send inmediatelly
		if((sync_packet->sync_packet_code == SYNC_PACKET_CODE) && (sync_packet->sync_packet_type == SYNC_MSG_DISC)){//An unsynchronized node should not sent a sync packet
			return RET_ERROR;
		}
	}

	if(dest_mac_addr->mac_val == DEFAULT_MAC_FLOOD_ADDR || dest_mac_addr->mac_val == DEFAULT_MAC_BC_ADDR){	//In this case write only in the first position of the buffer
		if(yeti_mac_data->tx_packets_buffer[0] != NULL){	//The slot is full so it cannot be sent
			return RET_ERROR;								//Unable to send the packet if there is no available place in the buffer
		}
		else{
			ytMutexWait(yeti_mac_data->tx_mutex_id, YT_WAIT_FOREVER);	//Lock to prevent sending packets while modifying tx buffers (and opposite)
			yeti_mac_data->tx_packets_buffer[0] = packet;
			yeti_mac_data->num_packets_in_buffer++;
			ytMutexRelease(yeti_mac_data->tx_mutex_id);
			return RET_OK;
		}
	}
	else{
		if(yeti_mac_data->num_packets_in_buffer < PACKETS_TO_SEND_BUFFER_SIZE){

			ytMutexWait(yeti_mac_data->tx_mutex_id, YT_WAIT_FOREVER);	//Lock to prevent sending packets while modifying tx buffers (and opposite)
			for(i=0; i<PACKETS_TO_SEND_BUFFER_SIZE-1; i++ ){//In some situations fragmentation in the buffer may appear. Check an available slot in the buffer. -1 as the first slot is reserved for broadcast
				if(*(yeti_mac_data->tx_packets_buffer_head_ptr) != NULL){
					yeti_mac_data->tx_packets_buffer_head_ptr++;
					if(yeti_mac_data->tx_packets_buffer_head_ptr >= &(yeti_mac_data->tx_packets_buffer[PACKETS_TO_SEND_BUFFER_SIZE])){
						yeti_mac_data->tx_packets_buffer_head_ptr = &(yeti_mac_data->tx_packets_buffer[1]);
					}
				}
				else{
					break;
				}
			}
			if(*(yeti_mac_data->tx_packets_buffer_head_ptr) != NULL){
				ytMutexRelease(yeti_mac_data->tx_mutex_id);
				return RET_ERROR; //No available space
			}

			*(yeti_mac_data->tx_packets_buffer_head_ptr) = packet;

			yeti_mac_data->tx_packets_buffer_head_ptr++;
			if(yeti_mac_data->tx_packets_buffer_head_ptr >= &(yeti_mac_data->tx_packets_buffer[PACKETS_TO_SEND_BUFFER_SIZE])){
				yeti_mac_data->tx_packets_buffer_head_ptr = &(yeti_mac_data->tx_packets_buffer[1]);
			}

			yeti_mac_data->num_packets_in_buffer++;

			ytMutexRelease(yeti_mac_data->tx_mutex_id);
			return RET_OK;
		}
		else{
			return RET_ERROR;	//Unable to send the packet if there is no available place in the buffer
		}
	}

#else
	return phy_send_packet(packet, STANDARD_PCKT_LENGTH);
#endif

}

/**
 *
 * @param mac_layer_data
 * @return
 */
static retval_t yeti_mac_rcv_packet(void){
	if(yeti_mac_data == NULL){
		return RET_ERROR;
	}
	//Nothing to do when using sync MAC. Duty cycling must not be controlled by upper layers
#if !USE_SYNC_MAC
	phy_set_mode_receiving();
#endif
	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @param packet
 * @return
 */
static retval_t yeti_mac_send_packet_done(net_packet_t* packet, uint16_t size){
	sync_packet_t* sync_packet  = (sync_packet_t*) &(packet->data[0]);
	if(yeti_mac_data == NULL){
		return RET_ERROR;
	}
#if USE_SYNC_MAC

	if(((yeti_mac_data->current_state== RX_PACKETS) || (yeti_mac_data->current_state == RX_TX_PACKETS)) && (!yeti_mac_data->go_to_receive)){
#if GW_ALLWAYS_ON
		if((!yeti_mac_data->is_node_gw) && yeti_mac_data->is_node_sync && (!yeti_mac_data->sending_sync_time)){		//Gateway or unsync node never sleeps
			phy_set_mode_sleep();	//Sleep mode
		}
#else
		if(!(yeti_mac_data->is_node_sync) && yeti_mac_data->is_node_gw && (!yeti_mac_data->sending_sync_time)){		//Gateway or unsync node never sleeps
			phy_set_mode_sleep();	//Sleep mode
		}
#endif
		else{
			phy_set_mode_receiving();
		}
	}
	else{
		phy_set_mode_receiving();
	}

	if(is_packet_beacon(packet, size)){
		ytFree(packet);	//Free the beacon
		return RET_OK;
	}
	if(sync_packet->sync_packet_code != SYNC_PACKET_CODE){
		return rt_packet_sent(packet);
	}
	else{		//If it is a sync packet release it
		return tx_packetbuffer_release_packet(packet);
	}

#else
	rt_packet_sent(packet);
	phy_set_mode_receiving();
#endif
	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @param packet
 * @return
 */
static retval_t yeti_mac_rcv_packet_done(net_packet_t* packet, uint16_t size){
	yeti_mac_addr_t* yeti_mac_addr;
	net_packet_t* re_send_packet;
	flood_packet_t* flood_packet;
	sync_packet_t* sync_packet =(sync_packet_t*) &(packet->data[0]);
	uint16_t i;
	uint8_t resend = 1;
	if(yeti_mac_data == NULL){
		return RET_ERROR;
	}
#if USE_SYNC_MAC

		phy_set_mode_receiving();	//Continue listening till the mac rx state changes to sleep

		if(is_packet_beacon(packet, size)){					//In RX mode a beacon could be received , so RX state must be continued
			beacon_packet_t* beacon = (beacon_packet_t*) packet;
			if((beacon->dest_mac_addr == yeti_mac_data->node_mac_addr.mac_val) || (beacon->dest_mac_addr == DEFAULT_MAC_BC_ADDR) || (beacon->dest_mac_addr == DEFAULT_MAC_FLOOD_ADDR)){
					yeti_mac_data->go_to_receive = 1;
			}

			return RET_OK;
		}

		//Check if the packet is for me. If it is, process it
		else if((packet->header.mac_hdr.dest_mac_addr == DEFAULT_MAC_BC_ADDR) || (packet->header.mac_hdr.dest_mac_addr == yeti_mac_data->node_mac_addr.mac_val) || (packet->header.mac_hdr.dest_mac_addr == DEFAULT_MAC_FLOOD_ADDR)){

			if((packet->header.mac_hdr.dest_mac_addr == DEFAULT_MAC_FLOOD_ADDR)){	//If it is a flood message try to resend

				yeti_mac_addr = (yeti_mac_addr_t*) ytMalloc(sizeof(yeti_mac_addr_t));	//To the upper layer
				yeti_mac_addr->mac_val = packet->header.mac_hdr.src_mac_addr;
				rt_packet_received(packet, (mac_addr_t) yeti_mac_addr);
				ytFree(yeti_mac_addr);

				flood_packet = (flood_packet_t*)&(packet->data[PACKET_DATA_SIZE-sizeof(flood_packet_t)]);
				flood_packet->num_hops++;
				if(flood_packet->num_hops < MAX_FLOOD_HOPS){	//Prevent overflow if the packet is corrupted
					for(i=0; i<flood_packet->num_hops; i++){
						if(flood_packet->src_addreses[i].mac_val == yeti_mac_data->node_mac_addr.mac_val){	//Do not resend if I have already sent a packet
							resend = 0;
							break;
						}
					}

					if(resend){

						if((re_send_packet = tx_packetbuffer_get_free_packet())!= NULL){	//Resend if possible
							memcpy(re_send_packet, packet, FIXED_PACKET_SIZE);
							flood_packet = (flood_packet_t*)&(re_send_packet->data[PACKET_DATA_SIZE-sizeof(flood_packet_t)]);

							flood_packet->src_addreses[flood_packet->num_hops].mac_val = yeti_mac_data->node_mac_addr.mac_val;

							re_send_packet->header.mac_hdr.dest_mac_addr = DEFAULT_MAC_FLOOD_ADDR;
							re_send_packet->header.mac_hdr.src_mac_addr = yeti_mac_data->node_mac_addr.mac_val;


							if(yeti_mac_resend_flood_packet(re_send_packet) != RET_OK){ //mac_send synchronously
								tx_packetbuffer_release_packet(re_send_packet);
							}

						}
					}
				}

			}
			else{
				if(sync_packet->sync_packet_code == SYNC_PACKET_CODE){			//Received a sync_packet. Process it
					rcv_sync_packet(packet);
				}
				else{
					yeti_mac_addr = (yeti_mac_addr_t*) ytMalloc(sizeof(yeti_mac_addr_t));	//To the upper layer
					yeti_mac_addr->mac_val = packet->header.mac_hdr.src_mac_addr;
					rt_packet_received(packet, (mac_addr_t) yeti_mac_addr);
					ytFree(yeti_mac_addr);
				}
			}
		}

#else

	phy_set_mode_receiving();	//Continue listening till the mac rx state changes to sleep

	//Check if the packet is for me. If it is process it
	if((packet->header.mac_hdr.dest_mac_addr == DEFAULT_MAC_BC_ADDR) || (packet->header.mac_hdr.dest_mac_addr == yeti_mac_data->node_mac_addr.mac_val) || (packet->header.mac_hdr.dest_mac_addr == DEFAULT_MAC_FLOOD_ADDR)){

		if((packet->header.mac_hdr.dest_mac_addr == DEFAULT_MAC_FLOOD_ADDR)){	//If it is a flood message try to resend

			yeti_mac_addr = (yeti_mac_addr_t*) ytMalloc(sizeof(yeti_mac_addr_t));	//To the upper layer
			yeti_mac_addr->mac_val = packet->header.mac_hdr.src_mac_addr;
			rt_packet_received(packet, (mac_addr_t) yeti_mac_addr);
			ytFree(yeti_mac_addr);

			flood_packet = (flood_packet_t*)&(packet->data[PACKET_DATA_SIZE-sizeof(flood_packet_t)]);
			flood_packet->num_hops++;
			if(flood_packet->num_hops < MAX_FLOOD_HOPS){	//Prevent overflow if the packet is corrupted
				for(i=0; i<flood_packet->num_hops; i++){
					if(flood_packet->src_addreses[i].mac_val == yeti_mac_data->node_mac_addr.mac_val){	//Do not resend if I have already sent a packet
						resend = 0;
						break;
					}
				}

				if(resend){

					if((re_send_packet = tx_packetbuffer_get_free_packet())!= NULL){	//Resend if possible
						memcpy(re_send_packet, packet, FIXED_PACKET_SIZE);
						flood_packet = (flood_packet_t*)&(re_send_packet->data[PACKET_DATA_SIZE-sizeof(flood_packet_t)]);

						flood_packet->src_addreses[flood_packet->num_hops].mac_val = yeti_mac_data->node_mac_addr.mac_val;

						re_send_packet->header.mac_hdr.dest_mac_addr = DEFAULT_MAC_FLOOD_ADDR;
						re_send_packet->header.mac_hdr.src_mac_addr = yeti_mac_data->node_mac_addr.mac_val;

						ytDelay((((uint32_t)rand())%FLOOD_MAX_RANDOM_DELAY));		//Random delay to prevent packet interferences

						if(phy_send_packet(re_send_packet, STANDARD_PCKT_LENGTH) != RET_OK){
							tx_packetbuffer_release_packet(re_send_packet);
						}

					}
				}
			}
		}

		else{
			yeti_mac_addr = (yeti_mac_addr_t*) ytMalloc(sizeof(yeti_mac_addr_t));	//To the upper layer
			yeti_mac_addr->mac_val = packet->header.mac_hdr.src_mac_addr;
			rt_packet_received(packet, (mac_addr_t) yeti_mac_addr);
			ytFree(yeti_mac_addr);
		}
	}

	else{	//Packet not for me
		return RET_ERROR;
	}
#endif
	return RET_OK;
}


#if USE_SYNC_MAC
static retval_t rcv_sync_packet(net_packet_t* packet){
uint64_t lapsed_time_since_sent = 0;
net_packet_t* re_send_packet;
sync_packet_t* re_send_sync_packet;
sync_packet_t* sync_packet = (sync_packet_t*) &(packet->data[0]);

	if(sync_packet->sync_packet_type == SYNC_MSG_DISC){	//If a receive a sync discovery message, I update my timestamp and answer to the sender

		if(!yeti_mac_data->is_node_gw){	//GW should not Set never its timestamp
			if(!yeti_mac_data->last_sync_timestamp){	//Only sync if no other node is syncing
				if(!yeti_mac_data->node_sync_level){	//No sync level stablished yet
					yeti_mac_data->node_sync_level = sync_packet->sync_level +1;
				}
				if(sync_packet->sync_level < yeti_mac_data->node_sync_level){				//Only sync if the received packet comes from a lower level sync node (nearer to the gw)
//					memcpy(&sync_timestamp, &packet->data[SYNC_PACKET_TIMESTAMP_OFFSET], sizeof(uint64_t));

					ytProcessSuspendAll();		//Critical section to prevent delays
					//Subseconds are not set by the SetTimestamp function, so this apply sync function must be called with a second precission
					if(yeti_mac_data->sync_time_offset != NO_SYNC_TIME_OFFSET){
						lapsed_time_since_sent = (uint64_t) (ytGetSysTickMilliSec() - packet->rcv_tick_time + yeti_mac_data->sync_time_offset);
					}
					else{
						lapsed_time_since_sent = (uint64_t) (ytGetSysTickMilliSec() - packet->rcv_tick_time + DEFAULT_SYNC_TIME_OFFSET);
					}

					if(lapsed_time_since_sent > MAX_DELAY_ALLOWED_SYNC_PACKET){
						ytProcessResumeAll();
						return RET_ERROR;
					}

					//TRY WITH VOLATILE IF DONT WORKS WHEN USING COMPILE OPTIMIZATION
					yeti_mac_data->last_sync_proc_delay = (uint32_t)(1000 - ((sync_packet->sync_timestamp+lapsed_time_since_sent)%1000));	//Remaining time to the next second
					yeti_mac_data->last_sync_timestamp = ((((sync_packet->sync_timestamp+lapsed_time_since_sent)/1000)+1)*1000);	//Update in the next second
					yeti_mac_data->node_sync_level = sync_packet->sync_level +1;
					ytSemaphoreRelease(yeti_mac_data->sync_semaphore_id);
					ytProcessResumeAll();

					//Now resend an ack packet with my timestamp if my time offset is not set yet (the time offset is obtained only once)
					if(yeti_mac_data->sync_time_offset == NO_SYNC_TIME_OFFSET){	//Time offset not set yet
						if((re_send_packet = tx_packetbuffer_get_free_packet())!= NULL){	//Resend if possible
							re_send_sync_packet =(sync_packet_t*) &(re_send_packet->data[0]);
							re_send_sync_packet->sync_packet_type = SYNC_MSG_ACK;
							re_send_sync_packet->sync_packet_code = SYNC_PACKET_CODE;
							re_send_sync_packet->sync_level = yeti_mac_data->node_sync_level;	//Timestamp is set when it is actually sent
							re_send_sync_packet->sync_tx_offset_time = NO_SYNC_TIME_OFFSET;

							re_send_packet->header.mac_hdr.dest_mac_addr = packet->header.mac_hdr.src_mac_addr;		//Dest is the source
							re_send_packet->header.mac_hdr.src_mac_addr = yeti_mac_data->node_mac_addr.mac_val;

							yeti_mac_addr_t yeti_mac_addr;
							yeti_mac_addr.mac_val = packet->header.mac_hdr.src_mac_addr;
							if(yeti_mac_send_packet((mac_addr_t) &yeti_mac_addr, re_send_packet) != RET_OK){ //mac_send synchronously
								tx_packetbuffer_release_packet(re_send_packet);
							}

						}
					}

				}
			}
		}

	}
	else if(sync_packet->sync_packet_type == SYNC_MSG_ACK){	//I receive a sync ack. Calculate the time offset and resend

		uint64_t my_timestamp = ytTimeGetTimestamp();
		if(my_timestamp > sync_packet->sync_timestamp){		//The received timestamp must be lower to my current timestamp always
			//Now resend a sync packet with the offset value calculated
			if((re_send_packet = tx_packetbuffer_get_free_packet())!= NULL){	//Resend if possible
				re_send_sync_packet = (sync_packet_t*) &(re_send_packet->data[0]);
				re_send_sync_packet->sync_packet_type = SYNC_MSG_SET_TIME;
				re_send_sync_packet->sync_packet_code = SYNC_PACKET_CODE;
				re_send_sync_packet->sync_level = yeti_mac_data->node_sync_level;	//Timestamp is set when it is actually sent
				re_send_sync_packet->sync_tx_offset_time = (uint32_t)((my_timestamp - sync_packet->sync_timestamp)/2);

				re_send_packet->header.mac_hdr.dest_mac_addr = packet->header.mac_hdr.src_mac_addr;		//Dest is the source
				re_send_packet->header.mac_hdr.src_mac_addr = yeti_mac_data->node_mac_addr.mac_val;
				yeti_mac_addr_t yeti_mac_addr;
				yeti_mac_addr.mac_val = packet->header.mac_hdr.src_mac_addr;
				if(yeti_mac_send_packet((mac_addr_t) &yeti_mac_addr, re_send_packet) != RET_OK){ //mac_send synchronously
					tx_packetbuffer_release_packet(re_send_packet);
				}
			}

		}

	}

	else if(sync_packet->sync_packet_type == SYNC_MSG_SET_TIME){	//I receive a set time message. Set the offset time and apply sync
		if(!yeti_mac_data->is_node_gw){	//GW should not Set never its timestamp
			if(!yeti_mac_data->last_sync_timestamp){	//Only sync if no other node is syncing
				if(!yeti_mac_data->node_sync_level){	//No sync level stablished yet
					yeti_mac_data->node_sync_level = sync_packet->sync_level +1;
				}
				if(sync_packet->sync_level < yeti_mac_data->node_sync_level){				//Only sync if the received packet comes from a lower level sync node (nearer to the gw)
					if(sync_packet->sync_tx_offset_time < 50){			//larger values have no sense
						yeti_mac_data->sync_time_offset = sync_packet->sync_tx_offset_time;		//Set my new time offset for synchronization
					}
					ytProcessSuspendAll();		//Critical section to prevent delays
					//Subseconds are not set by the SetTimestamp function, so this apply sync function must be called with a second precission
					if(yeti_mac_data->sync_time_offset != NO_SYNC_TIME_OFFSET){
						lapsed_time_since_sent = (uint64_t) (ytGetSysTickMilliSec() - packet->rcv_tick_time + yeti_mac_data->sync_time_offset);
					}
					else{
						lapsed_time_since_sent = (uint64_t) (ytGetSysTickMilliSec() - packet->rcv_tick_time + DEFAULT_SYNC_TIME_OFFSET);
					}

					if(lapsed_time_since_sent > MAX_DELAY_ALLOWED_SYNC_PACKET){
						ytProcessResumeAll();
						return RET_ERROR;
					}

					//TRY WITH VOLATILE IF DONT WORKS WHEN USING COMPILE OPTIMIZATION
					yeti_mac_data->last_sync_proc_delay = (uint32_t)(1000 - ((sync_packet->sync_timestamp+lapsed_time_since_sent)%1000));	//Remaining time to the next second
					yeti_mac_data->last_sync_timestamp = ((((sync_packet->sync_timestamp+lapsed_time_since_sent)/1000)+1)*1000);	//Update in the next second
					yeti_mac_data->node_sync_level = sync_packet->sync_level +1;
					ytSemaphoreRelease(yeti_mac_data->sync_semaphore_id);
					ytProcessResumeAll();
				}
			}
		}
	}
	else{
		return RET_ERROR;
	}
	return RET_OK;

}

#endif
/**
 *
 * @param mac_layer_data
 * @param mac_addr_fd
 * @return
 */
static retval_t yeti_mac_set_addr(mac_addr_t mac_addr_fd){
	yeti_mac_addr_t*  dest_mac_addr = (yeti_mac_addr_t*) mac_addr_fd;
	if(yeti_mac_data == NULL){
		return RET_ERROR;
	}
	if((dest_mac_addr->mac_val == DEFAULT_MAC_BC_ADDR) || (dest_mac_addr->mac_val == DEFAULT_MAC_FLOOD_ADDR)){
		return RET_ERROR;
	}
	yeti_mac_data->node_mac_addr.mac_val = dest_mac_addr->mac_val;

	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @param mac_str_val
 * @param mac_addr_fd
 * @return
 */
static retval_t yeti_mac_new_addr(char* mac_str_val, char format, mac_addr_t* mac_addr_fd){
	yeti_mac_addr_t* yeti_mac_addr;
	uint32_t addr_val;
	(*mac_addr_fd) = NULL;
	if(mac_addr_fd == NULL){
		return RET_ERROR;
	}
	if((mac_str_val == NULL) || (format == 0)){
		return RET_ERROR;
	}
	else{
		if((format == 'D') || (format == 'd')){
			if(strlen(mac_str_val)>10){	//Max addr value 4294967296
				return RET_ERROR;
			}
			addr_val = strtoul(mac_str_val, NULL, 10);
						yeti_mac_addr = (yeti_mac_addr_t*) ytMalloc(sizeof(yeti_mac_addr_t));
			yeti_mac_addr->mac_val = addr_val;
			(*mac_addr_fd) = (mac_addr_t) yeti_mac_addr;

		}
		else if((format == 'H') || (format == 'h')){
			if(strlen(mac_str_val)>10){	//Max addr value 0xFFFFFFFF
				return RET_ERROR;
			}
			if((mac_str_val[0] != '0') || (mac_str_val[1] != 'x')){
				return RET_ERROR;
			}
			addr_val = strtoul(mac_str_val+2, NULL, 16);

			yeti_mac_addr = (yeti_mac_addr_t*) ytMalloc(sizeof(yeti_mac_addr_t));
			yeti_mac_addr->mac_val = addr_val;
			(*mac_addr_fd) = (mac_addr_t) yeti_mac_addr;
		}
		else{
			return RET_ERROR;
		}

	}
	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @param mac_addr_fd
 * @return
 */
static retval_t yeti_mac_delete_addr(mac_addr_t mac_addr_fd){
	if(mac_addr_fd == NULL){
		return RET_ERROR;
	}
	ytFree(mac_addr_fd);
	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @param mac_addr_fd
 * @param mac_str_val
 * @return
 */
static retval_t yeti_mac_addr_to_string(mac_addr_t mac_addr_fd, char* mac_str_val, char format){
	yeti_mac_addr_t* yeti_mac_addr = (yeti_mac_addr_t*) mac_addr_fd;
	if((mac_addr_fd == NULL) || (mac_str_val == NULL)){
		return RET_ERROR;
	}
	if((format == 'D') || (format == 'd')){
		sprintf(mac_str_val, "%ul", (unsigned int) yeti_mac_addr->mac_val);
	}
	else if((format == 'H') || (format == 'h')){
		sprintf(mac_str_val, "0x%08X",(unsigned int) yeti_mac_addr->mac_val);
	}
	else{
		mac_str_val[0] = 'N';
		mac_str_val[1] = 'a';
		mac_str_val[2] = 'N';
		return RET_ERROR;
	}

	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @param dest_addr_fd
 * @param from_addr_fd
 * @return
 */
static retval_t yeti_mac_addr_cpy(mac_addr_t dest_addr_fd, mac_addr_t from_addr_fd){
	yeti_mac_addr_t* yeti_mac_addr_dest = (yeti_mac_addr_t*) dest_addr_fd;
	yeti_mac_addr_t* yeti_mac_addr_from = (yeti_mac_addr_t*) from_addr_fd;
	if((dest_addr_fd == NULL) || (from_addr_fd == NULL)){
		return RET_ERROR;
	}
	yeti_mac_addr_dest->mac_val = yeti_mac_addr_from->mac_val;
	return RET_OK;
}

/**
 *
 * @param mac_layer_data
 * @param a_addr_fd
 * @param b_addr_fd
 * @return
 */
static uint16_t yeti_mac_addr_cmp(mac_addr_t a_addr_fd, mac_addr_t b_addr_fd){
	yeti_mac_addr_t* yeti_mac_addr_a = (yeti_mac_addr_t*) a_addr_fd;
	yeti_mac_addr_t* yeti_mac_addr_b = (yeti_mac_addr_t*) b_addr_fd;
	if((a_addr_fd == NULL) || (b_addr_fd == NULL)){
		return 0;
	}
	if(yeti_mac_addr_a->mac_val == yeti_mac_addr_b->mac_val){
		return 1;
	}
	return 0;
}


/**
 *
 * @return
 */
static retval_t yeti_mac_set_node_as_gw(void){
	if(yeti_mac_data == NULL){
		return RET_ERROR;
	}
#if USE_SYNC_MAC
	yeti_mac_data->is_node_gw = 1;
	yeti_mac_data->node_sync_level = 0;

#endif
	return RET_OK;
}

/**
 *
 * @return
 */
static uint16_t yeti_mac_is_node_linked(void){
#if USE_SYNC_MAC
	if(yeti_mac_data->is_node_gw){
		return 1;
	}
	if(yeti_mac_data->is_node_sync){
		return 1;
	}
	return 0;
#else
	return 1;
#endif
}

/**
 *
 * @param duty_cycle
 * @return
 */
static retval_t yeti_mac_set_duty_cycle(uint32_t duty_cycle){
#if USE_SYNC_MAC
	uint32_t mac_packet_tx_slot_time;
	uint32_t mac_rdc_period;
	if(duty_cycle > 100 ){
		return RET_ERROR;
	}
	mac_rdc_period =((yeti_mac_data->yeti_mac_config.mac_beacon_listen_on_time*100)/duty_cycle);
	mac_packet_tx_slot_time = (mac_rdc_period - yeti_mac_data->yeti_mac_config.mac_beacon_listen_on_time - (2*MAC_RDC_GUARD))/NUM_TX_SLOTS;
	if (mac_packet_tx_slot_time < MAC_MIN_PACKET_TX_SLOT_TIME){
		return RET_ERROR;
	}
	yeti_mac_data->yeti_mac_config.mac_duty_cycle = duty_cycle;
	yeti_mac_data->yeti_mac_config.mac_rdc_period = mac_rdc_period;
	yeti_mac_data->yeti_mac_config.mac_packet_tx_slot_time = mac_packet_tx_slot_time;

	return RET_OK;
#endif
	return RET_ERROR;
}

/**
 *
 * @param process_id
 * @param priority
 * @return
 */
static retval_t yeti_mac_set_process_priority(process_class_t priority){
#if USE_SYNC_MAC
	return ytSetProcessPriority(yeti_mac_data->yetimac_process_id, priority);
#endif
	return RET_ERROR;
}

/**
 *
 * @param process_id
 * @return
 */
static process_class_t yeti_mac_get_process_priority(void){
#if USE_SYNC_MAC
	return ytGetProcessPriority(yeti_mac_data->yetimac_process_id);
#endif
	return VERY_LOW_PRIORITY_PROCESS;
}

#if USE_SYNC_MAC
/**
 *
 * @param argument
 */
static void sync_disc_func(void const * argument){
	net_packet_t* packet;
	sync_packet_t* sync_packet;
	yeti_mac_addr_t yeti_mac_addr;
	if((yeti_mac_data->is_node_gw == 1) || (yeti_mac_data->node_sync_level > 0)){ //If I am GW or I am Sync with the master I send a sync packet
		if((packet = tx_packetbuffer_get_free_packet()) == NULL){
			return;
		}

		packet->header.mac_hdr.src_mac_addr = yeti_mac_data->node_mac_addr.mac_val;
		packet->header.mac_hdr.dest_mac_addr = DEFAULT_MAC_BC_ADDR;
		sync_packet = (sync_packet_t*) &(packet->data[0]);
		sync_packet->sync_packet_type = SYNC_MSG_DISC;
		sync_packet->sync_packet_code = SYNC_PACKET_CODE;
		sync_packet->sync_level = yeti_mac_data->node_sync_level;	//Timestamp is set when it is actually sent
		sync_packet->sync_tx_offset_time = NO_SYNC_TIME_OFFSET;

		yeti_mac_addr.mac_val = DEFAULT_MAC_BC_ADDR;
		yeti_mac_data->sending_sync_time = ytGetSysTickMilliSec();		//Wait in RX mode during SENDING_SYNC_TIMEOUT for calibrating offset times
		if(yeti_mac_send_packet((mac_addr_t) &yeti_mac_addr, packet) != RET_OK){	//Schedule a packet to be sent. The timestamp will be appended when it is actually sent
			yeti_mac_data->sending_sync_time = 0;
			tx_packetbuffer_release_packet(packet);
		}
	}

	if((!yeti_mac_data->is_node_gw) && yeti_mac_data->is_node_sync){	//If I am sync and I am not the GW
		yeti_mac_data->num_times_func ++;
		if (yeti_mac_data->num_times_func > MAX_UNSYNC_TIMES){		//node unsynchronized. Set to listen always
			yeti_mac_data->num_times_func = 0;
			yeti_mac_data->is_node_sync = 0;
			yeti_mac_data->node_sync_level = 0;
			phy_set_mode_receiving();	//RX mode always
		}

	}

}


/**
 *
 * @param argument
 */
static void time_apply_sync_func(void const * argument){

	uint64_t current_timestamp;

	while(1){
		ytSemaphoreWait(yeti_mac_data->sync_semaphore_id, YT_WAIT_FOREVER);

		ytDelay(yeti_mac_data->last_sync_proc_delay);


		current_timestamp = ytTimeGetTimestamp();
		if(!yeti_mac_data->is_node_gw){

			if(yeti_mac_data->is_node_sync){	//If I have been previously synchronized
				if(current_timestamp < yeti_mac_data->last_sync_timestamp - TIMESTAMP_SYNC_DIFF){
					ytTimeSetTimestamp(yeti_mac_data->last_sync_timestamp);
				}
				else if(current_timestamp > yeti_mac_data->last_sync_timestamp + TIMESTAMP_SYNC_DIFF){
					yeti_mac_data->last_sync_timestamp = 0;
					return; // Do not update the timestamp if it is too unsynchronized
				}
				else{
					if(yeti_mac_data->last_sync_timestamp != current_timestamp){	//Dont update if it is the same
						ytTimeSetTimestamp(yeti_mac_data->last_sync_timestamp);
					}
				}

				yeti_mac_data->num_times_func = 0;

				yeti_mac_data->last_sync_timestamp = 0;
			}
			else{			//If It is my first synchronization

				ytTimeSetTimestamp(yeti_mac_data->last_sync_timestamp);

				yeti_mac_data->num_times_func = 0;

				if(yeti_mac_data->sync_time_offset != NO_SYNC_TIME_OFFSET){	//I can only be synchornized if the offset is updated
					yeti_mac_data->is_node_sync = 1;
				}

				yeti_mac_data->last_sync_timestamp = 0;
			}

		}
	}

}


static void yetimac_process(void const * argument){
	yetimac_state_t next_state;
	uint32_t current_time, next_slot_time;
	uint32_t delay_time;

	phy_set_mode_receiving();	//RX mode
	current_time = (uint32_t)(ytTimeGetTimestamp()%yeti_mac_data->yeti_mac_config.mac_rdc_period);
	next_state = RX_TX_PACKETS;
	ytDelay(yeti_mac_data->yeti_mac_config.mac_rdc_period - current_time);

	while(1){
		if((ytGetSysTickMilliSec()-yeti_mac_data->sending_sync_time)> SENDING_SYNC_TIMEOUT){
			yeti_mac_data->sending_sync_time = 0;
		}
		yeti_mac_data->current_state = next_state;

		switch (yeti_mac_data->current_state){
			case RX_BEACONS:	//OFFSET TIME. Just hear beacons for RDC OFFSET TIME
				yeti_mac_data->go_to_receive = 0;
				phy_set_mode_receiving();	//RX mode
				next_state = RX_TX_BEACONS;
				current_time = (uint32_t)(ytTimeGetTimestamp()%yeti_mac_data->yeti_mac_config.mac_rdc_period);						//GO TO TX_RX_BEACONS

				if( (current_time + yeti_mac_data->yeti_mac_config.mac_beacon_listen_on_time) < yeti_mac_data->yeti_mac_config.mac_rdc_period + MAC_RDC_GUARD){
					delay_time = (yeti_mac_data->yeti_mac_config.mac_rdc_period + MAC_RDC_GUARD) - current_time - yeti_mac_data->yeti_mac_config.mac_beacon_listen_on_time;		//Wait to the tx_rx_beacons state
				}
				else{
					delay_time = ((2*yeti_mac_data->yeti_mac_config.mac_rdc_period) + MAC_RDC_GUARD) - current_time - yeti_mac_data->yeti_mac_config.mac_beacon_listen_on_time; //Weird case: wait to the tx_rx_beacons state
				}

			break;

			case RX_TX_BEACONS:
				ytMutexWait(yeti_mac_data->tx_mutex_id, YT_WAIT_FOREVER);	//Lock to prevent sending packets while modifying tx buffers (and opposite)
				if(!yeti_mac_data->num_packets_in_buffer){	//If there are no packets to send, wait till the rx or sleep time
					phy_set_mode_receiving();	//RX mode
					next_state = RX_PACKETS;
					current_time = (uint32_t)(ytTimeGetTimestamp()%yeti_mac_data->yeti_mac_config.mac_rdc_period);
					delay_time = yeti_mac_data->yeti_mac_config.mac_rdc_period - current_time;
				}
				else{

					if(yeti_mac_data->current_slot_number < NUM_TX_SLOTS){	//Send the next beacon

						send_packet_beacon_from_buff(yeti_mac_data->current_slot_number);
						yeti_mac_data->current_slot_number++;
					}


					next_slot_time = ((yeti_mac_data->yeti_mac_config.mac_rdc_period - ((NUM_TX_SLOTS-yeti_mac_data->current_slot_number)*MAC_BEACON_TX_SLOT_TIME) - MAC_RDC_GUARD)%yeti_mac_data->yeti_mac_config.mac_rdc_period);
					current_time = (uint32_t)(ytTimeGetTimestamp()%yeti_mac_data->yeti_mac_config.mac_rdc_period);

					if(yeti_mac_data->current_slot_number >= NUM_TX_SLOTS){		//Stop sending beacons. Go to RX packets (or Sleep)
						yeti_mac_data->current_slot_number = 0;
						next_state = RX_PACKETS;
						delay_time = yeti_mac_data->yeti_mac_config.mac_rdc_period - current_time;			//Wait to the rx_packets state
					}

					else{
						if(next_slot_time > current_time){
							delay_time = next_slot_time - current_time; //Wait to the next slot. Continue in RX_TX_Beacons state
						}
						else{			//Strange case. Go to RX Packets and bypass RX_TX beacons state
							yeti_mac_data->current_slot_number = 0;
							next_state = RX_PACKETS;
							delay_time = yeti_mac_data->yeti_mac_config.mac_rdc_period - current_time;
						}

					}

				}
				ytMutexRelease(yeti_mac_data->tx_mutex_id);
			break;

			case RX_PACKETS:	//OFFSET TIME. Just hear packets for RDC OFFSET TIME

				current_time = (uint32_t)(ytTimeGetTimestamp()%yeti_mac_data->yeti_mac_config.mac_rdc_period);

				if(current_time != 0){	//Used to prevent errors due to tiny un-synchronization
					if(current_time >= (yeti_mac_data->yeti_mac_config.mac_rdc_period - yeti_mac_data->yeti_mac_config.mac_beacon_listen_on_time)){	//Beacon time has not ended yet
						delay_time = yeti_mac_data->yeti_mac_config.mac_rdc_period - current_time;	//Wait to end it
						break;
					}
				}
				if(yeti_mac_data->go_to_receive){
					phy_set_mode_receiving();	//RX mode
				}
				else{
#if GW_ALLWAYS_ON
					if((!yeti_mac_data->is_node_gw) && yeti_mac_data->is_node_sync && (!yeti_mac_data->sending_sync_time)){		//Gateway or unsync node never sleeps
						phy_set_mode_sleep();	//Sleep mode
					}
#else
					if(!(yeti_mac_data->is_node_sync) && yeti_mac_data->is_node_gw && (!yeti_mac_data->sending_sync_time)){		//Gateway or unsync node never sleeps
						phy_set_mode_sleep();	//Sleep mode
					}
#endif

					else{
						phy_set_mode_receiving();	//RX mode
					}
				}

				next_state = RX_TX_PACKETS;
				if(current_time < MAC_RDC_GUARD){
					delay_time = (MAC_RDC_GUARD-current_time) + (rand()%(yeti_mac_data->yeti_mac_config.mac_packet_tx_slot_time-MAC_MIN_PACKET_TX_SLOT_TIME));				//Go to RX_TX PACKETS. First packet
				}
				else{
					delay_time = (yeti_mac_data->yeti_mac_config.mac_rdc_period - current_time + MAC_RDC_GUARD) + (rand()%(yeti_mac_data->yeti_mac_config.mac_packet_tx_slot_time-MAC_MIN_PACKET_TX_SLOT_TIME)) ;	//Go to RX_TX PACKETS
				}


				break;

			case RX_TX_PACKETS:

				ytMutexWait(yeti_mac_data->tx_mutex_id, YT_WAIT_FOREVER);	//Lock to prevent sending packets while modifying tx buffers (and opposite)

				if(!yeti_mac_data->num_packets_in_buffer){			//The buffer has been emptied. Go to next beacon time
					next_state = RX_BEACONS;
					yeti_mac_data->current_slot_number = 0;
					yeti_mac_data->num_beacon_addresses_sent = 0;
					current_time = (uint32_t)(ytTimeGetTimestamp()%yeti_mac_data->yeti_mac_config.mac_rdc_period);

					if(yeti_mac_data->yeti_mac_config.mac_rdc_period > current_time + yeti_mac_data->yeti_mac_config.mac_beacon_listen_on_time){
						delay_time = yeti_mac_data->yeti_mac_config.mac_rdc_period - current_time - yeti_mac_data->yeti_mac_config.mac_beacon_listen_on_time;
					}
					else{
						delay_time = (2*yeti_mac_data->yeti_mac_config.mac_rdc_period) - current_time - yeti_mac_data->yeti_mac_config.mac_beacon_listen_on_time;
					}

					ytMutexRelease(yeti_mac_data->tx_mutex_id);
					break;
				}

				if(yeti_mac_data->current_slot_number < NUM_TX_SLOTS){	//Send the next packet

					send_packet_from_buff(yeti_mac_data->current_slot_number);
					yeti_mac_data->current_slot_number++;
				}

				current_time = (uint32_t)(ytTimeGetTimestamp()%yeti_mac_data->yeti_mac_config.mac_rdc_period);
				next_slot_time = ((yeti_mac_data->current_slot_number*yeti_mac_data->yeti_mac_config.mac_packet_tx_slot_time) + MAC_RDC_GUARD) + (rand()%(yeti_mac_data->yeti_mac_config.mac_packet_tx_slot_time-MAC_MIN_PACKET_TX_SLOT_TIME));

				if(yeti_mac_data->current_slot_number >= NUM_TX_SLOTS){		//Stop sending packets. Go to sending beacons

					yeti_mac_data->current_slot_number = 0;
					yeti_mac_data->num_beacon_addresses_sent = 0;
					next_state = RX_BEACONS;
					if(yeti_mac_data->yeti_mac_config.mac_rdc_period > current_time + yeti_mac_data->yeti_mac_config.mac_beacon_listen_on_time){
						delay_time = yeti_mac_data->yeti_mac_config.mac_rdc_period - current_time - yeti_mac_data->yeti_mac_config.mac_beacon_listen_on_time;
					}
					else{
						delay_time = (2*yeti_mac_data->yeti_mac_config.mac_rdc_period) - current_time - yeti_mac_data->yeti_mac_config.mac_beacon_listen_on_time;
					}

				}

				else{
					if(next_slot_time > current_time){
						delay_time = next_slot_time - current_time;	//Wait to the next slot
					}
					else{		//Strange case. Go to RX Beacons and bypass RX_TX packets state
						yeti_mac_data->current_slot_number = 0;
						yeti_mac_data->num_beacon_addresses_sent = 0;
						next_state = RX_BEACONS;
						if(yeti_mac_data->yeti_mac_config.mac_rdc_period > current_time + yeti_mac_data->yeti_mac_config.mac_beacon_listen_on_time){
							delay_time = yeti_mac_data->yeti_mac_config.mac_rdc_period - current_time - yeti_mac_data->yeti_mac_config.mac_beacon_listen_on_time;
						}
						else{
							delay_time = (2*yeti_mac_data->yeti_mac_config.mac_rdc_period) - current_time - yeti_mac_data->yeti_mac_config.mac_beacon_listen_on_time;
						}
					}
				}

				ytMutexRelease(yeti_mac_data->tx_mutex_id);
				break;

			default:		//This should not happen
				current_time = (uint32_t)(ytTimeGetTimestamp()%yeti_mac_data->yeti_mac_config.mac_rdc_period);
				delay_time = yeti_mac_data->yeti_mac_config.mac_rdc_period - current_time;
				next_state = RX_PACKETS;
				yeti_mac_data->go_to_receive = 1;
				break;

		}

		ytDelay(delay_time);

	}
}

/**
 *
 * @param packet
 * @return
 */
static uint16_t check_packet_beacon_sent(net_packet_t* packet){
	uint16_t i;
	for(i=0; i<yeti_mac_data->num_beacon_addresses_sent; i++){
		if(packet->header.mac_hdr.dest_mac_addr == yeti_mac_data->beacon_addresses_sent[i].mac_val){

			return 1;
		}
	}

	return 0;
}

/**
 *
 * @param packet_num
 * @return
 */
static retval_t send_packet_beacon_from_buff(uint16_t packet_num){	//Send
	beacon_packet_t* beacon;

	if( yeti_mac_data->num_packets_in_buffer){		//Only sends the packet beacon if there are packets available
		if(yeti_mac_data->tx_packets_buffer[packet_num] != NULL){
			if(check_packet_beacon_sent(yeti_mac_data->tx_packets_buffer[packet_num])){	//First check if the packet beacon has been sent
				return RET_ERROR;
			}

			beacon = (beacon_packet_t*) ytMalloc(sizeof(beacon_packet_t));
			beacon->beacon_id = YETIMAC_BEACON_ID;
			beacon->dest_mac_addr = yeti_mac_data->tx_packets_buffer[packet_num]->header.mac_hdr.dest_mac_addr;

			if (phy_send_packet((net_packet_t*)beacon, BEACON_PCKT_LENGTH) != RET_OK){	//Free the packet beacon, as it was unable to be sent
				ytFree(beacon);
				return RET_ERROR;
			}
			else{
				if(yeti_mac_data->num_beacon_addresses_sent < NUM_TX_SLOTS){
					yeti_mac_data->beacon_addresses_sent[yeti_mac_data->num_beacon_addresses_sent].mac_val = beacon->dest_mac_addr;
					yeti_mac_data->num_beacon_addresses_sent++;
				}
			}

			return RET_OK;

		}

		return RET_ERROR;

	}
	else{
		return RET_ERROR;
	}
}


/**
 *
 * @return
 */
static retval_t send_packet_from_buff(uint16_t packet_num){
//	uint64_t sync_timestamp;
//	uint8_t* sync_level;
	sync_packet_t* sync_packet;

	if(yeti_mac_data->num_packets_in_buffer){		//Only sends the packet if it is available
		if(yeti_mac_data->tx_packets_buffer[packet_num] != NULL){
			if(!check_packet_beacon_sent(yeti_mac_data->tx_packets_buffer[packet_num])){	//First check if the packet beacon has been sent
				return RET_ERROR;
			}
			sync_packet = (sync_packet_t*) &(yeti_mac_data->tx_packets_buffer[packet_num]->data[0]);
			if(sync_packet->sync_packet_code == SYNC_PACKET_CODE){
				sync_packet->sync_timestamp = ytTimeGetTimestamp();				//Append the timestamp to the packet when it is actually sent
//				memcpy(&((yeti_mac_data->tx_packets_buffer[packet_num])->data[SYNC_PACKET_TIMESTAMP_OFFSET]), &sync_timestamp, sizeof(uint64_t));
//				sync_level = (uint8_t*) &((yeti_mac_data->tx_packets_buffer[packet_num])->data[SYNC_PACKET_SYNC_LVL_OFFSET]);
//				*sync_level = yeti_mac_data->node_sync_level;
			}
			if (phy_send_packet (yeti_mac_data->tx_packets_buffer[packet_num], STANDARD_PCKT_LENGTH) != RET_OK){	//Send the packet
				return RET_ERROR;
			}

			yeti_mac_data->num_packets_in_buffer--;
			yeti_mac_data->tx_packets_buffer[packet_num] = NULL;	//Free buffer slot

			return RET_OK;
		}

		return RET_ERROR;
	}
	else{
		return RET_ERROR;
	}
}

/**
 *
 * @param net_packet
 * @param pckt_size
 * @return
 */
static uint16_t is_packet_beacon(net_packet_t* net_packet, uint16_t size){

	beacon_packet_t* beacon = (beacon_packet_t*) net_packet;
	if(size == BEACON_PCKT_LENGTH){
		if(beacon->beacon_id == YETIMAC_BEACON_ID){
			return 1;
		}
	}
	return 0;
}

/**
 *
 * @param packet
 * @return
 */
static retval_t yeti_mac_resend_flood_packet(net_packet_t* packet){
	if(yeti_mac_data == NULL){
		return RET_ERROR;
	}


	if((!yeti_mac_data->is_node_sync) && (!yeti_mac_data->is_node_gw)){			//If the node is not synchronized in the network send inmediatelly
		ytDelay((((uint32_t)rand())%FLOOD_MAX_RANDOM_DELAY));		//Random delay to prevent packet interferences
		return phy_send_packet(packet, STANDARD_PCKT_LENGTH);
	}
	else{					//Schedule to send in the correct slot when the node is synchronized. Gateway always sends synchronously

		ytMutexWait(yeti_mac_data->tx_mutex_id, YT_WAIT_FOREVER);	//Lock to prevent sending packets while modifying tx buffers (and opposite)

		if(yeti_mac_data->tx_packets_buffer[0] != NULL){	//The slot is full so it cannot be sent
			ytMutexRelease(yeti_mac_data->tx_mutex_id);
			return RET_ERROR;								//Unable to send the packet if there is no available place in the buffer
		}
		else{
			yeti_mac_data->tx_packets_buffer[0] = packet;
			yeti_mac_data->num_packets_in_buffer++;
			ytMutexRelease(yeti_mac_data->tx_mutex_id);
			return RET_OK;
		}

	}
}
#endif
#endif
