/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * yeti_mac.h
 *
 *  Created on: 11 de may. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file yeti_mac.h
 */
#ifndef YETIOS_CORE_NET_MAC_LAYER_INCLUDE_YETI_MAC_H_
#define YETIOS_CORE_NET_MAC_LAYER_INCLUDE_YETI_MAC_H_

#define YETI_MAC_LAYER yeti_mac


#define DEFAULT_MAC_BC_ADDR 		0xFFFFFFFF			//Broadcast messages use this address
#define DEFAULT_MAC_BC_ADDR_STR 	"0xFFFFFFFF"

#define DEFAULT_MAC_FLOOD_ADDR 		0xFFFFFFFE			//Netflood messages use this address
#define DEFAULT_MAC_FLOOD_ADDR_STR 	"0xFFFFFFFE"

//#define DEFAULT_MAC_SYNC_ADDR 		0xFFFFFFFD			//Sync messages use this address to identify them
//#define DEFAULT_MAC_SYNC_ADDR_STR 	"0xFFFFFFFD"

typedef struct yeti_mac_hdr_{
	uint32_t dest_mac_addr;
	uint32_t src_mac_addr;
}yeti_mac_hdr_t;



#endif /* YETIOS_CORE_NET_MAC_LAYER_INCLUDE_YETI_MAC_H_ */
