/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * system_net_api_func_def.h
 *
 *  Created on: 24 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file system_net_api_func_def.h
 */
#ifndef APPLICATION_CORE_SYSTEM_NET_API_FUNC_DEF_H_
#define APPLICATION_CORE_SYSTEM_NET_API_FUNC_DEF_H_


/* *********************************************************/
/* ****************UDP Like Functions***********************/
/* *********************************************************/
/**
 *
 * @param port_number
 * @return
 */
API_FUNC retval_t ytUrOpen(uint16_t port_number){
	return net_uc_open(port_number);
}
/**
 *
 * @param port_number
 * @return
 */
API_FUNC retval_t ytUrClose(uint16_t port_number){
	return net_uc_close(port_number);
}
/**
 *
 * @param port_number
 * @param dest_addr_fd
 * @param data
 * @param size
 * @return
 */
API_FUNC uint32_t ytUrSend(uint16_t port_number, ytNetAddr_t dest_addr_fd, uint8_t* data, uint32_t size){
	return net_uc_send(port_number, dest_addr_fd, data, size);
}
/**
 *
 * @param port_number
 * @param from_addr_fd
 * @param data
 * @param size
 * @return
 */
API_FUNC uint32_t ytUrRcv(uint16_t port_number, ytNetAddr_t from_addr_fd, uint8_t* data, uint32_t size){
	return net_uc_rcv(port_number, from_addr_fd, data, size);
}
/**
 *
 * @param port_number
 * @param from_addr_fd
 * @param data
 * @param size
 * @param callback_func
 * @param args
 * @return
 */
API_FUNC retval_t ytUrRcvAsync(uint16_t port_number, ytNetAddr_t from_addr_fd, uint8_t* data, uint32_t size, ytAsyncReadCbFunc_t callback_func, void* args){
	return net_uc_rcv_async(port_number, from_addr_fd, data, size, callback_func, args);
}

/**
 *
 * @param port_number
 * @return
 */
API_FUNC float32_t ytUrGetSignalLevel(uint16_t port_number){
	return net_uc_get_last_signal_level(port_number);
}
/* *********************************************************/


/* *********************************************************/
/* ****************TCP Like Functions***********************/
/* *********************************************************/
/**
 *
 * @param port_number
 * @return
 */
API_FUNC uint32_t ytRlCreateServer(uint16_t port_number){
	return net_rc_create_server(port_number);
}
/**
 *
 * @param server_fd
 * @return
 */
API_FUNC retval_t ytRlDeleteServer(uint32_t server_fd){
	return net_rc_delete_server(server_fd);
}
/**
 *
 * @param server_fd
 * @param from_addr_fd
 * @return
 */
API_FUNC uint32_t ytRlServerAcceptConnection(uint32_t server_fd, ytNetAddr_t from_addr_fd){
	return net_rc_server_accept_connection(server_fd, from_addr_fd);
}
/**
 *
 * @param dest_addr_fd
 * @param port_number
 * @return
 */
API_FUNC uint32_t ytRlConnectToServer(ytNetAddr_t dest_addr_fd, uint16_t port_number){
	return net_rc_connect_to_server(dest_addr_fd, port_number);
}
/**
 *
 * @param connection_fd
 * @return
 */
API_FUNC retval_t ytRlCloseConnection(uint32_t connection_fd){
	return net_rc_close_connection(connection_fd);
}
/**
 *
 * @param connection_fd
 * @param data
 * @param size
 * @return
 */
API_FUNC uint32_t ytRlSend(uint32_t connection_fd, uint8_t* data, uint32_t size){
	return net_rc_send(connection_fd, data, size);
}
/**
 *
 * @param connection_fd
 * @param data
 * @param size
 * @return
 */
API_FUNC uint32_t ytRlRcv(uint32_t connection_fd, uint8_t* data, uint32_t size){
	return net_rc_rcv(connection_fd, data, size);
}
/**
 *
 * @param connection_fd
 * @param data
 * @param size
 * @param callback_func
 * @param args
 * @return
 */
API_FUNC retval_t ytRlRcvAsync(uint32_t connection_fd, uint8_t* data, uint32_t size, ytAsyncReadCbFunc_t callback_func, void* args){
	return net_rc_rcv_async(connection_fd, data, size, callback_func, args);
}
/**
 *
 * @param connection_fd
 * @return
 */
API_FUNC uint16_t ytRlCheckConnection(uint32_t connection_fd){
	return net_rc_check_connection(connection_fd);
}

/**
 *
 * @param port_number
 * @return
 */
API_FUNC float32_t ytRlGetSignalLevel(uint32_t connection_fd){
	return net_rc_get_last_signal_level(connection_fd);
}

/**
 *
 * @param connection_fd
 * @param timeout
 * @return
 */
API_FUNC retval_t ytRlSetConnectionTimeout(uint32_t connection_fd, uint32_t timeout){
	return net_rc_set_connection_timeout(connection_fd, timeout);
}
/* *********************************************************/


/* *********************************************************/
/* ************Network Addresses Functions******************/
/* *********************************************************/
/**
 *
 * @param net_str_val
 * @param format
 * @return
 */
API_FUNC ytNetAddr_t ytNewNetAddr(char* net_str_val, char format){
	return new_net_addr(net_str_val, format);
}
/**
 *
 * @return
 */
API_FUNC ytNetAddr_t ytNewEmptyNetAddr(void){
	return new_empty_net_addr();
}
/**
 *
 * @param net_addr
 * @return
 */
API_FUNC retval_t ytDeleteNetAddr(ytNetAddr_t net_addr){
	return delete_net_addr(net_addr);
}
/**
 *
 * @param net_addr
 * @param net_addr_str_val
 * @param format
 * @return
 */
API_FUNC retval_t ytNetAddrToString(ytNetAddr_t net_addr, char* net_addr_str_val, char format){
	return net_addr_to_string(net_addr, net_addr_str_val, format);
}
/**
 *
 * @param dest_addr
 * @param from_addr
 * @return
 */
API_FUNC retval_t ytNetAddrCpy(ytNetAddr_t dest_addr, ytNetAddr_t from_addr){
	return net_addr_cpy(dest_addr, from_addr);
}
/**
 *
 * @param a_addr
 * @param b_addr
 * @return
 */
API_FUNC uint16_t ytNetAddrCmp(ytNetAddr_t a_addr, ytNetAddr_t b_addr){
	return net_addr_cmp(a_addr, b_addr);
}
/**
 *
 * @param index
 * @return
 */
API_FUNC ytNetAddr_t ytGetNodeAddr(uint16_t index){
	return get_node_addr(index);
}
/**
 *
 * @param node_addr
 * @return
 */
API_FUNC retval_t ytNetAddNodeAddr(ytNetAddr_t node_addr){
	return net_add_node_addr(node_addr);
}
/**
 *
 * @param node_addr
 * @return
 */
API_FUNC retval_t ytNetRemoveNodeAddr(ytNetAddr_t node_addr){
	return net_remove_node_addr(node_addr);
}
/**
 *
 * @param dest_addr
 * @param next_addr
 * @param hops
 * @return
 */
API_FUNC retval_t ytNetAddRoute(ytNetAddr_t dest_addr, net_addr_t next_addr, uint16_t hops){
	return net_add_route(dest_addr, next_addr, hops);
}
/**
 *
 * @param dest_addr
 * @param next_addr
 * @param hops
 * @return
 */
API_FUNC retval_t ytNetRemoveRoute(ytNetAddr_t dest_addr, net_addr_t next_addr, uint16_t hops){
	return net_remove_route(dest_addr, next_addr, hops);
}

/**
 *
 * @return
 */
API_FUNC retval_t ytSetNodeAsGw(void){
	return net_set_node_as_gw();
}


/**
 *
 * @return
 */
API_FUNC uint16_t ytMacIsNodeLinked(void){
	return mac_is_node_linked();
}

/**
 *
 * @param duty_cycle
 * @return
 */
API_FUNC retval_t ytMacSetDutyCycle(uint32_t duty_cycle){
	return mac_set_duty_cycle(duty_cycle);
}

/* *********************************************************/
#endif /* APPLICATION_CORE_SYSTEM_NET_API_FUNC_DEF_H_ */
