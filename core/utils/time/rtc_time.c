/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * rtc_time.c
 *
 *  Created on: 6 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file rtc_time.c
 */

#include "rtc_time.h"
#include "system_api.h"

/* Private variables */

static uint32_t rtc_time_mutex_id;
static uint16_t rtc_time_initialized = 0;

/**
 * @brief	Initializes the rtc time driver for timestamp operations.
 * @return	Return status
 */
retval_t rtc_time_init(){
#if ENABLE_RTC_TIMESTAMP
	if(rtc_time_initialized == 0){
		rtc_time_arch_init();
		rtc_time_mutex_id = ytMutexCreate();
		// TODO: ¿Donde pollas hago el init de la HAL?
		rtc_time_arch_set_timestamp(INIT_TIMESTAMP);
		rtc_time_initialized++;
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
#endif
	return RET_ERROR;
}

/**
 * @brief	De-Initializes the rtc driver
 * @return	Return status
 */
retval_t rtc_time_deinit(){
#if ENABLE_RTC_TIMESTAMP
	if(rtc_time_initialized != 0){
		rtc_time_arch_deinit();
		ytMutexDelete(rtc_time_mutex_id);
		rtc_time_initialized = 0;
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
#endif
	return RET_ERROR;
}

/**
 * @brief	Obtain the system timestamp from a RTC source
 * @return	The system timestamp in milliseconds. Return type: uint64_t which is usually equal to unsigned long long
 */
uint64_t rtc_time_get_timestamp(){
#if ENABLE_RTC_TIMESTAMP
	uint64_t timestamp = 0;
	if(rtc_time_initialized){
		ytMutexWait(rtc_time_mutex_id, YT_WAIT_FOREVER);
		timestamp = rtc_time_arch_get_timestamp();
		ytMutexRelease(rtc_time_mutex_id);
	}
	return timestamp;
#endif
	return 0;
}

/**
 * @brief 				Sets a new system timestamp
 * @param new_timestamp	New timestamp to be set. Type: uint64_t which is usually equal to unsigned long long
 * @return				Return status
 */
retval_t rtc_time_set_timestamp(uint64_t new_timestamp){
#if ENABLE_RTC_TIMESTAMP
	if(rtc_time_initialized){
		ytMutexWait(rtc_time_mutex_id, YT_WAIT_FOREVER);
		 rtc_time_arch_set_timestamp(new_timestamp);
		ytMutexRelease(rtc_time_mutex_id);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
#endif
	return RET_ERROR;

}

/**
 * @brief					Gets the current Date
 * @param rtc_time_date		Returned current date structure
 * @return					Return status
 */
retval_t rtc_time_get_date(rtc_time_date_t* rtc_time_date){
#if ENABLE_RTC_TIMESTAMP
	if(rtc_time_initialized){
		ytMutexWait(rtc_time_mutex_id, YT_WAIT_FOREVER);
		rtc_time_arch_get_date(rtc_time_date);
		ytMutexRelease(rtc_time_mutex_id);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
#endif
	return RET_ERROR;
}

/**
 * @brief				Sets the current date
 * @param rtc_time_date	Date structure to be set
 * @return				Return status
 */
retval_t rtc_time_set_date(rtc_time_date_t* rtc_time_date){
#if ENABLE_RTC_TIMESTAMP
	if(rtc_time_initialized){
		ytMutexWait(rtc_time_mutex_id, YT_WAIT_FOREVER);
		rtc_time_arch_set_date(rtc_time_date);
		ytMutexRelease(rtc_time_mutex_id);
		return RET_OK;
	}
	else{
		return RET_ERROR;
	}
#endif
	return RET_ERROR;
}
