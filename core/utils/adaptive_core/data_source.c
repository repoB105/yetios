/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * data_source.c
 *
 *  Created on: 19 de jul. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file data_source.c
 */

#include "data_source.h"
#include "adaptive_core.h"

#define MAX_DATA_SOURCES_NUM	8

static data_source_t* data_sources[MAX_DATA_SOURCES_NUM];
static uint16_t num_data_sources = 0;

/**
 *
 * @param data_source
 * @param sampling_period
 * @param sampling_func
 * @param data_source_id
 * @return
 */
retval_t init_data_source(data_source_t* data_source, uint32_t sampling_period, sample_source_func_t sampling_func, data_source_id_t data_source_id){
	if(data_source == NULL){
		return RET_ERROR;
	}
	if(sampling_func == NULL){
		return RET_ERROR;
	}

	if(sampling_period%ADAPTIVE_CORE_RESOLUTION){
		data_source->sampling_period = ((sampling_period/ADAPTIVE_CORE_RESOLUTION)+1) * ADAPTIVE_CORE_RESOLUTION;
	}
	else{
		data_source->sampling_period = sampling_period;
	}


	data_source->remaining_time = data_source->sampling_period;
	data_source->buffer_size = 0;
	data_source->buffer_counter = 0;
	data_source->data_buffer = NULL;
	data_source->data_source_id = data_source_id;
	data_source->sampling_func = sampling_func;

	return RET_OK;
}


/**
 *
 * @param data_source
 * @return
 */
retval_t deinit_data_source(data_source_t* data_source){
	if(data_source == NULL){
		return RET_ERROR;
	}

	data_source->sampling_period = 0;
	data_source->buffer_size = 0;
	data_source->buffer_counter = 0;
	data_source->sampling_func = NULL;
	unregister_data_source(data_source);

	return RET_OK;
}


/**
 *
 * @param data_source
 * @return
 */
retval_t register_data_source(data_source_t* data_source){

	if(data_source == NULL){
		return RET_ERROR;
	}
	if(num_data_sources >= MAX_DATA_SOURCES_NUM){
		return RET_ERROR;
	}

	data_sources[num_data_sources] = data_source;
	num_data_sources++;

	return RET_OK;

}

/**
 *
 * @param data_source
 * @return
 */
retval_t unregister_data_source(data_source_t* data_source){
	uint16_t i, j;
	if(data_source == NULL){
		return RET_ERROR;
	}
	for (i=0; i<num_data_sources; i++){
		if(data_sources[i] == data_source){
			for(j=i; j<num_data_sources-1; j++){
				data_sources[j] = data_sources[j+1];
			}
			num_data_sources--;
			return RET_OK;
		}
	}

	return RET_ERROR;
}

/**
 *
 * @param data_source_id
 * @return
 */
data_source_t* get_data_source(data_source_id_t data_source_id){
	uint16_t i;
	if(data_source_id == NO_SOURCE){
		return NULL;
	}
	for (i=0; i<num_data_sources; i++){
		if(data_sources[i]->data_source_id == data_source_id){

			return data_sources[i];
		}
	}

	return NULL;
}

/**
 *
 * @return
 */
retval_t sample_data_sources(void){
	uint16_t i;
	for (i=0; i<num_data_sources; i++){
		data_sources[i]->remaining_time -= ADAPTIVE_CORE_RESOLUTION;
		if(data_sources[i]->remaining_time == 0){
			if(data_sources[i]->sampling_func != NULL){
				data_sources[i]->sampling_func(data_sources[i]);
			}
			data_sources[i]->remaining_time = data_sources[i]->sampling_period;
		}
	}

	return RET_OK;

}


/**
 *
 * @return
 */
retval_t init_sources(void){
	extern uint32_t (*_src_initcall_start)(void); // Defined by the linker
	extern uint32_t (*_src_initcall_end)(void); 	// Defined by the linker

	uint32_t* start = (uint32_t*) &_src_initcall_start;
	uint32_t* end = (uint32_t*) &_src_initcall_end;


	while(((*start) != (uint32_t)NULL) && (start < end)){
		retval_t (*func)(void)=(retval_t(*)(void))(*start);
		if(func != NULL){
			func();
		}
		start++;
	}

	return RET_OK;
}


/**
 *
 * @return
 */
retval_t deinit_sources(void){
	extern uint32_t (*_src_deinitcall_start)(void); // Defined by the linker
	extern uint32_t (*_src_deinitcall_end)(void); // Defined by the linker


	uint32_t* start = (uint32_t*) &_src_deinitcall_start;
	uint32_t* end = (uint32_t*) &_src_deinitcall_end;

	while(((*start) != (uint32_t)NULL) && (start < end)){
		retval_t (*func)(void)=(retval_t(*)(void))(*start);
		if(func != NULL){
			func();
		}
		start++;
	}

	return RET_OK;
}
