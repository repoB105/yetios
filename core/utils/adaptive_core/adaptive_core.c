/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adaptive_core.c
 *
 *  Created on: 27 de feb. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file adaptive_core.c
 */

#include "platform-conf.h"
#include "adaptive_core.h"
#include "watch.h"
#include "system_api.h"
#include "data_source.h"
#include "adaptive_algorithm.h"

#define STATUS_LED_ON_TIME	50
#define STATUS_LED_PERIOD	2000



#define WATCH_MSG_QUEUE_SIZE	2


osMessageQDef(watchMsgBox, WATCH_MSG_QUEUE_SIZE, uint32_t);
osMessageQId  watchMsgBoxId;


static void mainDelayWatchMsg(uint32_t time);
/**
 *
 */
void adaptive_main_loop(void){

	uint32_t t1;
	watchMsgBoxId = osMessageCreate(osMessageQ(watchMsgBox), NULL);

	init_sources();
	init_adapt_algorithms();

	t1 = osKernelSysTick();

	while(1){

#if USE_HW_WATCHDOG
		watchdogRefresh();		//The watchdog resets the systems if no activity is detected in 5 seconds
#endif

		mainDelayWatchMsg(ADAPTIVE_CORE_RESOLUTION);

		//Sample data sources when their periods have elapsed
		sample_data_sources();
		//Adaptive algorithms execution
		execute_adaptive_algorithms();

		if((osKernelSysTick()-t1) >= (STATUS_LED_PERIOD - STATUS_LED_ON_TIME)){
#ifdef STATUS_LED
			ytLedsOn(STATUS_LED);
#endif
			t1 = osKernelSysTick();
		}

		if((osKernelSysTick()-t1) >= (STATUS_LED_ON_TIME)){
#ifdef STATUS_LED
			ytLedsOff(STATUS_LED);
#endif
		}

	}

}


/**
 * @brief		This function listens to messages during delay periods for the main process. For example it calls the osExitProcess function when a message to do it is received
 * @param time	Time to delay
 */
static void mainDelayWatchMsg(uint32_t time){
	uint32_t t1, t2;
	mainWatchMsg_t* mainWatchMsg;
	osEvent evt;

	t1 = osKernelSysTick();
	t2 = osKernelSysTick();

	while(t2-t1 < (time)){
		evt = osMessageGet(watchMsgBoxId, (time) - (t2-t1));  // wait for message
		if (evt.status == osEventMessage) {
			mainWatchMsg = evt.value.p;
			switch(mainWatchMsg->MsgType){
			case EXIT_TASK_MSG:
				osExitProcess((uint32_t)(mainWatchMsg->data.uintData));
				break;
			case ERROR_MSG:
#ifdef ERROR_LED
				ytLedsOn(ERROR_LED);
#endif
				osThreadSuspendAll();
				while(1);
				break;
			case NO_MSG:
				break;
			default:
				break;
			}

			deleteMainWatchMsg(mainWatchMsg);
		}
		t2 = osKernelSysTick();
	}
}
