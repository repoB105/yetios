/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * process.c
 *
 *  Created on: 13 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file process.c
 */

#include "process.h"
#include "platform-conf.h"
#include "watch.h"
#include "yetimote_stdio.h"
#include "leds.h"

#define PROCESS_MAX_PRIORITY	((uint16_t)osPriorityAboveNormal)
#define PROCESS_MIN_PRIORITY	((uint16_t)osPriorityBelowNormal)

extern osMessageQId  watchMsgBoxId;

static gen_list* process_list;
static uint16_t num_processes = 0;

static void processCallbackFunc(void const * argument);
static retval_t freeProcessDynamicMem(process_control_t* process);
static retval_t terminateProcess(process_control_t* process);
static uint16_t generateNewUniqueId(void);
static process_control_t* getProcessHandle(uint16_t processId);


/**
 *
 * @return
 */
retval_t initProcessList(void){
	process_list = gen_list_init();

	if(process_list == NULL){
		return RET_ERROR;
	}
	return RET_OK;
}

/**
 * @brief	Init all User Processes on startup. THIS function MUST not be called from user processes
 * @return	Return status
 */
retval_t initUserProcesses(void){

	extern uint32_t (*_process_init_start)(void); // Defined by the linker
	extern uint32_t (*_process_init_end)(void); // Defined by the linker

	uint32_t* start = (uint32_t*) &_process_init_start;
	uint32_t* end = (uint32_t*) &_process_init_end;

	while(((*start) != (uint32_t)NULL) && (start < end)){
		retval_t (*func)(void)=(retval_t(*)(void))(*start);
		if(func() != RET_OK){
#ifdef ERROR_LED
			leds_on(ERROR_LED);	// If there is any problem initializing a process, block the system
#endif
			osThreadSuspendAll();
			while(1);
		}
		start++;
	}

	return RET_OK;
}

/**
 * @brief	Exit all user processes running. THIS function MUST not be called from user processes
 * @return	Return status
 */
retval_t exitUserProcesses(void){
	uint16_t processId = 1;
	while(num_processes > 0){
		osExitProcess(processId);
		processId++;
	}
	return RET_OK;
}


/**
 * @brief					Create a new process and starts it. PUBLIC FUNCTION
 * @param name				Process name. For memory saving avoid large names (<16 bytes).
 * @param func				Function to be launched when the process starts
 * @param process_class		Class of the process to be created
 * @param stacksize			Amount of stack to reserve for this process
 * @param processUniqueId	Returned value of the process Id created. Use a pointer to NULL if is not desired to return the id
 * @param arg				Argument to the process function
 * @return					Return status
 */
retval_t startProcess(char* name, process_func_t func, process_class_t process_class, uint32_t stacksize,
		uint16_t* processUniqueId, void* arg){
	osPriority priority;

	if(num_processes >= MAX_NUM_PROCESSES){
		return RET_ERROR;
	}
	if(strlen(name) <= 0){
		return RET_ERROR;
	}
	switch(process_class){					//FreeRTOS low priority reserved for core process
	case NORMAL_PRIORITY_PROCESS:
		priority = osPriorityAboveNormal;
		break;
	case LOW_PRIORITY_PROCESS:
		priority = osPriorityNormal;
		break;
	case VERY_LOW_PRIORITY_PROCESS:
		priority = osPriorityBelowNormal;
		break;
	case HIGH_PRIORITY_PROCESS:
		priority = osPriorityHigh;
		break;
	case VERY_HIGH_PRIORITY_PROCESS:
		priority = osPriorityRealtime;
		break;
	default:
		return RET_ERROR;
		break;
	}
	if((func == NULL) || (stacksize<128) || (strlen(name)>30)){
		return RET_ERROR;
	}
	process_control_t* process_control = (process_control_t*) pvPortMalloc(sizeof(process_control_t));
	if(process_control == NULL){
		return RET_ERROR;
	}
	if((process_control->childProcessesList = gen_list_init())== NULL){
		return RET_ERROR;
	}

	process_control->func_args.args = arg;
	process_control->func_args.function =func;
	process_control->parentProcessId = getProcessId();

	const osThreadDef_t os_thread_def = 	{ name, processCallbackFunc, priority, 0, stacksize};
	osThreadSuspendAll();		//Region crítica necesaria, para que la tarea no comience antes de introducir en la lista. Además de este modo nadie más meterá cosas en la lista simultaneamente

	if ((process_control->freeRrtosThreadId = osThreadCreate(&os_thread_def, (void*) &(process_control->func_args))) == NULL){
		vPortFree(process_control);
		gen_list_remove_and_delete_all(process_control->childProcessesList);
		osThreadResumeAll();
		return RET_ERROR;
	}
	process_control->processId = generateNewUniqueId();
	process_control->parentProcessRef = getProcessHandle(process_control->parentProcessId);

	if(processUniqueId != NULL){
		(*processUniqueId) = process_control->processId;
	}

	process_control->processName = (char*) pvPortMalloc(strlen(name) + 1);
	memcpy(process_control->processName, name, strlen(name) + 1);

	if(process_control->parentProcessId != 0){
		if((gen_list_add(process_control->parentProcessRef->childProcessesList, process_control)) != RET_OK){	//Me incluyo en la lista de hijos de mi padre (si tengo padre distinto de 0)
			vPortFree(process_control->processName);
			vPortFree(process_control);
			gen_list_remove_and_delete_all(process_control->childProcessesList);
			osThreadResumeAll();
			return RET_ERROR;
		}
	}
#if USE_ADAPT_PRIORITIES_ALG
	process_control->num_tokens = 0;
	process_control->task_type = 0;
#endif
	if((gen_list_add(process_list, (void*) process_control)) != RET_OK){		//Me añado a la lista global de procesos
		vPortFree(process_control->processName);
		vPortFree(process_control);
		gen_list_remove_and_delete_all(process_control->childProcessesList);
		osThreadResumeAll();
		return RET_ERROR;
	}
	num_processes++;
	osThreadResumeAll();
	return RET_OK;
}

/**
 * @brief			Exit the selected process. PUBLIC FUNCTION
 * @param processId	Process to be exited
 * @return			Return status
 */
retval_t exitProcess(uint16_t processId){
	mainWatchMsg_t* mainWatchMsg;
	if(processId == 0){
		return RET_ERROR;
	}
	process_control_t* process_to_exit;

	if((process_to_exit= getProcessHandle(processId)) == NULL){	//Compruebo que existe el proceso que quiero borrar
		return RET_ERROR;
	}
	//La funcion exit debe ejecutarse en la hebra principal main para asi poder borrarme a mi mismo sin problemas
	mainWatchMsg =  createMainWatchMsg(EXIT_TASK_MSG, (void*) ((uint32_t)processId));
	osMessagePut(watchMsgBoxId, (uint32_t)mainWatchMsg, osWaitForever);
	return RET_OK;

}


/**
 * @brief				Suspends the selected running process
 * @param process_id	Process id to be suspended
 * @return				Return status
 */
retval_t processSuspend(uint16_t process_id){
	process_control_t* process;
	if((process = getProcessHandle(process_id)) != NULL){
		osThreadSuspend(process->freeRrtosThreadId);
		return RET_OK;
	}
	return RET_ERROR;
}

/**
 * @brief				Resumes the selected suspended process
 * @param process_id	Process id to be resumed
 * @return				Return status
 */
retval_t processResume(uint16_t process_id){
	process_control_t* process;
	if((process = getProcessHandle(process_id)) != NULL){
		osThreadResume(process->freeRrtosThreadId);
		return RET_OK;
	}
	return RET_ERROR;
}

/**
 * @brief					Sets priority of a process
 * @param process_id		Process id to change its priority
 * @param process_class		New Priority
 * @return					Return status
 */
retval_t setProcessPriority(uint16_t process_id, process_class_t process_class){
	process_control_t* process;
	osPriority priority;
	if((process = getProcessHandle(process_id)) != NULL){
		switch(process_class){					//FreeRTOS low priority reserved for core process
		case NORMAL_PRIORITY_PROCESS:
			priority = osPriorityAboveNormal;
			break;
		case LOW_PRIORITY_PROCESS:
			priority = osPriorityNormal;
			break;
		case VERY_LOW_PRIORITY_PROCESS:
			priority = osPriorityBelowNormal;
			break;
		case HIGH_PRIORITY_PROCESS:
			priority = osPriorityHigh;
			break;
		case VERY_HIGH_PRIORITY_PROCESS:
			priority = osPriorityRealtime;
			break;
		default:
			return RET_ERROR;
			break;
		}
		osThreadSetPriority (process->freeRrtosThreadId, priority);
		return RET_OK;
	}
	return RET_ERROR;
}

/**
 * @brief				Returns the current priority of a process
 * @param process_id	Process id instance
 * @return				Priority of the process
 */
process_class_t getProcessPriority(uint16_t process_id){
	process_control_t* process;
	osPriority priority;
	if((process = getProcessHandle(process_id)) != NULL){
		priority = osThreadGetPriority (process->freeRrtosThreadId);
		switch(priority){					//FreeRTOS low priority reserved for core process
		case osPriorityAboveNormal:
			return NORMAL_PRIORITY_PROCESS;
		case osPriorityNormal:
			return LOW_PRIORITY_PROCESS;
		case osPriorityBelowNormal:
			return VERY_LOW_PRIORITY_PROCESS;
		case osPriorityHigh:
			return HIGH_PRIORITY_PROCESS;
		case osPriorityRealtime:
			return VERY_HIGH_PRIORITY_PROCESS;
		default:
			return VERY_LOW_PRIORITY_PROCESS;
		}
	}
	return VERY_LOW_PRIORITY_PROCESS;
}


/**
 * @brief	Returns the current running process id
 * @return	Process id value
 */
uint16_t getProcessId(void){
	gen_list* current = process_list;
	process_control_t* process_control;
	osThreadId current_thread = osThreadGetId();

	while(current->next != NULL){
		process_control = (process_control_t*) current->next->item;
		if(process_control->freeRrtosThreadId == current_thread){
			return process_control->processId;
		}
		current = current->next;
	}
	return 0;
}

/**
 * @brief	Returns the parent process id of the current running process
 * @return	Parent process id
 */
uint16_t getCurrentParentProcessId(void){
	gen_list* current = process_list;
	process_control_t* process_control;
	osThreadId current_thread = osThreadGetId();

	while(current->next != NULL){
		process_control = (process_control_t*) current->next->item;
		if(process_control->freeRrtosThreadId == current_thread){
			return process_control->parentProcessId;
		}
		current = current->next;
	}
	return 0;
}

/**
 * @brief			Returns the parent process id of the selected process
 * @param processId	Process id
 * @return			Parent process id
 */
uint16_t getParentProcessId(uint16_t processId){
	gen_list* current = process_list;
	process_control_t* process_control;

	while(current->next != NULL){
		process_control = (process_control_t*) current->next->item;
		if(process_control->processId == processId){
			return process_control->parentProcessId;
		}
		current = current->next;
	}
	return 0;
}

/**
 * @brief	Prints in stdio readable information about current running processses
 * @return	Return Status
 */
retval_t printProcesses(void){
	gen_list* current = process_list;
	process_control_t* process_control;
	_printf("NAME\t\tID\tPARENT\tFREE STACK\r\n");

	while(current->next != NULL){
		process_control = (process_control_t*) current->next->item;

		uint32_t* TCB = (uint32_t*) process_control->freeRrtosThreadId;
		uint32_t topOfStack = (uint32_t) ((*TCB));
		uint32_t  startOfStack = (uint32_t) ((*(TCB+12)));
		_printf("%s\t%d\t%d\t%d Bytes\r\n", process_control->processName, (int)process_control->processId, (int)process_control->parentProcessId, (int)(topOfStack-startOfStack));
		current = current->next;
	}

	return RET_OK;
}

/**
 *
 * @param timer_type
 * @param timer_func
 * @param argument
 * @return
 */
uint32_t y_timerCreate(os_timer_type timer_type, process_func_t timer_func, void *argument){
	osTimerId new_timer;

	if(!timer_func){
		return 0;
	}
	osTimerDef(yt_timer, timer_func);
	new_timer = osTimerCreate(osTimer(yt_timer), timer_type, argument);

	return (uint32_t) new_timer;
}

/**
 *
 * @param timer_id
 * @param millisec
 * @return
 */
retval_t y_timerStart(uint32_t timer_id, uint32_t millisec){
	osStatus status;
	if(timer_id == 0){
		return RET_ERROR;
	}
	if((status = osTimerStart ((osTimerId) timer_id,  millisec)) != osOK){
		return RET_ERROR;
	}
	return RET_OK;
}

/**
 *
 * @param timer_id
 * @return
 */
retval_t y_timerStop(uint32_t timer_id){
	osStatus status;
	if(timer_id == 0){
		return RET_ERROR;
	}
	if((status = osTimerStop ((osTimerId) timer_id)) != osOK){
		return RET_ERROR;
	}
	return RET_OK;
}

/**
 *
 * @param timer_id
 * @return
 */
retval_t y_timerDelete(uint32_t timer_id){
	osStatus status;
	if(timer_id == 0){
		return RET_ERROR;
	}
	if((status = osTimerDelete((osTimerId) timer_id)) != osOK){
		return RET_ERROR;
	}
	return RET_OK;
}

/**
 *
 * @return
 */
uint32_t y_mutexCreate(void){
	osMutexId new_mutex;
	osMutexDef(yt_mutex);
	new_mutex = osMutexCreate(osMutex(yt_mutex));

	return (uint32_t) new_mutex;
}
/**
 *
 * @param mutex_id
 * @param millisec
 * @return
 */
retval_t y_mutexWait(uint32_t mutex_id, uint32_t millisec){
	osStatus status;
	if(mutex_id == 0){
		return RET_ERROR;
	}
	if((status = osMutexWait ((osMutexId) mutex_id,  millisec)) != osOK){
		return RET_ERROR;
	}
	return RET_OK;
}
/**
 *
 * @param mutex_id
 * @return
 */
retval_t y_mutexRelease(uint32_t mutex_id){
	osStatus status;
	if(mutex_id == 0){
		return RET_ERROR;
	}
	if((status = osMutexRelease ((osMutexId) mutex_id)) != osOK){
		return RET_ERROR;
	}
	return RET_OK;
}
/**
 *
 * @param mutex_id
 * @return
 */
retval_t y_mutexDelete(uint32_t mutex_id){
	osStatus status;
	if(mutex_id == 0){
		return RET_ERROR;
	}
	if((status = osMutexDelete((osMutexId) mutex_id)) != osOK){
		return RET_ERROR;
	}
	return RET_OK;
}
/**
 *
 * @param count
 * @return
 */
uint32_t y_semaphoreCreate(int32_t count){
	osSemaphoreId new_semph;
	osSemaphoreDef(yt_semaphore);
	new_semph = osSemaphoreCreate(osSemaphore(yt_semaphore), count);
	if(new_semph == NULL){
		return 0;
	}

	return (uint32_t) new_semph;
}
/**
 *
 * @param semaphore_id
 * @param millisec
 * @return
 */
retval_t y_semaphoreWait(uint32_t semaphore_id, uint32_t millisec){
	osStatus status;
	if(semaphore_id == 0){
		return RET_ERROR;
	}
	if((status = osSemaphoreWait ((osSemaphoreId) semaphore_id,  millisec)) != osOK){
		return RET_ERROR;
	}
	return RET_OK;
}
/**
 *
 * @param semaphore_id
 * @return
 */
retval_t y_semaphoreRelease(uint32_t semaphore_id){
	osStatus status;
	if(semaphore_id == 0){
		return RET_ERROR;
	}
	if((status = osSemaphoreRelease ((osSemaphoreId) semaphore_id)) != osOK){
		return RET_ERROR;
	}
	return RET_OK;
}
/**
 *
 * @param semaphore_id
 * @return
 */
retval_t y_semaphoreDelete(uint32_t semaphore_id){
	osStatus status;
	if(semaphore_id == 0){
		return RET_ERROR;
	}
	if((status = osSemaphoreDelete((osSemaphoreId) semaphore_id)) != osOK){
		return RET_ERROR;
	}
	return RET_OK;
}

/**
 *
 * @param q_size
 * @return
 */
uint32_t y_messageqCreate(uint32_t q_size){

	osMessageQDef(yt_messageq, q_size, uint32_t);
	osMessageQId new_msg = osMessageCreate(osMessageQ(yt_messageq), NULL);
	if(new_msg == NULL){
		return 0;
	}

	return (uint32_t) new_msg;
}
/**
 *
 * @param messageq_id
 * @param msg
 * @param millisec
 * @return
 */
retval_t y_messageqPut(uint32_t messageq_id, void* msg, uint32_t millisec){
	osStatus status;
	if(messageq_id == 0){
		return RET_ERROR;
	}
	if((status = osMessagePut ((osMessageQId) messageq_id, (uint32_t) msg,  millisec)) != osOK){
		return RET_ERROR;
	}
	return RET_OK;
}
/**
 *
 * @param messageq_id
 * @param millisec
 */
void* y_messageqGet(uint32_t messageq_id, uint32_t millisec){
	osEvent event;
	if(messageq_id == 0){
		return NULL;
	}
	event = osMessageGet ((osMessageQId) messageq_id,  millisec);
    if (event.status == osEventMessage) {
      return event.value.p;
    }
	return NULL;
}
/**
 *
 * @param messageq_id
 * @return
 */
retval_t y_messageqDelete(uint32_t messageq_id){
	osStatus status;
	if(messageq_id == 0){
		return RET_ERROR;
	}
	if((status = osMessageDelete((osMessageQId) messageq_id)) != osOK){
		return RET_ERROR;
	}
	return RET_OK;
}



/**
 * @brief			Free the dynamic memory that has been reserved in this process with yMalloc and has not been previously freed. Private function
 * @param process	Process to free the dynamic memory
 * @return			Return status
 */
static retval_t freeProcessDynamicMem(process_control_t* process){

#if USE_YETI_OS_HEAP_MAN
	vRemoveThreadResMem((uint32_t) process->freeRrtosThreadId);
#endif
	return RET_OK;
}


/**
 * @brief			Exit an user process. THIS function MUST not be called from user processes
 * @param processId	Process to be exited
 * @return			Return status
 */
retval_t osExitProcess(uint16_t processId){

	if(processId == 0){
		return RET_ERROR;
	}

	process_control_t* process_to_exit;

	if((process_to_exit= getProcessHandle(processId)) == NULL){
		return RET_ERROR;
	}
#if CHILD_PROCESSES_LINKED_WITH_PARENTS
	gen_list* current_proc_list = process_list;
	uint16_t shouldDeleteChild;
	process_control_t* process_aux;
	process_control_t* last_child;

	while(current_proc_list->next != NULL){
		last_child = (process_control_t*) current_proc_list->next->item;	//Recorro toda la lista y busco los hijos mas en la base del arbol

		//Estos son procesos que están en la base
		if(last_child->childProcessesList->next == NULL){
			shouldDeleteChild = 1;
			process_aux = last_child;
			while(process_aux->parentProcessRef != process_to_exit){
				if(process_aux->parentProcessRef == NULL){	//He llegado arriba del todoc sin encontrarme con el proceso que he mandado borrar
					shouldDeleteChild = 0;					//Por tanto no hay que borrar este proceso de la ya que no está emparentado con el que hemos mandado borrar
					break;
				}
				process_aux = process_aux->parentProcessRef;
			}
			if(shouldDeleteChild){
				//Si me tengo que eliminar a mi mismo
				if((terminateProcess(last_child)) != RET_OK){
					return RET_ERROR;
				}
				//Reempiezo el bucle al eliminar un nodo.
				current_proc_list = process_list;
				continue;
			}
		}


		current_proc_list = current_proc_list->next;
	}
#endif


	if((terminateProcess(process_to_exit)) != RET_OK){		//Finalmente me salgo del proceso que debo salirme
		return RET_ERROR;
	}


	return RET_OK;
}

/**
 *
 * @return
 */
gen_list* get_process_list(void){
	return process_list;
}

osStatus osThreadSetPriority (osThreadId thread_id, osPriority priority);

osPriority osThreadGetPriority (osThreadId thread_id);

/**
 *
 * @param process_control
 * @return
 */
int16_t get_process_priority(process_control_t* process_control){
	return (int16_t) osThreadGetPriority (process_control->freeRrtosThreadId);
}

/**
 *
 * @param process_control
 * @param priority
 * @return
 */
retval_t set_process_priority(process_control_t* process_control, int16_t priority){
	if(process_control == NULL){
		return RET_ERROR;
	}
	if(priority > PROCESS_MAX_PRIORITY){
		osThreadSetPriority (process_control->freeRrtosThreadId, (osPriority) PROCESS_MAX_PRIORITY);
	}
	else if(priority < PROCESS_MIN_PRIORITY){
		osThreadSetPriority (process_control->freeRrtosThreadId, (osPriority) PROCESS_MIN_PRIORITY);
	}
	else{
		osThreadSetPriority (process_control->freeRrtosThreadId, (osPriority) priority);
	}

	return RET_OK;

}

/**
 *
 * @param process_control
 * @return
 */
retval_t increase_process_priority(process_control_t* process_control){
	if(process_control == NULL){
		return RET_ERROR;
	}
	int16_t new_priority = (int16_t) osThreadGetPriority (process_control->freeRrtosThreadId);
	new_priority++;
	if(new_priority > PROCESS_MAX_PRIORITY){
		osThreadSetPriority (process_control->freeRrtosThreadId, (osPriority) PROCESS_MAX_PRIORITY);
	}
	else{
		osThreadSetPriority (process_control->freeRrtosThreadId, (osPriority) new_priority);
	}

	return RET_OK;

}

/**
 *
 * @param process_control
 * @return
 */
retval_t decrease_process_priority(process_control_t* process_control){
	if(process_control == NULL){
		return RET_ERROR;
	}
	int16_t new_priority = (int16_t) osThreadGetPriority (process_control->freeRrtosThreadId);
	new_priority--;
	if(new_priority < PROCESS_MIN_PRIORITY){
		osThreadSetPriority (process_control->freeRrtosThreadId, (osPriority) PROCESS_MIN_PRIORITY);
	}
	else{
		osThreadSetPriority (process_control->freeRrtosThreadId, (osPriority) new_priority);
	}

	return RET_OK;
}


/**
 * @brief			Terminate a process and liberate its memory struct. Private function
 * @param process	Process to be terminated
 * @return			Return status
 */
static retval_t terminateProcess(process_control_t* process){

	if(process == NULL){
		return RET_ERROR;
	}
	gen_list* current = process->childProcessesList;
	process_control_t* childProcess;
	osThreadId threadToTerminate = process->freeRrtosThreadId;

	if(current == NULL){
		return RET_ERROR;
	}
	osThreadSuspendAll();
	freeProcessDynamicMem(process);//Libero la memoria dinámica reservada con yMallocs en el proceso que no haya sido liberada
	if(process->parentProcessRef != NULL){
		gen_list_remove(process->parentProcessRef->childProcessesList, (void*) process);	//Me borro de la lista de hijos de mi padre
	}
	//Pongo a mis hijos apuntando a mi padre
	while(current->next != NULL){
		childProcess = (process_control_t*) current->next->item;
		childProcess->parentProcessRef = process->parentProcessRef;
		childProcess->parentProcessId = process->parentProcessId;
		//Añado a la lista de hijos del nuevo padre (si este no es null)
		if(childProcess->parentProcessId != 0){
			gen_list_add(process->parentProcessRef->childProcessesList, (void*) childProcess);
		}
		current = current->next;
	}

	//Elimino mi lista de hijos
	vPortFree(process->childProcessesList);

	//Me elimino de la lista de procesos global
	gen_list_remove(process_list, process);
	num_processes--;

	//Libero el process_control
	vPortFree(process->processName);
	vPortFree(process);

	osThreadResumeAll();

	//Mato la hebra
	osThreadTerminate(threadToTerminate);

	return RET_OK;

}

/**
 * @brief			Process callback for all user processes. Private function
 * @param argument	Argument to the callback function which contains the real function callback to be called
 */
static void processCallbackFunc(void const * argument){
	process_func_args_t* func_args = (process_func_args_t*) argument;
//	osDelay(10);
//	init_process_procedure();	//This function may be used or not
	func_args->function(func_args->args);
	if(exitProcess(getProcessId()) != RET_OK){ 			//This function should close the process adequately
		osThreadSuspendAll();
		while(1);
	}
	while(1){
		osDelay(1000);		//Sleep the process till it is externally closed
	}
}

/**
 * @brief	Generate a new unique id. Private function
 * @return	Return status
 */
static uint16_t generateNewUniqueId(void){
	process_control_t* process_control;
	uint16_t current_val = 1;
	uint16_t repeat_search = 1;
	while(repeat_search){
	gen_list* current = process_list;
	if(current == NULL){
		return 0;
	}
	repeat_search = 0;
		while(current->next != NULL){
			process_control = (process_control_t*) current->next->item;
			if(process_control->processId == current_val){
				current_val++;
				repeat_search = 1;
				break;
			}
			current = current->next;
		}
	}
	return current_val;
}

/**
 * @brief			Returns the process handle struct from a processId
 * @param processId	Process Id
 * @return			Returns status
 */
static process_control_t* getProcessHandle(uint16_t processId){

	gen_list* current = process_list;
	process_control_t* process_control;

	if(current == NULL){
		return NULL;
	}

	while(current->next != NULL){
		process_control = (process_control_t*) current->next->item;
		if(process_control->processId == processId){
			return process_control;
		}
		current = current->next;
	}
	return NULL;
}
