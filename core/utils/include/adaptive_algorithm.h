/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adaptive_algorithm.h
 *
 *  Created on: 19 de jul. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file adaptive_algorithm.h
 */
#ifndef YETIOS_CORE_UTILS_INCLUDE_ADAPTIVE_ALGORITHM_H_
#define YETIOS_CORE_UTILS_INCLUDE_ADAPTIVE_ALGORITHM_H_

#include "data_source.h"

struct adaptive_algorithm_;

typedef retval_t (*adaptive_algorithm_func_t)(struct adaptive_algorithm_* adaptive_algorithm);

typedef struct adaptive_algorithm_{
	data_source_t** linked_data_sources;		//! Contains the instances to all data_sources used by this algorithm
	adaptive_algorithm_func_t algorithm_func;	//! Algorithm function to be called
	uint32_t algorithm_period;					//! Executing period of the algorithm function. The minimun period is defined by ADAPTIVE_CORE_RESOLUTION
	uint32_t remaining_time;					//! Current remaining time to finish the executing period
	uint16_t num_data_sources;					//! Number of data sources available
	uint16_t current_data_sources;				//! Number of data sources currently linked
}adaptive_algorithm_t;

retval_t init_adaptive_algorithm(adaptive_algorithm_t* adaptive_algorithm, uint16_t num_data_sources, adaptive_algorithm_func_t algorithm_func, uint32_t period);
retval_t deinit_adaptive_algorithm(adaptive_algorithm_t* adaptive_algorithm);

retval_t link_source_to_algorithm(adaptive_algorithm_t* adaptive_algorithm, data_source_id_t data_source_id);
retval_t unlink_source_to_algorithm(adaptive_algorithm_t* adaptive_algorithm, data_source_id_t data_source_id);

retval_t register_adaptive_algorithm(adaptive_algorithm_t* adaptive_algorithm);
retval_t unregister_adaptive_algorithm(adaptive_algorithm_t* adaptive_algorithm);

retval_t execute_adaptive_algorithms(void);


/*Adaptive Algorithms auto initialization macros*/
#define init_algorithm(fn) \
    static retval_t (*__alg_initcall_##fn)(void) __attribute__((__used__)) \
    __attribute__((__section__(".alg_initcall"))) = fn

#define deinit_algorithm(fn) \
    static retval_t (*__alg_deinitcall_##fn)(void) __attribute__((__used__)) \
    __attribute__((__section__(".alg_deinitcall"))) = fn


retval_t init_adapt_algorithms(void);
retval_t deinit_adapt_algorithms(void);

#endif /* YETIOS_CORE_UTILS_INCLUDE_ADAPTIVE_ALGORITHM_H_ */
