/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * reactor.h
 *
 *  Created on: 16 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file reactor.h
 */
#ifndef APPLICATION_CORE_UTILS_INCLUDE_REACTOR_H_
#define APPLICATION_CORE_UTILS_INCLUDE_REACTOR_H_

#include "types.h"
#include "generic_list.h"

typedef retval_t (*handle_event_func)(void* args);

typedef struct event_handler_{
	handle_event_func handle_func;
	void* args;
}event_handler_t;


typedef struct reactor_{
	uint32_t reactor_mutex_fd;
	uint32_t reactor_semaphore_fd;
	uint32_t pending_events;
	uint32_t handle_event_timeout;
	gen_list* handle_list;
}reactor_t;


reactor_t*  new_reactor(uint32_t handle_event_timeout);
retval_t delete_reactor(reactor_t* reactor);

retval_t reactor_handle_events(reactor_t* reactor);

retval_t post_event(reactor_t* reactor, handle_event_func handle_func, void* arg);



#endif /* APPLICATION_CORE_UTILS_INCLUDE_REACTOR_H_ */
