/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adxl355_driver.c
 *
 *  Created on: 8 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file adxl355_driver.c
 */


#include <adxl355_lib.h>
#include "device.h"
#include "platform-conf.h"
#include "system_api.h"
#include "arm_math.h"

#define DEBUG 1
#if DEBUG
#include "yetimote_stdio.h"
#define PRINTF(...) _printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#if ENABLE_ADXL355_DRIVER
#include "spi_driver.h"
#include "adxl355_plaform.h"

#define ADXL355_FIFO_THREESHOLD	20

#define READ_TIMER_PERIOD		140
#define NUM_SAMPLES_TO_READ		18

#define ADXL_INPUT_BUFFER_SIZE		64
/* Device name and Id */
#define ADXL355_DEVICE_NAME ADXL355_DEV


typedef struct adxl355_values_{
	float32_t x_val;
	float32_t y_val;
	float32_t z_val;
}adxl355_values_t;

/*Local variables*/
static uint16_t dev_opened = 0;
static adxl355_data_t* adxl355_data = NULL;

static adxl355_values_t input_buffer[ADXL_INPUT_BUFFER_SIZE*3];		//x, y, z components
static adxl355_values_t* buffer_ptr;
static uint16_t buffer_count = 0;

static uint32_t read_timer_id;
static void read_timer_func(void const * argument);

/* Device Init functions */
static retval_t adxl355_init(void);
static retval_t adxl355_exit(void);

/* Device driver operation functions declaration */
static retval_t adxl355_open(device_t* device, dev_file_t* filep);
static retval_t adxl355_close(dev_file_t* filep);
static size_t adxl355_read(dev_file_t* filep, uint8_t* prx, size_t size);
static size_t adxl355_write(dev_file_t* filep, uint8_t* ptx, size_t size);
static retval_t adxl355_ioctl(dev_file_t* filep, uint16_t request, void* args);


/* Define driver operations */
static driver_ops_t adxl355_driver_ops = {
	.open = adxl355_open,
	.close = adxl355_close,
	.read = adxl355_read,
	.write = adxl355_write,
	.ioctl = adxl355_ioctl,
};


/**
 *
 * @return
 */
static retval_t adxl355_init(void){

	if((adxl355_data = new_adxl355_data()) == NULL){
		return RET_ERROR;
	}
	if(ADXL355_init(adxl355_data, ADXL355_ISHTAR_CS_PIN, ADXL355_SPI_DEV) != RET_OK){
		delete_adxl355_data(adxl355_data);
		adxl355_data = NULL;
		return RET_ERROR;
	}


	if(registerDevice(ADXL355_DEVICE_ID, &adxl355_driver_ops, ADXL355_DEVICE_NAME) != RET_OK){
		ADXL355_deinit(adxl355_data);
		delete_adxl355_data(adxl355_data);
		adxl355_data = NULL;
		return RET_ERROR;
	}

	buffer_ptr = &input_buffer[0];
	read_timer_id = ytTimerCreate(ytTimerPeriodic, read_timer_func, NULL);
	ytTimerStart(read_timer_id, READ_TIMER_PERIOD);
	dev_opened = 0;

	PRINTF(">ADXL355 Init Done\r\n");

	return RET_OK;
}

/**
 *
 * @return
 */
static retval_t adxl355_exit(void){

	if(ADXL355_deinit(adxl355_data) != RET_OK){				//De-Inicializaci�n del HW del spi 2
		return RET_ERROR;
	}

	ytTimerStop(read_timer_id);
	delete_adxl355_data(adxl355_data);

	unregisterDevice(ADXL355_DEVICE_ID, ADXL355_DEVICE_NAME);

	dev_opened = 0;
	adxl355_data = NULL;

	PRINTF(">ADXL355 Exit Done\r\n");
	return RET_OK;
}

/**
 *
 * @param device
 * @param filep
 * @return
 */
static retval_t adxl355_open(device_t* device, dev_file_t* filep){
	if(device->device_state != DEV_STATE_INIT){
		return RET_ERROR;
	}
	if(adxl355_data == NULL){
		return RET_ERROR;
	}
	if(dev_opened){			//This driver can only be opened once
		return RET_ERROR;
	}
	dev_opened++;


	return RET_OK;
}

/**
 *
 * @param filep
 * @return
 */
static retval_t adxl355_close(dev_file_t* filep){
	if((adxl355_data == NULL) || (filep == NULL)){
		return RET_ERROR;
	}
	if(!dev_opened){
		return RET_ERROR;
	}
	dev_opened--;

	return RET_OK;
}

/**
 *
 * @param filep
 * @param ptx
 * @param size
 * @return
 */
static size_t adxl355_write(dev_file_t* filep, uint8_t* ptx, size_t size){
	return 0;
}


/**
 *
 * @param filep
 * @param prx
 * @param size
 * @return
 */
static size_t adxl355_read(dev_file_t* filep, uint8_t* prx, size_t size){	//La funcion lee un numero size de float32_t.
																			//Segun el numero de ejes configurados por cada size se leen uno, dos o tres ejes (representados en float64)
	size_t num_read;
	adxl355_values_t* start_buffer_ptr;

	if((adxl355_data == NULL) || (filep == NULL)){
		return 0;
	}
	if(!dev_opened){
		return 0;
	}

	if(size > buffer_count){
		return 0;
	}
	else{
		if(buffer_ptr-buffer_count >= input_buffer){
			start_buffer_ptr = buffer_ptr-buffer_count;
		}
		else{
			start_buffer_ptr = buffer_ptr + (ADXL_INPUT_BUFFER_SIZE-buffer_count);
		}

		if(start_buffer_ptr+size < ((input_buffer+ADXL_INPUT_BUFFER_SIZE))){
			memcpy(prx, start_buffer_ptr, (sizeof(adxl355_values_t)*size));
		}
		else{
			num_read = (input_buffer+ADXL_INPUT_BUFFER_SIZE)-start_buffer_ptr;
			memcpy(prx, start_buffer_ptr, (sizeof(adxl355_values_t)*num_read));
			start_buffer_ptr = &input_buffer[0];
			memcpy(prx, start_buffer_ptr, (sizeof(adxl355_values_t)*(size-num_read)));
		}

		buffer_count -= size;

	}

	return size;

}


/**
 *
 * @param filep
 * @param request
 * @param args
 * @return
 */
static retval_t adxl355_ioctl(dev_file_t* filep, uint16_t request, void* args){

	adxl355_driver_ioctl_cmd_t cmd;

	if((adxl355_data == NULL) || (filep == NULL)){
		return RET_ERROR;
	}
	if(!dev_opened){
		return RET_ERROR;
	}

	cmd = (spi_driver_ioctl_cmd_t) request;

	switch(cmd){
	case ADXL355_SET_MODE:
		ADXL355_SetMode(adxl355_data, (ADXL355_Mode_t) args);
		break;

	case ADXL355_SET_ODR:
		ADXL355_SetODR(adxl355_data, (ADXL355_ODR_t) args);
		break;

	case ADXL355_SET_SCALE:
		ADXL355_SetFullScale(adxl355_data, (ADXL355_Fullscale_t) args);
		break;

	case ADXL355_SET_AXIS:
		ADXL355_SetAxis(adxl355_data, (uint32_t) args);
		break;

	default:
		return RET_ERROR;
		break;
	}

	return RET_OK;
}


static void read_timer_func(void const * argument){
	uint8_t fifo_samples;
	AxesRaw_adxl_t* adxl_buff;
	uint16_t i;
	ADXL355_GetFifoStoredSamples(adxl355_data, &fifo_samples);
	if((fifo_samples/3) >= NUM_SAMPLES_TO_READ){
		adxl_buff = ytMalloc(NUM_SAMPLES_TO_READ*sizeof(AxesRaw_adxl_t));
		ADXL355_GetAccAxesRaw_FIFO_Mode(adxl355_data, adxl_buff, NUM_SAMPLES_TO_READ);


		if(buffer_ptr+NUM_SAMPLES_TO_READ >= input_buffer+ADXL_INPUT_BUFFER_SIZE){
			for(i=0; i<NUM_SAMPLES_TO_READ; i++){
				if(buffer_ptr >= input_buffer+ADXL_INPUT_BUFFER_SIZE){
					buffer_ptr = &input_buffer[0];
				}
				buffer_ptr[0].x_val =((float32_t) (((float32_t)adxl_buff[i].AXIS_X) * adxl355_data->resolution))/4096;
				buffer_ptr[0].y_val =((float32_t) (((float32_t)adxl_buff[i].AXIS_Y) * adxl355_data->resolution))/4096;
				buffer_ptr[0].z_val =((float32_t) (((float32_t)adxl_buff[i].AXIS_Z) * adxl355_data->resolution))/4096;
				buffer_ptr ++;
			}
		}
		else{
			for(i=0; i<NUM_SAMPLES_TO_READ; i++){
				buffer_ptr[0].x_val =((float32_t) (((float32_t)adxl_buff[i].AXIS_X) * adxl355_data->resolution))/4096;
				buffer_ptr[0].y_val =((float32_t) (((float32_t)adxl_buff[i].AXIS_Y) * adxl355_data->resolution))/4096;
				buffer_ptr[0].z_val =((float32_t) (((float32_t)adxl_buff[i].AXIS_Z) * adxl355_data->resolution))/4096;
				buffer_ptr ++;
			}
		}

		buffer_count+= NUM_SAMPLES_TO_READ;

		if(buffer_count > ADXL_INPUT_BUFFER_SIZE){
			buffer_count = ADXL_INPUT_BUFFER_SIZE;
		}

		ytFree(adxl_buff);
	}
}

/* Register the init functions in the kernel Init system */
init_device_1(adxl355_init);	//Level 1 Driver. It is initialized after Level 0 drivers do
exit_device_1(adxl355_exit);

#endif
