/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * at86rf215_arch_temp.c
 *
 *  Created on: 11/07/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file at86rf215_arch_temp.c
 */

#include "system_api.h"
#include "at86rf215_const.h"
#include "at86rf215_arch.h"
#include "at86rf215_utils.h"
#include "spi_driver.h"


/**
 *
 * @param at86rf215_data
 * @param spi_dev
 * @return
 */
__weak retval_t at86rf215_arch_init(at86rf215_data_t* at86rf215_data, char* spi_dev){
	return RET_ERROR;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
__weak retval_t at86rf215_arch_deInit(at86rf215_data_t* at86rf215_data){
	return RET_ERROR;
}


/**
 *
 * @param at86rf215_data
 * @param reg_addr
 * @param read_val
 * @return
 */
__weak retval_t at86rf215_arch_read_reg(at86rf215_data_t* at86rf215_data, uint16_t reg_addr, uint8_t* read_val){
	return RET_ERROR;
}


/**
 *
 * @param at86rf215_data
 * @param reg_addr
 * @param write_val
 * @return
 */
__weak retval_t at86rf215_arch_write_reg(at86rf215_data_t* at86rf215_data, uint16_t reg_addr, uint8_t write_val){
	return RET_ERROR;
}


/**
 *
 * @param at86rf215_data
 * @param reg_addr
 * @param read_val
 * @param size
 * @return
 */
__weak retval_t at86rf215_arch_read_multi_reg(at86rf215_data_t* at86rf215_data, uint16_t reg_addr, uint8_t* read_val, uint16_t size){
	return RET_ERROR;
}


/**
 *
 * @param at86rf215_data
 * @param reg_addr
 * @param write_val
 * @param size
 * @return
 */
__weak retval_t at86rf215_arch_write_multi_reg(at86rf215_data_t* at86rf215_data, uint16_t reg_addr, uint8_t* write_val, uint16_t size){
	return RET_ERROR;
}


/**
 *
 * @param cc2500_data
 * @param rx_data
 * @param size
 * @return
 */
__weak retval_t at86rf215_arch_read_RxFrameBuffer(at86rf215_data_t* at86rf215_data, uint8_t* rx_data, uint16_t size){
	return RET_ERROR;
}


/**
 *
 * @param at86rf215_data
 * @param tx_data
 * @param size
 * @return
 */
__weak retval_t at86rf215_arch_write_TxFrameBuffer(at86rf215_data_t* at86rf215_data, uint8_t* tx_data, uint16_t size){
	return RET_ERROR;
}


/**
 *
 * @param at86rf215_data
 * @param status
 * @return
 */
__weak retval_t at86rf215_arch_read_status(at86rf215_data_t* at86rf215_data, rf_cmd_status_t* status){
	return RET_ERROR;
}


/**
 *
 * @param at86rf215_data
 * @param cmd
 * @return
 */
__weak retval_t at86rf215_arch_send_cmd(at86rf215_data_t* at86rf215_data, rf_cmd_state_t cmd){
	return RET_ERROR;
}


/**
 *
 * @param at86rf215_data
 * @param cmd
 * @return
 */
__weak retval_t at86rf215_arch_change_state(at86rf215_data_t* at86rf215_data, rf_cmd_state_t cmd){
	return RET_ERROR;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
__weak retval_t at86rf215_arch_clear_interrupt(at86rf215_data_t* at86rf215_data){
	return RET_ERROR;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
__weak retval_t at86rf215_arch_disable_interrupt(at86rf215_data_t* at86rf215_data){
	return RET_ERROR;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
__weak retval_t at86rf215_arch_enable_interrupt(at86rf215_data_t* at86rf215_data){
	return RET_ERROR;
}

/**
 *
 * @param at86rf215_data
 * @return
 */
__weak retval_t at86rf215_arch_chip_reset(at86rf215_data_t* at86rf215_data){
	return RET_ERROR;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
__weak retval_t at86rf215_arch_transceiver_reset(at86rf215_data_t* at86rf215_data){
	return RET_ERROR;
}


/**
 *
 * @param at86rf215_data
 */
__weak void at86rf215_arch_swap_band(at86rf215_data_t* at86rf215_data){
	return;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
__weak retval_t at86rf215_arch_transceiver_sleep(at86rf215_data_t* at86rf215_data, at86rf215_band_t band){
	return RET_ERROR;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
__weak retval_t at86rf215_arch_deep_sleep(at86rf215_data_t* at86rf215_data){
	return RET_ERROR;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
__weak retval_t at86rf215_arch_transceiver_wakeup(at86rf215_data_t* at86rf215_data, at86rf215_band_t band){
	return RET_ERROR;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
__weak retval_t at86rf215_arch_deep_sleep_wakeup(at86rf215_data_t* at86rf215_data){
	return RET_ERROR;
}



/**
 *
 * @param at86rf215_data
 * @return
 */
__weak retval_t at86rf215_arch_power_on(at86rf215_data_t* at86rf215_data){
	return RET_ERROR;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
__weak retval_t at86rf215_arch_power_off(at86rf215_data_t* at86rf215_data){
	return RET_ERROR;
}
