/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * at86rf215_core.c
 *
 *  Created on: 17/08/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file at86rf215_core.c
 */

#include "system_api.h"
#include "at86rf215_core.h"
#include "at86rf215_const.h"
#include "at86rf215_arch.h"
#include "at86rf215_config.h"


#define AT86RF215_SEND_TIMEOUT		50
#define AT86RF215_RCV_TIMEOUT		50


// Default AT86RF215 data
#define DEFAULT_CH_MODE				MODE_IEEE
#define DEFAULT_CH0_CENTER_FREQ		2400400
#define DEFAULT_CH_SPACING			400000
#define DEFAULT_CH_NUMBER			0
#define DEFAULT_MODULATION			BFSK
#define DEFAULT_BAUD_RATE			200000
#define DEFAULT_OUTPUT_POWER		ZERO_DBM


/**
 *
 * @param cs_pin
 * @param irq_pin
 * @param band
 * @param interrupt_cb_func
 * @param args
 * @return
 */
at86rf215_data_t* new_at86rf215_data(gpio_pin_t cs_pin, gpio_pin_t irq_pin, at86rf215_band_t band, ytProcessFunc_t interrupt_cb_func, void* args){

	at86rf215_data_t* new_at86rf215_data;

	if((new_at86rf215_data = (at86rf215_data_t*) ytMalloc(sizeof(at86rf215_data_t))) == NULL){
		return NULL;
	}

	new_at86rf215_data->spi_id = 0;
	new_at86rf215_data->cs_pin = cs_pin;
	new_at86rf215_data->irq_pin = irq_pin;
	new_at86rf215_data->band = band;
	new_at86rf215_data->interrupt_cb_func = interrupt_cb_func;
	new_at86rf215_data->interrupt_cb_arg = args;
	new_at86rf215_data->transmitting_packet = 0;
	new_at86rf215_data->receiving_packet = 0;
	new_at86rf215_data->config.ch_mode = DEFAULT_CH_MODE;
	new_at86rf215_data->config.ch0_center_freq = DEFAULT_CH0_CENTER_FREQ;
	new_at86rf215_data->config.ch_spacing = DEFAULT_CH_SPACING;
	new_at86rf215_data->config.ch_number = DEFAULT_CH_NUMBER;
	new_at86rf215_data->config.modulation = DEFAULT_MODULATION;
	new_at86rf215_data->config.baud_rate = DEFAULT_BAUD_RATE;
	new_at86rf215_data->config.output_pwr = DEFAULT_OUTPUT_POWER;
	new_at86rf215_data->at86rf215_mutex_id = ytMutexCreate();
	new_at86rf215_data->at86rf215_rx_time = 0;
	new_at86rf215_data->at86rf215_tx_time = 0;

	return new_at86rf215_data;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t delete_at86rf215_data(at86rf215_data_t* at86rf215_data){

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	ytMutexDelete(at86rf215_data->at86rf215_mutex_id);
	ytFree(at86rf215_data);

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param at86rf215_init_config
 * @param spi_dev
 * @return
 */
retval_t at86rf215_hw_init(at86rf215_data_t* at86rf215_data, at86rf215_config_t* at86rf215_init_config, char* spi_dev){

	uint8_t read_val = 0;
	at86rf215_irq_pin_config_t irq_pin_config;
	at86rf215_clko_pin_config_t clko_pin_config;
	at86rf215_xoc_config_t xoc_config;
	at86rf215_bmdvc_config_t bmdvc_config;

	if(at86rf215_data == NULL || at86rf215_init_config == NULL){
		return RET_ERROR;
	}

	// Initialize the necessary HW resources: SPI and GPIOs
	if(at86rf215_arch_init(at86rf215_data, spi_dev) != RET_OK){
		return RET_ERROR;
	}

	// Reset the entire device. After this action, all registers are set to their default values.
	if(at86rf215_arch_chip_reset(at86rf215_data) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	// Check part number
	if(at86rf215_arch_read_reg(at86rf215_data, RG_RF_PN, &read_val) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}
	if(read_val != AT86RF215_PN){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	// Check version number
	if(at86rf215_arch_read_reg(at86rf215_data, RG_RF_VN, &read_val) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}
	if(read_val != AT86RF215_VN){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}


	//--------------------------------------------------------------------------
	// Common Chip Register Configuration
	// These registers maintain their configuration during state DEEP_SLEEP
	//--------------------------------------------------------------------------

	// IRQ Pin Configuration (Register setting is maintained during state DEEP_SLEEP)
	irq_pin_config.irqmm = 1;			// Masked IRQ reasons do appear in IRQS register
	irq_pin_config.irqp = 0;			// Active high
	irq_pin_config.drv = RF_DRV2;		// Output Driver Strength of Pads: 2 mA
	if(at86rf215_config_irq_pin(at86rf215_data, irq_pin_config) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	// Clock Output Pin Configuration (Register setting is maintained during state DEEP_SLEEP)
	clko_pin_config.drv = RF_DRVCLKO2;	// Output Driver Strength CLKO: 2 mA
	clko_pin_config.os = 0;				// OFF
	if(at86rf215_config_clko_pin(at86rf215_data, clko_pin_config) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	// Crystal Oscillator Configuration (Register setting is maintained during state DEEP_SLEEP)
	xoc_config.fs = 1;				// Crystal Oscillator fast start-up enable: Enabled
	xoc_config.trim = 0;			// Crystal Oscillator Trim: +0.0pF
	if(at86rf215_config_xoc(at86rf215_data, xoc_config) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	// Transceiver Battery Monitor Control and Digital Voltage Regulator Control
	// (Register setting is maintained during state DEEP_SLEEP)
	bmdvc_config.bmhr = 0;			// Threshold range for the battery monitor: Low range enabled
	bmdvc_config.bmvth = 1;			// Battery Monitor Voltage Threshold: 1.75 V
	if(at86rf215_config_bmdvc(at86rf215_data, bmdvc_config) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}


	//--------------------------------------------------------------------------
	// The rest of the registers do not maintain their configuration during state DEEP_SLEEP
	//--------------------------------------------------------------------------

	at86rf215_data->config = *at86rf215_init_config;
	if(at86rf215_hw_config(at86rf215_data) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_hw_deInit(at86rf215_data_t* at86rf215_data){

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}
	at86rf215_arch_power_off(at86rf215_data);
	at86rf215_arch_deInit(at86rf215_data);

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_hw_config(at86rf215_data_t* at86rf215_data){

	at86rf215_fcs_config_t fcs_config;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}


	// Selection of causes that generate Radio IRQ events
	if(at86rf215_config_radio_irq_events(at86rf215_data, RF_IRQ_ALL_IRQ, FS_DISABLE) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}
	if(at86rf215_config_radio_irq_events(at86rf215_data, RF_IRQ_TRXERR, FS_ENABLE) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}
	if(at86rf215_config_radio_irq_events(at86rf215_data, RF_IRQ_BATLOW, FS_ENABLE) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	// Selection of causes that generate Baseband IRQ events
	if(at86rf215_config_baseband_irq_events(at86rf215_data, BB_IRQ_ALL_IRQ, FS_DISABLE) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}
	if(at86rf215_config_baseband_irq_events(at86rf215_data, BB_IRQ_TXFE, FS_ENABLE) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}
	if(at86rf215_config_baseband_irq_events(at86rf215_data, BB_IRQ_RXFS, FS_ENABLE) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}
	if(at86rf215_config_baseband_irq_events(at86rf215_data, BB_IRQ_RXFE, FS_ENABLE) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	// Chip Operating Mode Configuration: TRX09 and TRX24 in baseband mode
	if(at86rf215_set_op_mode(at86rf215_data, MODE_BBRF) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	// Channel configuration ---------------------------------------------------
	if(at86rf215_config_channel(at86rf215_data) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	// Baseband configuration --------------------------------------------------
	// PHY Control - Continuous Transmit: Disabled
	if(at86rf215_continuous_transmit(at86rf215_data, FS_DISABLE) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	// PHY Control - Frame Check Sequence Configuration
	fcs_config.fcsfe = FS_ENABLE;
	fcs_config.txafcs = 1;		// FCS autonomously calculated
	fcs_config.fcst = 0;		// FCS type 32-bit
	if(at86rf215_config_frame_check_sequence(at86rf215_data, fcs_config) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	// PHY Control - Baseband Enable (Check Chip Operating Mode Configuration)
	if(at86rf215_baseband_enable(at86rf215_data, FS_ENABLE) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	// PHY Control - PHY Type (Modulation)
	if(at86rf215_set_modulation(at86rf215_data) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	// PHY Control - Data Rate
	if(at86rf215_set_datarate(at86rf215_data) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	// PHY Control - PHY Default Configuration
	// This function configures the rest of the PHY Control registers to a
	// default value. It should always be called after set_modulation and set_datarate.
	if(at86rf215_default_phy_config(at86rf215_data) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	// Transmitter Front-End configuration --------------------------------------

	// Transmitter Front-End configuration - Output Power
	if(at86rf215_set_output_power(at86rf215_data) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	// Transmitter Front-End configuration - TXFE Default Configuration
	// This function configures the rest of the TXFE registers to a default value.
	// It should always be called after set_output_power.
	if(at86rf215_default_txfe_config(at86rf215_data) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	// Receiver Front-End configuration ----------------------------------------
	// Energy measurement and AGC configuration --------------------------------

	// Receiver Front-End configuration - RXFE Default Configuration for MRFSK
	if(at86rf215_default_rxfe_config(at86rf215_data) != RET_OK){
		at86rf215_hw_deInit(at86rf215_data);
		return RET_ERROR;
	}

	// Disable interrupts
	at86rf215_arch_disable_interrupt(at86rf215_data);


	// TODO: I/Q interface configuration

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param data
 * @param size
 * @return
 */
retval_t at86rf215_send_data(at86rf215_data_t* at86rf215_data, uint8_t* data, uint16_t size){

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(size > AT86RF215_MAX_FRAME_LENGTH){
		return RET_ERROR;
	}

	// Before the frame can be transmitted, the frame length needs to be
	// written to the registers BBCn_TXFLL and BBCn_TXFLH.
	// size == at86rf215_data->packet_length implies that the registers
	// BBCn_TXFLL and BBCn_TXFLH are correctly configured.

	ytMutexWait(at86rf215_data->at86rf215_mutex_id, YT_WAIT_FOREVER);

	// Check that another packet is not being transmitted
	if(at86rf215_data->transmitting_packet){
		if((ytGetSysTickMilliSec() - at86rf215_data->at86rf215_tx_time) > AT86RF215_SEND_TIMEOUT){
			at86rf215_data->transmitting_packet = 0;
		}
		else{
			ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
			return RET_ERROR;
		}
	}

	// Set the transmitting_packet flag
	at86rf215_data->at86rf215_tx_time = ytGetSysTickMilliSec();
	at86rf215_data->transmitting_packet = 1;


	// Check that another packet is not being received
	// It is not possible to transmit a packet while another is being received
	if (at86rf215_data->receiving_packet){
		if((ytGetSysTickMilliSec() - at86rf215_data->at86rf215_rx_time) > AT86RF215_RCV_TIMEOUT){
			at86rf215_data->receiving_packet = 0;
		}
		at86rf215_data->transmitting_packet = 0;
		ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
		return RET_ERROR;
	}


	// Go to the known state TRXOFF
	if(at86rf215_arch_change_state(at86rf215_data, RF_TRXOFF) != RET_OK){
		at86rf215_data->transmitting_packet = 0;
		ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
		return RET_ERROR;
	}

	// Set packet length
	if(at86rf215_set_packet_length(at86rf215_data, size) != RET_OK){
		at86rf215_data->transmitting_packet = 0;
		ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
		return RET_ERROR;
	}

	// The frame content is written to the TX frame buffer
	if(at86rf215_arch_write_TxFrameBuffer(at86rf215_data, data, size) != RET_OK){
		at86rf215_data->transmitting_packet = 0;
		ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
		return RET_ERROR;
	}

	// Go to the state TXPREP
	if(at86rf215_arch_change_state(at86rf215_data, RF_TXPREP) != RET_OK){
		at86rf215_data->transmitting_packet = 0;
		ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
		return RET_ERROR;
	}

	// Enable interrupt
	if(at86rf215_arch_enable_interrupt(at86rf215_data) != RET_OK){
		at86rf215_data->transmitting_packet = 0;
		ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
		return RET_ERROR;
	}

	// From state TXPREP the transmission is triggered by writing the command TX
	if(at86rf215_arch_send_cmd(at86rf215_data, RF_TX) != RET_OK){
		at86rf215_data->transmitting_packet = 0;
		ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
		return RET_ERROR;
	}

	ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_set_mode_rx(at86rf215_data_t* at86rf215_data){

	rf_cmd_status_t status;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	ytMutexWait(at86rf215_data->at86rf215_mutex_id, YT_WAIT_FOREVER);

	// Check that another packet is not being transmitted
	if(at86rf215_data->transmitting_packet){
		if((ytGetSysTickMilliSec() - at86rf215_data->at86rf215_tx_time) > AT86RF215_SEND_TIMEOUT){
			at86rf215_data->transmitting_packet = 0;
		}
		else{
			ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
			return RET_ERROR;
		}
	}

	// Check that another packet is not being received
	if (at86rf215_data->receiving_packet){
		if((ytGetSysTickMilliSec() - at86rf215_data->at86rf215_rx_time) > AT86RF215_RCV_TIMEOUT){
			at86rf215_data->receiving_packet = 0;
		}
		else{
			ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
			return RET_ERROR;
		}
	}

	// Read Status
	if(at86rf215_arch_read_status(at86rf215_data, &status) != RET_OK){
		ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
		return RET_ERROR;
	}

	if(status != STATUS_RF_RX){

		// Go to the known state TRXOFF
		if(at86rf215_arch_change_state(at86rf215_data, RF_TRXOFF) != RET_OK){
			ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
			return RET_ERROR;
		}

		// Enable interrupt
		if(at86rf215_arch_enable_interrupt(at86rf215_data) != RET_OK){
			ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
			return RET_ERROR;
		}

		// The state RX is reached from the state TXPREP or from the state TRXOFF
		// by writing the command RX to the register RFn_CMD.
		if(at86rf215_arch_send_cmd(at86rf215_data, RF_RX) != RET_OK){
			ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
			return RET_ERROR;
		}
	}
	ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_power_off(at86rf215_data_t* at86rf215_data){
	// Disable interrupt
	if(at86rf215_arch_disable_interrupt(at86rf215_data) != RET_OK){
		return RET_ERROR;
	}
	return at86rf215_arch_power_off(at86rf215_data);
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_set_mode_sleep(at86rf215_data_t* at86rf215_data){

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	ytMutexWait(at86rf215_data->at86rf215_mutex_id, YT_WAIT_FOREVER);

	// Check that another packet is not being transmitted
	if(at86rf215_data->transmitting_packet){
		if((ytGetSysTickMilliSec() - at86rf215_data->at86rf215_tx_time) > AT86RF215_SEND_TIMEOUT){
			at86rf215_data->transmitting_packet = 0;
		}
		else{
			ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
			return RET_ERROR;
		}
	}

	// Check that another packet is not being received
	if (at86rf215_data->receiving_packet){
		if((ytGetSysTickMilliSec() - at86rf215_data->at86rf215_rx_time) > AT86RF215_RCV_TIMEOUT){
			at86rf215_data->receiving_packet = 0;
		}
		else{
			ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
			return RET_ERROR;
		}
	}

	at86rf215_data->transmitting_packet = 0;
	at86rf215_data->receiving_packet = 0;

	at86rf215_power_off(at86rf215_data);

	ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_power_on(at86rf215_data_t* at86rf215_data){

	// Wake-up the AT86RF215 from state DEEP_SLEEP
	// This action causes the transition from state DEEP_SLEEP to TRXOFF in both transceivers.
	if(at86rf215_arch_power_on(at86rf215_data) != RET_OK){
		return RET_ERROR;
	}

	// Re-configure the transceiver
	if(at86rf215_hw_config(at86rf215_data) != RET_OK){
		return RET_ERROR;
	}

	// Disable interrupt
	if(at86rf215_arch_disable_interrupt(at86rf215_data) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
retval_t at86rf215_set_mode_idle(at86rf215_data_t* at86rf215_data){

	rf_cmd_status_t status;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	ytMutexWait(at86rf215_data->at86rf215_mutex_id, YT_WAIT_FOREVER);

	// Check that another packet is not being transmitted
	if(at86rf215_data->transmitting_packet){
		if((ytGetSysTickMilliSec() - at86rf215_data->at86rf215_tx_time) > AT86RF215_SEND_TIMEOUT){
			at86rf215_data->transmitting_packet = 0;
		}
		else{
			ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
			return RET_ERROR;
		}
	}

	// Check that another packet is not being received
	if (at86rf215_data->receiving_packet){
		if((ytGetSysTickMilliSec() - at86rf215_data->at86rf215_rx_time) > AT86RF215_RCV_TIMEOUT){
			at86rf215_data->receiving_packet = 0;
		}
		else{
			ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
			return RET_ERROR;
		}
	}

	at86rf215_data->transmitting_packet = 0;
	at86rf215_data->receiving_packet = 0;

	at86rf215_power_on(at86rf215_data);
	ytMutexRelease(at86rf215_data->at86rf215_mutex_id);

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
int8_t at86rf215_get_last_rssi(at86rf215_data_t* at86rf215_data){

	uint16_t rf24_std_offset = 0x0000;
	int8_t rssi = AT86RF215_INVALID_RSSI;		//  A value of 127 indicates that the RSSI value is invalid.

	if(at86rf215_data == NULL){
		return AT86RF215_INVALID_RSSI;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	if(at86rf215_arch_read_reg(at86rf215_data, (RG_RF09_RSSI + rf24_std_offset), &rssi) != RET_OK){
		return AT86RF215_INVALID_RSSI;
	}

	if((rssi < AT86RF215_MIN_RSSI) ||
	   (rssi > AT86RF215_MAX_RSSI) ){
		return AT86RF215_INVALID_RSSI;
	}
	return rssi;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
int8_t at86rf215_get_last_edv(at86rf215_data_t* at86rf215_data){

	uint16_t rf24_std_offset = 0x0000;
	int8_t edv = AT86RF215_INVALID_EDV;		//  A value of 127 indicates that the EDV value is invalid.

	if(at86rf215_data == NULL){
		return AT86RF215_INVALID_EDV;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	if(at86rf215_arch_read_reg(at86rf215_data, (RG_RF09_EDV + rf24_std_offset), &edv) != RET_OK){
		return AT86RF215_INVALID_EDV;
	}

	if((edv < AT86RF215_MIN_EDV) ||
	   (edv > AT86RF215_MAX_EDV) ){
		return AT86RF215_INVALID_EDV;
	}
	return edv;
}


/**
 *
 * @param at86rf215_data
 * @return
 */
// This routine is called when an interruption of the AT86RF215 is generated.
// ATTENTION! This routine is not called directly from the interruption.
// This routine is executed in another thread, which is controlled by a semaphore
// that is released from the interruption callback. All these actions are
// controlled by the PHY layer of the communication stack.
uint16_t at86rf215_irq_routine(at86rf215_data_t* at86rf215_data){

	uint16_t ret = 0;
	at86rf215_irq_events_t irq_events;

	at86rf215_get_irq_events(at86rf215_data, &irq_events);

	ytMutexWait(at86rf215_data->at86rf215_mutex_id, YT_WAIT_FOREVER);

	// Receiver Frame Start Interrupt
	if(irq_events.IRQ_BBC0_RXFS || irq_events.IRQ_BBC1_RXFS){
		if(!at86rf215_data->transmitting_packet){
			at86rf215_data->receiving_packet = 1;
			at86rf215_data->at86rf215_rx_time = ytGetSysTickMilliSec();
		}
	}

	// Transmitter Frame End Interrupt
	if(irq_events.IRQ_BBC0_TXFE || irq_events.IRQ_BBC1_TXFE){
		if(at86rf215_data->transmitting_packet){
			at86rf215_data->transmitting_packet = 0;
			ret |= RET_PCKT_SENT;
		}
	}

	// Receiver Frame End Interrupt
	if(irq_events.IRQ_BBC0_RXFE || irq_events.IRQ_BBC1_RXFE){
		if(!at86rf215_data->receiving_packet){
			at86rf215_arch_send_cmd(at86rf215_data, RF_RX);		// Return to state RX
		}
		else{
			ret |= RET_PCKT_RCV;
		}
	}

	if(irq_events.IRQ_RF09_TRXERR || irq_events.IRQ_RF24_TRXERR	||
	   irq_events.IRQ_RF09_BATLOW || irq_events.IRQ_RF24_BATLOW ){
		ret |= RET_TRX_ERROR;
	}

	ytMutexRelease(at86rf215_data->at86rf215_mutex_id);
	return ret;
}


/**
 *
 * @param at86rf215_data
 * @param num_rcv_bytes
 * @return
 */
retval_t at86rf215_read_num_rcv_bytes(at86rf215_data_t* at86rf215_data, uint8_t* num_rcv_bytes){

	ytMutexWait(at86rf215_data->at86rf215_mutex_id, YT_WAIT_FOREVER);

	at86rf215_get_rcv_packet_length(at86rf215_data, (uint16_t*) num_rcv_bytes);

	ytMutexRelease(at86rf215_data->at86rf215_mutex_id);

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param packet_data
 * @param size
 * @return
 */
retval_t at86rf215_read_rcv_data(at86rf215_data_t* at86rf215_data, uint8_t* packet_data, uint16_t size){

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(size > AT86RF215_MAX_FRAME_LENGTH){
		return RET_ERROR;
	}

	ytMutexWait(at86rf215_data->at86rf215_mutex_id, YT_WAIT_FOREVER);

	at86rf215_arch_read_RxFrameBuffer(at86rf215_data, packet_data, size);

	at86rf215_data->receiving_packet = 0;

	ytMutexRelease(at86rf215_data->at86rf215_mutex_id);

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param size
 * @return
 */
retval_t at86rf215_flush_last_rcv_data(at86rf215_data_t* at86rf215_data){

	ytMutexWait(at86rf215_data->at86rf215_mutex_id, YT_WAIT_FOREVER);
	at86rf215_data->receiving_packet = 0;
	ytMutexRelease(at86rf215_data->at86rf215_mutex_id);

	return RET_OK;
}

