/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * at86rf215_config.c
 *
 *  Created on: 17/08/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file at86rf215_config.c
 */

#include "system_api.h"
#include "at86rf215_arch.h"
#include "at86rf215_config.h"
#include "at86rf215_config_mrfsk.h"
#include "at86rf215_core.h"
#include "at86rf215_const.h"
#include "at86rf215_types.h"
#include "at86rf215_utils.h"

#define CH_NUM_FR1_L	126030
#define CH_NUM_FR1_H	1340967
#define CH_NUM_FR2_L	126030
#define CH_NUM_FR2_H	1340967
#define CH_NUM_FR3_L	85700
#define CH_NUM_FR3_H	296172

#define FCS_32_LEN		4
#define FCS_16_LEN		2

/**
 *
 * @param at86rf215_data
 * @param irq_pin_config
 * @return
 */
retval_t at86rf215_config_irq_pin(at86rf215_data_t* at86rf215_data, at86rf215_irq_pin_config_t irq_pin_config){

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	// IRQ Mask Mode
	if(at86rf215_write_subreg(at86rf215_data, SR_RF_CFG_IRQMM, irq_pin_config.irqmm) != RET_OK){
		return RET_ERROR;
	}

	// IRQ Polarity
	if(at86rf215_write_subreg(at86rf215_data, SR_RF_CFG_IRQP, irq_pin_config.irqp) != RET_OK){
		return RET_ERROR;
	}

	// Output Driver Strength of Pads
	if(at86rf215_write_subreg(at86rf215_data, SR_RF_CFG_DRV, irq_pin_config.drv) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param irq_type
 * @param new_state
 * @return
 */
retval_t at86rf215_config_radio_irq_events(at86rf215_data_t* at86rf215_data, rf_irq_t radio_irq_type, at86rf215_functional_state_t new_state){

	uint16_t rf24_std_offset = 0x0000;
	uint8_t shift;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}


	// Determine the bit offset for sub-register based on the Radio IRQ type
	switch(radio_irq_type){

	// I/Q IF Synchronization Failure Interrupt Mask
	case RF_IRQ_IQIFSF:
		shift = IRQM_IQIFSF_SHIFT;
		break;

	// Transceiver Error Interrupt Mask
	case RF_IRQ_TRXERR:
		shift = IRQM_TRXERR_SHIFT;
		break;

	// Battery Low Interrupt Mask
	case RF_IRQ_BATLOW:
		shift = IRQM_BATLOW_SHIFT;
		break;

	// Energy Detection Completion Interrupt Mask
	case RF_IRQ_EDC:
		shift = IRQM_EDC_SHIFT;
		break;

	// Transceiver Ready Interrupt Mask
	case RF_IRQ_TRXRDY:
		shift = IRQM_TRXRDY_SHIFT;
		break;

	// Wake-up / Reset Interrupt Mask
	case RF_IRQ_WAKEUP:
		shift = IRQM_WAKEUP_SHIFT;
		break;

	// All Radio Interrupt Mask
	case RF_IRQ_ALL_IRQ:
		break;

	default:
		return RET_ERROR;
		break;
	}


	// Enable/Disable all Radio IRQ types in one operation
	if(radio_irq_type == RF_IRQ_ALL_IRQ){
		if(new_state == FS_ENABLE){
			if(at86rf215_arch_write_reg(at86rf215_data, (RG_RF09_IRQM + rf24_std_offset), RF_IRQ_ALL_IRQ) != RET_OK){
				return RET_ERROR;
			}
		}
		else{
			if(at86rf215_arch_write_reg(at86rf215_data, (RG_RF09_IRQM + rf24_std_offset), RF_IRQ_NO_IRQ) != RET_OK){
				return RET_ERROR;
			}
		}
	}

	// Enable/Disable a specific Radio IRQ type
	else{
		if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_IRQM + rf24_std_offset), (uint8_t)radio_irq_type, shift, (uint8_t)new_state) != RET_OK){
			return RET_ERROR;
		}
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param baseband_irq_type
 * @param new_state
 * @return
 */
retval_t at86rf215_config_baseband_irq_events(at86rf215_data_t* at86rf215_data, bb_irq_t baseband_irq_type, at86rf215_functional_state_t new_state){

	uint16_t rf24_std_offset = 0x0000;
	uint8_t shift;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}


	// Determine the bit offset for sub-register based on the Radio IRQ type
	switch(baseband_irq_type){

	// Frame Buffer Level Indication Interrupt Mask
	case BB_IRQ_FBLI:
		shift = IRQM_FBLI_SHIFT;
		break;

	// AGC Release Interrupt Mask
	case BB_IRQ_AGCR:
		shift = IRQM_AGCR_SHIFT;
		break;

	// AGC Hold Interrupt Mask
	case BB_IRQ_AGCH:
		shift = IRQM_AGCH_SHIFT;
		break;

	// Transmitter Frame End Interrupt Mask
	case BB_IRQ_TXFE:
		shift = IRQM_TXFE_SHIFT;
		break;

	// Receiver Extended Match Interrupt Mask
	case BB_IRQ_RXEM:
		shift = IRQM_RXEM_SHIFT;
		break;

	// Receiver Address Match Interrupt Mask
	case BB_IRQ_RXAM:
		shift = IRQM_RXAM_SHIFT;
		break;

	// Receiver Frame End Interrupt Mask
	case BB_IRQ_RXFE:
		shift = IRQM_RXFE_SHIFT;
		break;

	// Receiver Frame Start Interrupt Mask
	case BB_IRQ_RXFS:
		shift = IRQM_RXFS_SHIFT;
		break;

	// All Baseband Interrupt Mask
	case BB_IRQ_ALL_IRQ:
		break;

	default:
		return RET_ERROR;
		break;
	}


	// Enable/Disable all Baseband IRQ types in one operation
	if(baseband_irq_type == BB_IRQ_ALL_IRQ){
		if(new_state == FS_ENABLE){
			if(at86rf215_arch_write_reg(at86rf215_data, (RG_BBC0_IRQM + rf24_std_offset), BB_IRQ_ALL_IRQ) != RET_OK){
				return RET_ERROR;
			}
		}
		else{
			if(at86rf215_arch_write_reg(at86rf215_data, (RG_BBC0_IRQM + rf24_std_offset), BB_IRQ_NO_IRQ) != RET_OK){
				return RET_ERROR;
			}
		}
	}

	// Enable/Disable a specific Radio IRQ type
	else{
		if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_IRQM + rf24_std_offset), (uint8_t)baseband_irq_type, shift, (uint8_t)new_state) != RET_OK){
			return RET_ERROR;
		}
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param irq_events
 * @return
 */
retval_t at86rf215_get_irq_events(at86rf215_data_t* at86rf215_data, at86rf215_irq_events_t* irq_events){

	uint8_t i = 0;
	uint8_t reg_val[4];

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	// Read Radio IRQ events
	// Read IRQ events registers. They are four consecutive registers starting from RG_RF09_IRQS.
	if(at86rf215_arch_read_multi_reg(at86rf215_data, RG_RF09_IRQS, reg_val, 4) != RET_OK){
		return RET_ERROR;
	}

	// Build the IRQ events word
	for(i=0; i < 4; i++){
		*(((uint8_t*)(irq_events))+i) = reg_val[i];
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param clko_pin_config
 * @return
 */
retval_t at86rf215_config_clko_pin(at86rf215_data_t* at86rf215_data, at86rf215_clko_pin_config_t clko_pin_config){

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	// Output Driver Strength CLKO
	if(at86rf215_write_subreg(at86rf215_data, SR_RF_CLKO_DRV, clko_pin_config.drv) != RET_OK){
		return RET_ERROR;
	}

	// Clock Output Selection
	if(at86rf215_write_subreg(at86rf215_data, SR_RF_CLKO_OS, clko_pin_config.os) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param xoc_config
 * @return
 */
retval_t at86rf215_config_xoc(at86rf215_data_t* at86rf215_data, at86rf215_xoc_config_t xoc_config){

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	// Crystal Oscillator fast start-up enable
	if(at86rf215_write_subreg(at86rf215_data, SR_RF_XOC_FS, xoc_config.fs) != RET_OK){
		return RET_ERROR;
	}

	// Crystal Oscillator Trim
	if(at86rf215_write_subreg(at86rf215_data, SR_RF_XOC_TRIM, xoc_config.trim) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param bmdvc_config
 * @return
 */
retval_t at86rf215_config_bmdvc(at86rf215_data_t* at86rf215_data, at86rf215_bmdvc_config_t bmdvc_config){

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	// Threshold range for the battery monitor
	if(at86rf215_write_subreg(at86rf215_data, SR_RF_BMDVC_BMHR, bmdvc_config.bmhr) != RET_OK){
		return RET_ERROR;
	}

	// Battery Monitor Voltage Threshold
	if(at86rf215_write_subreg(at86rf215_data, SR_RF_BMDVC_BMVTH, bmdvc_config.bmvth) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param op_mode
 * @return
 */
retval_t at86rf215_set_op_mode(at86rf215_data_t* at86rf215_data, at86rf215_op_mode_t op_mode){

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	// Chip Operating Mode Configuration
	if(at86rf215_write_subreg(at86rf215_data, SR_RF_IQIFC1_CHPM, op_mode) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param ch_center_freq
 * @return
 */
retval_t at86rf215_set_ch0_center_freq(at86rf215_data_t* at86rf215_data, uint16_t ch0_center_freq){

	uint16_t rf24_std_offset = 0x0000;
	uint8_t low_byte;
	uint8_t high_byte;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	low_byte = (ch0_center_freq & CCF0L_CCF0L_MASK);
	high_byte = (ch0_center_freq >> 8);

	if(at86rf215_arch_write_reg(at86rf215_data, (RG_RF09_CCF0L + rf24_std_offset), low_byte) != RET_OK){
		return RET_ERROR;
	}
	if(at86rf215_arch_write_reg(at86rf215_data, (RG_RF09_CCF0H + rf24_std_offset), high_byte) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param ch_number
 * @return
 */
retval_t at86rf215_set_ch_number_ieee_mode(at86rf215_data_t* at86rf215_data, uint16_t ch_number){

	uint16_t rf24_std_offset = 0x0000;
	uint8_t low_byte;
	uint8_t high_byte;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	// The channel number is a 9-bit value
	low_byte = (ch_number & CNL_CNL_MASK);
	high_byte = ((ch_number >> 8) & CNM_CNH_MASK);

	if(at86rf215_arch_write_reg(at86rf215_data, (RG_RF09_CNL + rf24_std_offset), low_byte) != RET_OK){
		return RET_ERROR;
	}
	if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_CNM + rf24_std_offset), \
			CNM_CNH_MASK, CNM_CNH_SHIFT, high_byte) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param ch_number
 * @return
 */
retval_t at86rf215_set_ch_number_fr_mode(at86rf215_data_t* at86rf215_data, uint32_t ch_number){

	uint16_t rf24_std_offset = 0x0000;
	uint8_t low_byte;
	uint8_t mid_byte;
	uint8_t high_byte;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	// The channel number is a 24-bit value
	low_byte =  (ch_number & 0x000000FF);
	mid_byte =  (ch_number & 0x0000FF00) >> 8;
	high_byte = (ch_number & 0x00FF0000) >> 16;

	if(at86rf215_arch_write_reg(at86rf215_data, (RG_RF09_CNL + rf24_std_offset), low_byte) != RET_OK){
		return RET_ERROR;
	}
	if(at86rf215_arch_write_reg(at86rf215_data, (RG_RF09_CCF0L + rf24_std_offset), mid_byte) != RET_OK){
		return RET_ERROR;
	}
	if(at86rf215_arch_write_reg(at86rf215_data, (RG_RF09_CCF0H + rf24_std_offset), high_byte) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param ch_spacing
 * @return
 */
retval_t at86rf215_set_ch_spacing(at86rf215_data_t* at86rf215_data, uint8_t ch_spacing){

	uint16_t rf24_std_offset = 0x0000;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	if(at86rf215_arch_write_reg(at86rf215_data, (RG_RF09_CS + rf24_std_offset), ch_spacing) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param ch_mode
 * @return
 */
retval_t at86rf215_set_ch_mode(at86rf215_data_t* at86rf215_data, at86rf215_ch_mode_t ch_mode){

	uint16_t rf24_std_offset = 0x0000;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	if(at86rf215_write_subreg(at86rf215_data, (RG_RF09_CNM + rf24_std_offset), \
			CNM_CM_MASK, CNM_CM_SHIFT, (uint8_t)ch_mode) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param at86rf215_config
 * @return
 */
retval_t at86rf215_config_channel(at86rf215_data_t* at86rf215_data){

	uint32_t f_offset = 0;
	uint16_t ch0_center_freq = 0;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	// IEEE 802.15.4g-2012 compliant channel scheme
	if(at86rf215_data->config.ch_mode == MODE_IEEE){

		// Offset frequency
		if(at86rf215_data->band == RF24){
			f_offset = 1500000;
		}

		// Channel Center Frequency (CCF0)
		ch0_center_freq = (at86rf215_data->config.ch0_center_freq - f_offset) / AT86RF215_FREQ_RESOLUTION;
		if(at86rf215_set_ch0_center_freq(at86rf215_data, ch0_center_freq) != RET_OK){
			return RET_ERROR;
		}

		// Channel Number (CN)
		if(at86rf215_set_ch_number_ieee_mode(at86rf215_data, at86rf215_data->config.ch_number) != RET_OK){
			return RET_ERROR;
		}

		// Channel Spacing (CS)
		if(at86rf215_set_ch_spacing(at86rf215_data, (at86rf215_data->config.ch_spacing / (AT86RF215_FREQ_RESOLUTION * 1000))) != RET_OK){
			return RET_ERROR;
		}
	}

	// Fine resolution 1: 389.5 - 510.0 MHz with 99.182 Hz channel stepping
	else if(at86rf215_data->config.ch_mode == MODE_FR1){
		// This mode is only valid for the sub-1 GHz transceiver (RF09)
		if(at86rf215_data->band != RF09){
			return RET_ERROR;
		}

		// The channel number ranges from 126030 (389.5 MHz) to 1340967 (510 MHz)
		if((at86rf215_data->config.ch_number < CH_NUM_FR1_L) ||
		   (at86rf215_data->config.ch_number > CH_NUM_FR1_H) ){
			return RET_ERROR;
		}

		// Channel Number (CN)
		if(at86rf215_set_ch_number_fr_mode(at86rf215_data, at86rf215_data->config.ch_number) != RET_OK){
			return RET_ERROR;
		}
	}

	// Fine resolution 2: 779 - 1020 MHz with 198.364 Hz channel stepping
	else if(at86rf215_data->config.ch_mode == MODE_FR2){
		// This mode is only valid for the sub-1 GHz transceiver (RF09)
		if(at86rf215_data->band != RF09){
			return RET_ERROR;
		}

		// The channel number ranges from 126030 (779 MHz) to 1340967 (1020 MHz)
		if((at86rf215_data->config.ch_number < CH_NUM_FR2_L) ||
		   (at86rf215_data->config.ch_number > CH_NUM_FR2_H) ){
			return RET_ERROR;
		}

		// Channel Number (CN)
		if(at86rf215_set_ch_number_fr_mode(at86rf215_data, at86rf215_data->config.ch_number) != RET_OK){
			return RET_ERROR;
		}
	}

	// Fine resolution 3: 2400 - 2483.5 MHz with 396.728 Hz channel stepping
	else if(at86rf215_data->config.ch_mode == MODE_FR3){
		// This mode is only valid for the 2.4 GHz transceiver (RF24)
		if(at86rf215_data->band != RF24){
			return RET_ERROR;
		}

		// The channel number ranges from 85700 (2400 MHz) to 296172 (2483.5 MHz)
		if((at86rf215_data->config.ch_number < CH_NUM_FR3_L) ||
		   (at86rf215_data->config.ch_number > CH_NUM_FR3_H) ){
			return RET_ERROR;
		}

		// Channel Number (CN)
		if(at86rf215_set_ch_number_fr_mode(at86rf215_data, at86rf215_data->config.ch_number) != RET_OK){
			return RET_ERROR;
		}
	}

	else{
		return RET_ERROR;
	}

	// Channel mode
	// A write access to the register RFn_CNM applies the current channel register values and
	// makes the frequency changes effective. If register RFn_CNM is not written the channel
	// frequency is not changed. The register RFn_CNM must always be written last.
	if(at86rf215_set_ch_mode(at86rf215_data, at86rf215_data->config.ch_mode) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param new_state
 */
retval_t at86rf215_continuous_transmit(at86rf215_data_t* at86rf215_data, at86rf215_functional_state_t new_state){

	uint16_t rf24_std_offset = 0x0000;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_PC + rf24_std_offset), \
			PC_CTX_MASK, PC_CTX_SHIFT, (uint8_t)new_state) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param fcs_config
 * @return
 */
retval_t at86rf215_config_frame_check_sequence(at86rf215_data_t* at86rf215_data, at86rf215_fcs_config_t fcs_config){

	uint16_t rf24_std_offset = 0x0000;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_PC + rf24_std_offset), \
			PC_FCSFE_MASK, PC_FCSFE_SHIFT, (uint8_t)fcs_config.fcsfe) != RET_OK){
		return RET_ERROR;
	}
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_PC + rf24_std_offset), \
			PC_TXAFCS_MASK, PC_TXAFCS_SHIFT, (uint8_t)fcs_config.txafcs) != RET_OK){
		return RET_ERROR;
	}
	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_PC + rf24_std_offset), \
			PC_FCST_MASK, PC_FCST_SHIFT, (uint8_t)fcs_config.fcst) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param fcs_config
 * @return
 */
retval_t at86rf215_get_fcs_status(at86rf215_data_t* at86rf215_data, at86rf215_fcs_status_t* fcs_status){

	uint16_t rf24_std_offset = 0x0000;
	uint8_t read_status;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	if(at86rf215_read_subreg(at86rf215_data, (RG_BBC0_PC + rf24_std_offset), \
			PC_FCSOK_MASK, PC_FCSOK_SHIFT, &read_status) != RET_OK){
		return RET_ERROR;
	}

	*fcs_status = (at86rf215_fcs_status_t)read_status;

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param fcs_length
 * @return
 */
retval_t at86rf215_get_fcs_length(at86rf215_data_t* at86rf215_data, uint8_t* fcs_length){

	uint16_t rf24_std_offset = 0x0000;
	uint8_t read_val = 0;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	// Check if the automatic handling of the Frame Check Sequence is enabled
	if(at86rf215_read_subreg(at86rf215_data, (RG_BBC0_PC + rf24_std_offset), PC_TXAFCS_MASK, PC_TXAFCS_SHIFT, &read_val) != RET_OK){
		return RET_ERROR;
	}

	// Transmitter Auto Frame Check Sequence is disabled
	if(read_val == FS_DISABLE){
		*fcs_length = 0;
	}

	// Transmitter Auto Frame Check Sequence is enabled
	else{

		// Check the TX FCS Type
		if(at86rf215_read_subreg(at86rf215_data, (RG_BBC0_PC + rf24_std_offset), PC_FCST_MASK, PC_FCST_SHIFT, &read_val) != RET_OK){
			return RET_ERROR;
		}

		// FCS type 32-bit
		if(read_val == 0){
			*fcs_length = FCS_32_LEN;
		}

		// FCS type 16-bit
		else{
			*fcs_length = FCS_16_LEN;
		}
	}
	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param new_state
 * @return
 */
retval_t at86rf215_baseband_enable(at86rf215_data_t* at86rf215_data, at86rf215_functional_state_t new_state){

	uint16_t rf24_std_offset = 0x0000;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_PC + rf24_std_offset), \
			PC_BBEN_MASK, PC_BBEN_SHIFT, (uint8_t)new_state) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param modulation
 * @return
 */
retval_t at86rf215_set_modulation(at86rf215_data_t* at86rf215_data){

	uint16_t rf24_std_offset = 0x0000;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}


	switch(at86rf215_data->config.modulation){

	case PHYOFF:
		// PHY Type: OFF
		if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_PC + rf24_std_offset), PC_PT_MASK, PC_PT_SHIFT, (uint8_t)BB_PHYOFF) != RET_OK){
			return RET_ERROR;
		}
		break;

	case BFSK:
		// PHY Type: MR-FSK
		if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_PC + rf24_std_offset), PC_PT_MASK, PC_PT_SHIFT, (uint8_t)BB_MRFSK) != RET_OK){
			return RET_ERROR;
		}

		// FSK Modulation Order: 2-FSK
		if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC0 + rf24_std_offset), FSKC0_MORD_MASK, FSKC0_MORD_SHIFT, (uint8_t)0x00) != RET_OK){
			return RET_ERROR;
		}

		// FSK Modulation Index based on information of Tables 68d and 134 of the IEEE 802.15.4-2012

		// FSK Modulation Index Scale: 1.0
		if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC0 + rf24_std_offset), FSKC0_MIDXS_MASK, FSKC0_MIDXS_SHIFT, (uint8_t)0x01) != RET_OK){
			return RET_ERROR;
		}

		if(at86rf215_data->config.ch0_center_freq >= 2400000){		// Frequency Band 2400-2483.5 MHz

			if(at86rf215_data->config.ch_spacing == 200000){			// Channel Spacing 0.2 MHz (Operating Mode #1)
				// FSK Modulation Index: 1.0
				if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC0 + rf24_std_offset), FSKC0_MIDX_MASK, FSKC0_MIDX_SHIFT, (uint8_t)0x03) != RET_OK){
					return RET_ERROR;
				}
			}

			else if(at86rf215_data->config.ch_spacing == 400000){	// Channel Spacing 0.4 MHz (Operating Mode #2 and #3)
				// FSK Modulation Index: 0.5
				if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC0 + rf24_std_offset), FSKC0_MIDX_MASK, FSKC0_MIDX_SHIFT, (uint8_t)0x01) != RET_OK){
					return RET_ERROR;
				}
			}

			else{
				return RET_ERROR;
			}
		}

		else{
			return RET_ERROR;	// TODO
		}

		break;

	case QFSK:
		// PHY Type: MR-FSK
//		if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_PC + rf24_std_offset), PC_PT_MASK, PC_PT_SHIFT, (uint8_t)BB_MRFSK) != RET_OK){
//			return RET_ERROR;
//		}

		// FSK Modulation Order: 4-FSK
//		if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_FSKC0 + rf24_std_offset), FSKC0_MORD_MASK, FSKC0_MORD_SHIFT, (uint8_t)0x01) != RET_OK){
//			return RET_ERROR;
//		}
		return RET_ERROR; // TODO
		break;

	case MROFDM:
		// PHY Type: MR-OFDM
//		if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_PC + rf24_std_offset), PC_PT_MASK, PC_PT_SHIFT, (uint8_t)BB_MROFDM) != RET_OK){
//			return RET_ERROR;
//		}
		return RET_ERROR; // TODO
		break;

	case MROQPSK:
		// PHY Type: MR-O-QPSK or legacy O-QPSK
//		if(at86rf215_write_subreg(at86rf215_data, (RG_BBC0_PC + rf24_std_offset), PC_PT_MASK, PC_PT_SHIFT, (uint8_t)BB_MROQPSK) != RET_OK){
//			return RET_ERROR;
//		}
		return RET_ERROR; // TODO
		break;

	default:
		return RET_ERROR;
		break;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param at86rf215_config
 * @return
 */
retval_t at86rf215_set_datarate(at86rf215_data_t* at86rf215_data){

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	switch(at86rf215_data->config.modulation){
	case PHYOFF:
		return RET_ERROR; // TODO
		break;

	case BFSK:
		if(at86rf215_set_mrfsk_datarate(at86rf215_data) != RET_OK){
			return RET_ERROR;
		}
		break;

	case QFSK:
		if(at86rf215_set_mrfsk_datarate(at86rf215_data) != RET_OK){
			return RET_ERROR;
		}
		break;

	case MROFDM:
		return RET_ERROR; // TODO
		break;

	case MROQPSK:
		return RET_ERROR; // TODO
		break;

	default:
		return RET_ERROR;
		break;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param modulation
 * @return
 */
retval_t at86rf215_default_phy_config(at86rf215_data_t* at86rf215_data){

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	switch(at86rf215_data->config.modulation){
	case PHYOFF:
		return RET_ERROR; // TODO
		break;

	case BFSK:
		if(at86rf215_default_mrfsk_phy_config(at86rf215_data) != RET_OK){
			return RET_ERROR;
		}
		break;

	case QFSK:
//		if(at86rf215_default_mrfsk_phy_config(at86rf215_data) != RET_OK){
//			return RET_ERROR;
//		}
		return RET_ERROR; // TODO
		break;

	case MROFDM:
		return RET_ERROR; // TODO
		break;

	case MROQPSK:
		return RET_ERROR; // TODO
		break;

	default:
		return RET_ERROR;
		break;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param at86rf215_config
 * @return
 */
retval_t at86rf215_set_output_power(at86rf215_data_t* at86rf215_data){

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	switch(at86rf215_data->config.modulation){
	case PHYOFF:
		return RET_ERROR; // TODO
		break;

	case BFSK:
		if(at86rf215_set_mrfsk_output_power(at86rf215_data) != RET_OK){
			return RET_ERROR;
		}
		break;

	case QFSK:
		if(at86rf215_set_mrfsk_output_power(at86rf215_data) != RET_OK){
			return RET_ERROR;
		}
		break;

	case MROFDM:
		return RET_ERROR; // TODO
		break;

	case MROQPSK:
		return RET_ERROR; // TODO
		break;

	default:
		return RET_ERROR;
		break;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param at86rf215_config
 * @return
 */
retval_t at86rf215_default_txfe_config(at86rf215_data_t* at86rf215_data){

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	switch(at86rf215_data->config.modulation){
	case PHYOFF:
		return RET_ERROR; // TODO
		break;

	case BFSK:
		if(at86rf215_default_mrfsk_txfe_config(at86rf215_data) != RET_OK){
			return RET_ERROR;
		}
		break;

	case QFSK:
		if(at86rf215_default_mrfsk_txfe_config(at86rf215_data) != RET_OK){
			return RET_ERROR;
		}
		break;

	case MROFDM:
		return RET_ERROR; // TODO
		break;

	case MROQPSK:
		return RET_ERROR; // TODO
		break;

	default:
		return RET_ERROR;
		break;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param at86rf215_config
 * @return
 */
retval_t at86rf215_default_rxfe_config(at86rf215_data_t* at86rf215_data){

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	switch(at86rf215_data->config.modulation){
	case PHYOFF:
		return RET_ERROR; // TODO
		break;

	case BFSK:
		if(at86rf215_default_mrfsk_rxfe_config(at86rf215_data) != RET_OK){
			return RET_ERROR;
		}
		break;

	case QFSK:
		if(at86rf215_default_mrfsk_rxfe_config(at86rf215_data) != RET_OK){
			return RET_ERROR;
		}
		return RET_ERROR;
		break;

	case MROFDM:
		return RET_ERROR; // TODO
		break;

	case MROQPSK:
		return RET_ERROR; // TODO
		break;

	default:
		return RET_ERROR;
		break;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param packet_length
 * @return
 */
retval_t at86rf215_set_packet_length(at86rf215_data_t* at86rf215_data, uint16_t packet_length){

	uint8_t fcs_length = 0;			// FCS length in octets
	uint16_t frame_length = 0;		// PSDU = packet + FCS
	uint16_t rf24_std_offset = 0x0000;

	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(packet_length < AT86RF215_MIN_FRAME_LENGTH){
		return RET_ERROR;
	}


	if(at86rf215_get_fcs_length(at86rf215_data, &fcs_length) != RET_OK){
		return RET_ERROR;
	}

	frame_length = packet_length + fcs_length;

	if(frame_length > AT86RF215_MAX_FRAME_LENGTH){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}


//	if(at86rf215_data->transmitting_packet || at86rf215_data->receiving_packet){
//		return RET_ERROR;
//	}

	if(at86rf215_data->receiving_packet){
		return RET_ERROR;
	}

	// TX Frame Length High Byte: This register contains the three most significant bits of the TX frame length.
	if(at86rf215_write_subreg(at86rf215_data,						\
							  (RG_BBC0_TXFLH + rf24_std_offset), 	\
							  TXFLH_TXFLH_MASK, 					\
							  TXFLH_TXFLH_SHIFT, 					\
							  (uint8_t)(frame_length >> 8) & TXFLH_TXFLH_MASK) != RET_OK){
		return RET_ERROR;
	}

	// TX Frame Length Low Byte: This register contains the eight least significant bits of the TX frame length.
	if(at86rf215_write_subreg(at86rf215_data,						\
							  (RG_BBC0_TXFLL + rf24_std_offset),	\
							  TXFLL_TXFLL_MASK,						\
							  TXFLL_TXFLL_SHIFT,					\
							  (uint8_t)(frame_length & TXFLL_TXFLL_MASK)) != RET_OK){
		return RET_ERROR;
	}

	return RET_OK;
}


/**
 *
 * @param at86rf215_data
 * @param rcv_packet_length
 * @return
 */
retval_t at86rf215_get_rcv_packet_length(at86rf215_data_t* at86rf215_data, uint16_t* rcv_packet_length){

	uint16_t rf24_std_offset = 0x0000;
	uint8_t high_byte;
	uint8_t low_byte;
	uint16_t frame_length = 0;		// PSDU = packet + FCS
	uint8_t fcs_length = 0;			// FCS length in octets


	if(at86rf215_data == NULL){
		return RET_ERROR;
	}

	if(at86rf215_data->band == RF24){
		rf24_std_offset = RF24_STD_OFFSET;
	}

	if(at86rf215_get_fcs_length(at86rf215_data, &fcs_length) != RET_OK){
		return RET_ERROR;
	}

	// RX Frame Length High Byte: This register contains the three most significant bits of the RX frame length.
	if(at86rf215_read_subreg(at86rf215_data,						\
							 (RG_BBC0_RXFLH + rf24_std_offset), 	\
							 RXFLH_RXFLH_MASK, 						\
							 RXFLH_RXFLH_SHIFT, 					\
							 &high_byte) != RET_OK){
		return RET_ERROR;
	}

	// RX Frame Length Low Byte: This register contains the eight least significant bits of the RX frame length.
	if(at86rf215_read_subreg(at86rf215_data,						\
							 (RG_BBC0_RXFLL + rf24_std_offset),		\
							 RXFLL_RXFLL_MASK,						\
							 RXFLL_RXFLL_SHIFT,						\
							 &low_byte) != RET_OK){
		return RET_ERROR;
	}

	frame_length = ((high_byte & RXFLH_RXFLH_MASK) << 8) | (low_byte & RXFLL_RXFLL_MASK);

	if((frame_length - fcs_length) < AT86RF215_MIN_FRAME_LENGTH){
		*rcv_packet_length = 0;
		return RET_ERROR;
	}

	*rcv_packet_length = frame_length - fcs_length;

	return RET_OK;
}
