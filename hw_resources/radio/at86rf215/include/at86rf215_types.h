/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * at86rf215_types.h
 *
 *  Created on: 20/07/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file at86rf215_types.h
 */

#ifndef YETIOS_HW_RESOURCES_RADIO_AT86RF215_INCLUDE_AT86RF215_TYPES_H_
#define YETIOS_HW_RESOURCES_RADIO_AT86RF215_INCLUDE_AT86RF215_TYPES_H_

#include "at86rf215_const.h"

/* AT86RF215 functional state. Used to enable or disable a specific option */
typedef enum at86rf215_functional_state_{
  FS_DISABLE = 0,
  FS_ENABLE = !FS_DISABLE
} at86rf215_functional_state_t;

/* AT86RF215 flag state */
typedef enum at86rf215_flag_state_{
  S_RESET = 0,
  S_SET = !S_RESET
} at86rf215_flag_state_t;

/* Enumeration for TRX identification */
typedef enum at86rf215_band_{
	RF09,	// ID for sub-1 GHz band
	RF24, 	// ID for 2.4 GHz band
	RFBOTH 	// ID for both radio bands
}at86rf215_band_t;

/* IRQ Pin Configuration struct */
typedef struct at86rf215_irq_pin_config_{
	uint8_t		irqmm:1;
	uint8_t		irqp:1;
	uint8_t		drv:2;
}at86rf215_irq_pin_config_t;

/* IRQ Events struct */
typedef struct at86rf215_irq_events_{

	at86rf215_flag_state_t	IRQ_RF09_WAKEUP:1;	// Wake-up / Reset Interrupt
	at86rf215_flag_state_t	IRQ_RF09_TRXRDY:1;	// Transceiver Ready Interrupt
	at86rf215_flag_state_t	IRQ_RF09_EDC:1;		// Energy Detection Completion Interrupt
	at86rf215_flag_state_t	IRQ_RF09_BATLOW:1;	// Battery Low Interrupt
	at86rf215_flag_state_t	IRQ_RF09_TRXERR:1;	// Transceiver Error Interrupt
	at86rf215_flag_state_t	IRQ_RF09_IQIFSF:1;	// I/Q IF Synchronization Failure Interrupt
	at86rf215_flag_state_t  :1;					// Reserved bit
	at86rf215_flag_state_t  :1;					// Reserved bit

	at86rf215_flag_state_t	IRQ_RF24_WAKEUP:1;	// Wake-up / Reset Interrupt
	at86rf215_flag_state_t	IRQ_RF24_TRXRDY:1;	// Transceiver Ready Interrupt
	at86rf215_flag_state_t	IRQ_RF24_EDC:1;		// Energy Detection Completion Interrupt
	at86rf215_flag_state_t	IRQ_RF24_BATLOW:1;	// Battery Low Interrupt
	at86rf215_flag_state_t	IRQ_RF24_TRXERR:1;	// Transceiver Error Interrupt
	at86rf215_flag_state_t	IRQ_RF24_IQIFSF:1;	// I/Q IF Synchronization Failure Interrupt
	at86rf215_flag_state_t  :1;					// Reserved bit
	at86rf215_flag_state_t  :1;					// Reserved bit

	at86rf215_flag_state_t	IRQ_BBC0_RXFS:1;	// Receiver Frame Start Interrupt
	at86rf215_flag_state_t	IRQ_BBC0_RXFE:1;	// Receiver Frame End Interrupt
	at86rf215_flag_state_t	IRQ_BBC0_RXAM:1;	// Receiver Address Match Interrupt
	at86rf215_flag_state_t	IRQ_BBC0_RXEM:1;	// Receiver Extended Match Interrupt
	at86rf215_flag_state_t	IRQ_BBC0_TXFE:1;	// Transmitter Frame End Interrupt
	at86rf215_flag_state_t	IRQ_BBC0_AGCH:1;	// AGC Hold Interrupt
	at86rf215_flag_state_t	IRQ_BBC0_AGCR:1;	// AGC Release Interrupt
	at86rf215_flag_state_t	IRQ_BBC0_FBLI:1;	// Frame Buffer Level Indication Interrupt

	at86rf215_flag_state_t	IRQ_BBC1_RXFS:1;	// Receiver Frame Start Interrupt
	at86rf215_flag_state_t	IRQ_BBC1_RXFE:1;	// Receiver Frame End Interrupt
	at86rf215_flag_state_t	IRQ_BBC1_RXAM:1;	// Receiver Address Match Interrupt
	at86rf215_flag_state_t	IRQ_BBC1_RXEM:1;	// Receiver Extended Match Interrupt
	at86rf215_flag_state_t	IRQ_BBC1_TXFE:1;	// Transmitter Frame End Interrupt
	at86rf215_flag_state_t	IRQ_BBC1_AGCH:1;	// AGC Hold Interrupt
	at86rf215_flag_state_t	IRQ_BBC1_AGCR:1;	// AGC Release Interrupt
	at86rf215_flag_state_t	IRQ_BBC1_FBLI:1;	// Frame Buffer Level Indication Interrupt

}at86rf215_irq_events_t;

/* Clock Output Pin Configuration struct */
typedef struct at86rf215_clko_pin_config_{
	uint8_t		drv:2;
	uint8_t		os:3;
}at86rf215_clko_pin_config_t;

/* Crystal Oscillator Configuration struct */
typedef struct at86rf215_xoc_config_{
	uint8_t		fs:1;
	uint8_t		trim:4;
}at86rf215_xoc_config_t;

/* Battery Monitor Control and Digital Voltage Regulator Control Configuration struct */
typedef struct at86rf215_bmdvc_config_{
	uint8_t		bmhr:1;
	uint8_t		bmvth:4;
}at86rf215_bmdvc_config_t;

/* Chip Modes */
typedef enum at86rf215_op_mode_{
	MODE_BBRF	= RF_MODE_BBRF,
	MODE_RF		= RF_MODE_RF,
	MODE_BBRF09 = RF_MODE_BBRF09,
	MODE_BBRF24 = RF_MODE_BBRF24
}at86rf215_op_mode_t;

/* Channel Modes */
typedef enum at86rf215_ch_mode_{
	MODE_IEEE	= 0x00,		// IEEE 802.15.4g-2012 compliant channel scheme
	MODE_FR1	= 0x01,		// Fine resolution: 389.5 - 510.0 MHz with 99.182 Hz channel stepping
	MODE_FR2 	= 0x02,		// Fine resolution: 779 - 1020 MHz with 198.364 Hz channel stepping
	MODE_FR3	= 0x03		// Fine resolution: 2400 - 2483.5 MHz with 396.728 Hz channel stepping
}at86rf215_ch_mode_t;

/* Frame Check Sequence Configuration struct */
typedef struct at86rf215_fcs_config_{
	uint8_t		fcsfe:1;	// Frame Check Sequence Filter Enable
	uint8_t		txafcs:1;	// Transmitter Auto Frame Check Sequence
	uint8_t		fcst:1;		// Frame Check Sequence Type
}at86rf215_fcs_config_t;

/* Frame Check Sequence Validation */
typedef enum at86rf215_fcs_status_{
  FCSKO 		= 0,		// FCS not valid
  FCSOK 		= !FCSKO	// FCS valid
}at86rf215_fcs_status_t;

/* PHY Types */
typedef enum at86rf215_modulation_{
	PHYOFF		= 0x00,		// OFF
	BFSK		= 0x01,		// 2-FSK
	QFSK		= 0x02,		// 4-FSK
	MROFDM		= 0x03,		// MR-OFDM
	MROQPSK		= 0x04		// MR-O-QPSK or legacy O-QPSK
}at86rf215_modulation_t;

/* Output Power */
// These values are extracted from Figures 11-8, 11-10 and 11-12 of the AT86RF215 Datasheet
// Values extracted from Figures were replaced by empirical ones taken in the 2.4 GHz band
typedef enum at86rf215_outputpwr_{
	MIN_PWR			= 0,	// Minimum measured power: -16 dBm
	NEG_10_DBM		= 6,	// Value from figures: 3
	NEG_5_DBM		= 12,	// Value from figures: 8
	ZERO_DBM		= 18,	// Value from figures: 13
	POS_5_DBM		= 24,	// Value from figures: 18
	POS_10_DBM		= 31,	// Value from figures: 24
	MAX_PWR			= 31	// Maximum measured power: +10 dBm
}at86rf215_outputpwr_t;

#endif /* YETIOS_HW_RESOURCES_RADIO_AT86RF215_INCLUDE_AT86RF215_TYPES_H_ */
