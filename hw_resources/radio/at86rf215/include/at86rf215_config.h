/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * at86rf215_config.h
 *
 *  Created on: 17/08/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file at86rf215_config.h
 */

#ifndef YETIOS_HW_RESOURCES_RADIO_AT86RF215_INCLUDE_AT86RF215_CONFIG_H_
#define YETIOS_HW_RESOURCES_RADIO_AT86RF215_INCLUDE_AT86RF215_CONFIG_H_


#define AT86RF215_FREQ_RESOLUTION 	25	// kHz

// IRQ Functions
retval_t at86rf215_config_irq_pin(at86rf215_data_t* at86rf215_data, at86rf215_irq_pin_config_t irq_pin_config);
retval_t at86rf215_config_radio_irq_events(at86rf215_data_t* at86rf215_data, rf_irq_t irq_type, at86rf215_functional_state_t new_state);
retval_t at86rf215_config_baseband_irq_events(at86rf215_data_t* at86rf215_data, bb_irq_t baseband_irq_type, at86rf215_functional_state_t new_state);
retval_t at86rf215_get_irq_events(at86rf215_data_t* at86rf215_data, at86rf215_irq_events_t* irq_events);

// Common Chip Register Functions
retval_t at86rf215_config_clko_pin(at86rf215_data_t* at86rf215_data, at86rf215_clko_pin_config_t clko_pin_config);
retval_t at86rf215_config_xoc(at86rf215_data_t* at86rf215_data, at86rf215_xoc_config_t xoc_config);
retval_t at86rf215_config_bmdvc(at86rf215_data_t* at86rf215_data, at86rf215_bmdvc_config_t bmdvc_config);

// Chip Operation Mode
retval_t at86rf215_set_op_mode(at86rf215_data_t* at86rf215_data, at86rf215_op_mode_t op_mode);

// Channel Configuration
retval_t at86rf215_set_ch_center_freq(at86rf215_data_t* at86rf215_data, uint16_t ch_center_freq);
retval_t at86rf215_set_ch_number_ieee_mode(at86rf215_data_t* at86rf215_data, uint16_t ch_number);
retval_t at86rf215_set_ch_number_fr_mode(at86rf215_data_t* at86rf215_data, uint32_t ch_number);
retval_t at86rf215_set_ch_spacing(at86rf215_data_t* at86rf215_data, uint8_t ch_spacing);
retval_t at86rf215_set_ch_mode(at86rf215_data_t* at86rf215_data, at86rf215_ch_mode_t ch_mode);
retval_t at86rf215_config_channel(at86rf215_data_t* at86rf215_data);

// Baseband Configuration
retval_t at86rf215_continuous_transmit(at86rf215_data_t* at86rf215_data, at86rf215_functional_state_t new_state);
retval_t at86rf215_config_frame_check_sequence(at86rf215_data_t* at86rf215_data, at86rf215_fcs_config_t fcs_config);
retval_t at86rf215_get_fcs_status(at86rf215_data_t* at86rf215_data, at86rf215_fcs_status_t* fcs_status);
retval_t at86rf215_get_fcs_length(at86rf215_data_t* at86rf215_data, uint8_t* fcs_length);
retval_t at86rf215_baseband_enable(at86rf215_data_t* at86rf215_data, at86rf215_functional_state_t new_state);
retval_t at86rf215_set_modulation(at86rf215_data_t* at86rf215_data);
retval_t at86rf215_set_datarate(at86rf215_data_t* at86rf215_data);
retval_t at86rf215_default_phy_config(at86rf215_data_t* at86rf215_data);

// Transmitter Front-End Configuration
retval_t at86rf215_set_output_power(at86rf215_data_t* at86rf215_data);
retval_t at86rf215_default_txfe_config(at86rf215_data_t* at86rf215_data);

// Receiver Front-End Configuration
retval_t at86rf215_default_rxfe_config(at86rf215_data_t* at86rf215_data);

// Packet Length Configuration
retval_t at86rf215_set_packet_length(at86rf215_data_t* at86rf215_data, uint16_t packet_length);
retval_t at86rf215_get_rcv_packet_length(at86rf215_data_t* at86rf215_data, uint16_t* rcv_packet_length);

#endif /* YETIOS_HW_RESOURCES_RADIO_AT86RF215_INCLUDE_AT86RF215_CONFIG_H_ */
