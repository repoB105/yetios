/*
 * Copyright (c) 2018, B105 Electronic Systems Lab,
 * Universidad Politecnica de Madrid. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * at86rf215_core.h
 *
 *  Created on: 11/07/2018
 *      Author: Ramiro Utrilla  <rutrilla@b105.upm.es>
 *
 */
/**
 * @file at86rf215_core.h
 */

#ifndef YETIOS_HW_RESOURCES_RADIO_AT86RF215_INCLUDE_AT86RF215_CORE_H_
#define YETIOS_HW_RESOURCES_RADIO_AT86RF215_INCLUDE_AT86RF215_CORE_H_

#include "at86rf215_types.h"

#define RET_PCKT_RCV		0x01
#define RET_PCKT_SENT		0x02
#define RET_TRX_ERROR		0x04

#define AT86RF215_PN		0x34
#define AT86RF215_VN		0x03

#define AT86RF215_MAX_FRAME_LENGTH	MAX_FRAME_BUFFER_SIZE	// Octets
#define AT86RF215_MIN_FRAME_LENGTH	1						// Octets

#define AT86RF215_INVALID_RSSI	 127
#define AT86RF215_MIN_RSSI		-127
#define AT86RF215_MAX_RSSI		 4

#define AT86RF215_INVALID_EDV	 127
#define AT86RF215_MIN_EDV		-127
#define AT86RF215_MAX_EDV		 4


/* AT86RF215 Configuration struct */
typedef struct at86rf215_config_{
	// Channel Configuration
	at86rf215_ch_mode_t		ch_mode;			// Channel Mode/Scheme
	uint32_t				ch0_center_freq;	// Base Modulation freq. in kHz
	uint32_t				ch_spacing;			// Channel spacing in Hz
	uint32_t				ch_number;			// Channel number

	// PHY Configuration
	at86rf215_modulation_t	modulation;			// PHY Type
	uint32_t				baud_rate;			// Data Rate transmitted over the air in bps
	at86rf215_outputpwr_t	output_pwr;			// Output Power in dBm

	// The receiver bandwidth is automatically configured according to the recommendations
	// of the AT86RF215 datasheet. Its value will depend on the selected modulation and baud rate.
	//uint32_t	 			rx_bandwidth;		// Receiver Bandwidth in KHz

}at86rf215_config_t;

typedef struct __packed at86rf215_data_{
	uint32_t spi_id;
	gpio_pin_t cs_pin;
	gpio_pin_t irq_pin;
	at86rf215_band_t band;
	at86rf215_config_t config;
	ytProcessFunc_t interrupt_cb_func;
	void* interrupt_cb_arg;
	uint32_t at86rf215_mutex_id;
	uint32_t at86rf215_rx_time;
	uint32_t at86rf215_tx_time;
	uint8_t transmitting_packet;
	uint8_t receiving_packet;
}at86rf215_data_t;


at86rf215_data_t* new_at86rf215_data(gpio_pin_t cs_pin, gpio_pin_t irq_pin, at86rf215_band_t band, ytProcessFunc_t interrupt_cb_func, void* args);
retval_t delete_at86rf215_data(at86rf215_data_t* at86rf215_data);

retval_t at86rf215_hw_init(at86rf215_data_t* at86rf215_data, at86rf215_config_t* at86rf215_init_config, char* spi_dev);
retval_t at86rf215_hw_deInit(at86rf215_data_t* at86rf215_data);
retval_t at86rf215_hw_config(at86rf215_data_t* at86rf215_data);

retval_t at86rf215_send_data(at86rf215_data_t* at86rf215_data, uint8_t* data, uint16_t size);
retval_t at86rf215_set_mode_rx(at86rf215_data_t* at86rf215_data);

retval_t at86rf215_power_off(at86rf215_data_t* at86rf215_data);
retval_t at86rf215_set_mode_sleep(at86rf215_data_t* at86rf215_data);

retval_t at86rf215_power_on(at86rf215_data_t* at86rf215_data);
retval_t at86rf215_set_mode_idle(at86rf215_data_t* at86rf215_data);

int8_t at86rf215_get_last_rssi(at86rf215_data_t* at86rf215_data);
int8_t at86rf215_get_last_edv(at86rf215_data_t* at86rf215_data);

// This routine is called when an interruption of the AT86RF215 is generated.
// ATTENTION! This routine is not called directly from the interruption.
// This routine is executed in another thread, which is controlled by a semaphore
// that is released from the interruption callback. All these actions are
// controlled by the PHY layer of the communication stack.
uint16_t at86rf215_irq_routine(at86rf215_data_t* at86rf215_data);

retval_t at86rf215_read_num_rcv_bytes(at86rf215_data_t* at86rf215_data, uint8_t* num_rcv_bytes);
retval_t at86rf215_read_rcv_data(at86rf215_data_t* at86rf215_data, uint8_t* packet_data, uint16_t size);
retval_t at86rf215_flush_last_rcv_data(at86rf215_data_t* at86rf215_data);

#endif /* YETIOS_HW_RESOURCES_RADIO_AT86RF215_INCLUDE_AT86RF215_CORE_H_ */
