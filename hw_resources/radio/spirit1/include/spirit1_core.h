/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * spirit1_core.h
 *
 *  Created on: 4 de dic. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file spirit1_core.h
 */
#ifndef APPLICATION_BOARD_PLATFORM_RADIO_SPIRIT1_INCLUDE_SPIRIT1_CORE_H_
#define APPLICATION_BOARD_PLATFORM_RADIO_SPIRIT1_INCLUDE_SPIRIT1_CORE_H_

#include "platform-cpu.h"
#include "system_api.h"
#include "arm_math.h"

#define DOUBLE_XTAL_THR                         30000000
#define SPIRIT1_FUNCS_TIMEOUT					100

#define RET_PCKT_RCV	0x01
#define RET_PCKT_SENT	0x02

/**
 * @brief  SPIRIT Modulation enumeration
 */
typedef enum
{
  FSK         = 0x00, /*!< 2-FSK modulation selected */
  GFSK_BT05   = 0x50, /*!< GFSK modulation selected with BT=0.5 */
  GFSK_BT1    = 0x10, /*!< GFSK modulation selected with BT=1 */
  ASK_OOK     = 0x20, /*!< ASK or OOK modulation selected. ASK will use power ramping */
  MSK         = 0x30  /*!< MSK modulation selected */

}ModulationSelect;

typedef ModulationSelect modulation_t;

typedef struct spirit1_config_{
	uint32_t	baud_rate;			//In bps
	uint32_t 	channel_spacing;	//In Khz
	uint32_t	freq_deviation;		//In KHz
	uint32_t 	rx_bandwidth;		//In Khz
	uint32_t	modulation_freq;	//Freq in Hz
	uint32_t 	xtal_freq;			//Xtal Freq in Hz
	int16_t 	output_power;		//In dBm. Positive or negative
	modulation_t modulation;
	uint16_t 	channel_num;
}spirit1_config_t;

typedef struct __packed spirit1_data_{
	gpio_pin_t cs_pin;
	gpio_pin_t sdn_pin;
	gpio_pin_t gpio3_int_pin;
	uint32_t spi_id;
	uint32_t spirit1_rx_time;
	uint32_t spirit1_tx_time;
	uint32_t spirit1_mutex_id;
	uint32_t spirit1_aes_semph_id;
	ytProcessFunc_t interrupt_cb_func;
	void* interrupt_cb_arg;
	uint32_t xtal_freq;
	uint8_t transmitting_packet;
	uint8_t receiving_packet;
	uint8_t checking_rssi;
}spirit1_data_t;

spirit1_data_t* new_spirit1_data(gpio_pin_t cs_pin, gpio_pin_t sdn_pin, gpio_pin_t gpio3_int_pin, ytProcessFunc_t interrupt_cb_func, void* args);
retval_t delete_spirit1_data(spirit1_data_t* spirit1_data);

retval_t spirit1_hw_init(spirit1_data_t* spirit1_data, spirit1_config_t* spirit1_init_config, char* spi_dev);
retval_t spirit1_hw_deinit(spirit1_data_t* spirit1_data);

retval_t spirit1_set_baud_rate(spirit1_data_t* spirit1_data, uint32_t baud_rate);
retval_t spirit1_set_output_power(spirit1_data_t* spirit1_data, int16_t output_power);
retval_t spirit1_set_channel_num(spirit1_data_t* spirit1_data, uint16_t channel_num);
retval_t spirit1_set_base_freq(spirit1_data_t* spirit1_data, uint32_t base_freq);
retval_t spirit1_set_packet_size(spirit1_data_t* spirit1_data, uint16_t packet_size);

retval_t spirit1_send_data(spirit1_data_t* spirit1_data, uint8_t* data, uint16_t size);

retval_t spirit1_set_mode_rx(spirit1_data_t* spirit1_data);
retval_t spirit1_set_mode_sleep(spirit1_data_t* spirit1_data);
retval_t spirit1_set_mode_idle(spirit1_data_t* spirit1_data);

retval_t spirit1_power_off(spirit1_data_t* spirit1_data);
retval_t spirit1_power_on(spirit1_data_t* spirit1_data);

retval_t spirit1_check_device_info_reg(spirit1_data_t* spirit1_data);

float32_t spirit1_check_channel_rssi(spirit1_data_t* spirit1_data);
float32_t spirit1_get_last_rssi(spirit1_data_t* spirit1_data);

retval_t spirit1_aes_encrypt_data(spirit1_data_t* spirit1_data, uint8_t* data, uint8_t* key, uint16_t size);
retval_t spirit1_aes_decrypt_data(spirit1_data_t* spirit1_data, uint8_t* data, uint8_t* key, uint16_t size);

//Esta rutina es llamada al saltar una interrupcion del spirit. OJO! No es llamada por la interrupcion sino por una hebra. Es decir por la capa phy
//Si se da el caso de que la interrupcion recibe un paquete, este packete se guarda en la variable pasada por parametro
//Devuelve distinto de 0 si ha recibido un packete
uint16_t spirit1_irq_routine(spirit1_data_t* spirit1_data);

retval_t spirit1_read_num_rcv_bytes(spirit1_data_t* spirit1_data, uint8_t* num_rcv_bytes);
retval_t spirit1_read_rcv_data(spirit1_data_t* spirit1_data, uint8_t* packet_data, uint16_t size);
retval_t spirit1_flush_last_rcv_data(spirit1_data_t* spirit1_data, uint16_t size);

#include "spirit1_arch.h"

#endif /* APPLICATION_BOARD_PLATFORM_RADIO_SPIRIT1_INCLUDE_SPIRIT1_CORE_H_ */
