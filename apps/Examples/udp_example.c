/*
 * Copyright (c) 2018, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * udp_example.c
 *
 *  Created on: 7 de may. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file udp_example.c
 */

#include "system_api.h"
#include "system_net_api.h"
#include "time_meas.h"

#define USE_UDP_EXAMPLE_PROCESS	0

#if USE_UDP_EXAMPLE_PROCESS

#define NODE_1	1
//#define NODE_2	2

#define UDP_PORT			444

static uint16_t testProcId;

static void set_addr(void);
/* TEST PROCESS ************************************/
YT_PROCESS void udp_test_func(void const * argument);


ytInitProcess(udp_test_proc, udp_test_func, NORMAL_PRIORITY_PROCESS, 280, &testProcId, NULL);

#define BUF_SIZE	32
char tx_data[BUF_SIZE];
char rx_data[BUF_SIZE];

char addr_str_buf[8];

YT_PROCESS void udp_test_func(void const * argument){
	ytNetAddr_t net_addr;
	uint32_t pckt_id = 0;

	ytDelay(500);
	set_addr();

	ytGpioInitPin(GPIO_PIN_B4, GPIO_PIN_OUTPUT_PUSH_PULL, GPIO_PIN_NO_PULL);	//Init pin for Spirit1 led
	while(1){

#if NODE_1
		ytUrOpen(UDP_PORT);	//Open the port

		sprintf(tx_data, "TEST_PACKET ID: %d\r\n", (int) pckt_id);

		net_addr = ytNewNetAddr("2", 'D');	//Send to node 2
		if(ytUrSend(UDP_PORT, net_addr,(uint8_t*) tx_data, strlen(tx_data) + 1) == (strlen(tx_data)+1)){
			ytPrintf("SEND OK\r\n");
		}
		ytUrClose(UDP_PORT);
		ytDeleteNetAddr(net_addr);

		pckt_id++;
		ytDelay(200);
		ytGpioPinSet(GPIO_PIN_B4);
		ytDelay(50);
		ytGpioPinReset(GPIO_PIN_B4);
#endif

#if NODE_2
		ytUrOpen(UDP_PORT);
		net_addr = ytNewNetAddr("22222", 'D');
		ytUrRcv(UDP_PORT, net_addr,(uint8_t*) rx_data, BUF_SIZE);
		ytNetAddrToString(net_addr, addr_str_buf, 'D');
		float32_t lvl =  ytUrGetSignalLevel(UDP_PORT);
		ytPrintf("RCV: %s from %s  LVL: %.2fdB\r\n",(uint8_t*) rx_data, addr_str_buf, lvl);

		ytUrClose(UDP_PORT);
		ytDeleteNetAddr(net_addr);
		ytGpioPinToggle(GPIO_PIN_B4);
#endif

	}
}
/* *************************************************/


static void set_addr(void){
	ytNetAddr_t net_addr;
#if NODE_1
	/* Set node Addr */
	net_addr = ytNewNetAddr("1", 'D');		//Node adress
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);
#endif

#if NODE_2
	/* Set node Addr */
	net_addr = ytNewNetAddr("2", 'D');		//Node adress
	ytNetAddNodeAddr(net_addr);
	ytDeleteNetAddr(net_addr);

	ytSetNodeAsGw();		//Define This node as the GW
#endif
}

#endif
