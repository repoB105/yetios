/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */     
#include "leds.h"
#include "stm32l4xx_hal.h"
#include "lptim.h"
#include "platform-conf.h"
#include "low_power.h"
#include "system_api.h"
/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
osThreadId mainWatchTaskHandle;

/* USER CODE BEGIN Variables */
#define LPTIM_CLOCK_HZ					32768
#define TICKS_PER_SYSTICK_LOW_POWER		(LPTIM_CLOCK_HZ/configTICK_RATE_HZ)
#define MAX_LOW_POWER_COUNT 			0xFFF0
#define portNVIC_SYSTICK_CTRL_REG			( * ( ( volatile uint32_t * ) 0xe000e010 ) )
#define portNVIC_SYSTICK_LOAD_REG			( * ( ( volatile uint32_t * ) 0xe000e014 ) )
#define portNVIC_SYSTICK_CURRENT_VALUE_REG	( * ( ( volatile uint32_t * ) 0xe000e018 ) )
#define portNVIC_SYSTICK_ENABLE_BIT			( 1UL << 0UL )

#if ENABLE_RUNTIME_STATS
TIM_HandleTypeDef        htim2;
#endif

#if USE_STOP_MODE
extern __IO uint32_t uwTick;

uint32_t using_periph = 0;
uint32_t low_power_entered = 0;
#endif
/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void mainProcess(void const * argument);

extern void MX_USB_DEVICE_Init(void);
extern void MX_FATFS_Init(void);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* USER CODE BEGIN FunctionPrototypes */
extern void main_process(void);
/* USER CODE END FunctionPrototypes */

/* Pre/Post sleep processing prototypes */
void PreSleepProcessing(uint32_t *ulExpectedIdleTime);
void PostSleepProcessing(uint32_t *ulExpectedIdleTime);

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);
void vApplicationMallocFailedHook(void);

/* USER CODE BEGIN 1 */

/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{
#if ENABLE_RUNTIME_STATS
	  /* Enable TIM2 clock */
	  __HAL_RCC_TIM2_CLK_ENABLE();

	/* Initialize TIM2 */
	  htim2.Instance = TIM2;

	  /*
	   * Configure the 40Khz timer
	  */
	  htim2.Init.Period = 0xFFFFFFFF;	//MAX count available in 32 bits timer
	  htim2.Init.Prescaler = ((configCPU_CLOCK_HZ/1000000)*25) - 1;		//40KHz counting
	  htim2.Init.ClockDivision = 0;
	  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
	  if(HAL_TIM_Base_Init(&htim2) == HAL_OK)
	  {
	    /* Start the TIM time Base generation*/
	    HAL_TIM_Base_Start(&htim2);
	  }
#endif
}

__weak unsigned long getRunTimeCounterValue(void)
{
#if ENABLE_RUNTIME_STATS
	return __HAL_TIM_GET_COUNTER(&htim2);
#else
	return 0;
#endif
}

/* USER CODE END 1 */

/* USER CODE BEGIN 4 */
__weak void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
   /* Run time stack overflow checking is performed if
   configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
   called if a stack overflow is detected. */
#ifdef ERROR_LED
	ytLedsOn(ERROR_LED);
#endif
	while(1);
}
/* USER CODE END 4 */

/* USER CODE BEGIN 5 */
__weak void vApplicationMallocFailedHook(void)
{
   /* vApplicationMallocFailedHook() will only be called if
   configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h. It is a hook
   function that will get called if a call to pvPortMalloc() fails.
   pvPortMalloc() is called internally by the kernel whenever a task, queue,
   timer or semaphore is created. It is also called by various parts of the
   demo application. If heap_1.c or heap_2.c are used, then the size of the
   heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
   FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
   to query the size of free heap space that remains (although it does not
   provide information on how the remaining heap might be fragmented). */
#ifdef ERROR_LED
	ytLedsOn(ERROR_LED);
#endif
	while(1);
}
/* USER CODE END 5 */

/* USER CODE BEGIN PREPOSTSLEEP */
__weak void PreSleepProcessing(uint32_t *ulExpectedIdleTime)
{
	/* place for user code */
}

__weak void PostSleepProcessing(uint32_t *ulExpectedIdleTime)
{
/* place for user code */
}
/* USER CODE END PREPOSTSLEEP */

/* Init FreeRTOS */

void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
       
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of mainWatchTask */
  osThreadDef(mainWatchTask, mainProcess, osPriorityLow, 0, 320);
  mainWatchTaskHandle = osThreadCreate(osThread(mainWatchTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
}

/* mainProcess function */
void mainProcess(void const * argument)
{

  /* USER CODE BEGIN mainProcess */
#if !ENABLE_STOP_MODE_DEBUG
  HAL_DBGMCU_DisableDBGStopMode();
#endif
  HAL_RCCEx_WakeUpStopCLKConfig(RCC_STOP_WAKEUPCLOCK_MSI);		//SET MSI clock when returning from sleep

  main_process();
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END mainProcess */
}

/* USER CODE BEGIN Application */

void disable_low_power_mode(void){
#if USE_STOP_MODE
	while(low_power_entered);
	using_periph ++;
#endif
}

void enable_low_power_mode(void){
#if USE_STOP_MODE
	using_periph--;
#endif
}

#if USE_STOP_MODE

void vPortSuppressTicksAndSleep( TickType_t xExpectedIdleTime ){

	if(!using_periph){
		low_power_entered++;
		HAL_SuspendTick();

		uint32_t lowpower_ticks =((xExpectedIdleTime*LPTIM_CLOCK_HZ)/configTICK_RATE_HZ)-5;		//-5: Empirical measured value

		uint32_t elapsed_systick_count = ( configCPU_CLOCK_HZ / configTICK_RATE_HZ ) - portNVIC_SYSTICK_CURRENT_VALUE_REG;
		uint32_t elapsed_lowpowerticks = (elapsed_systick_count*LPTIM_CLOCK_HZ)/configCPU_CLOCK_HZ + 1;

		lowpower_ticks -= elapsed_lowpowerticks;

		if(lowpower_ticks >= MAX_LOW_POWER_COUNT){
			lowpower_ticks = MAX_LOW_POWER_COUNT;
		}

		__HAL_LPTIM_CLEAR_FLAG(&hlptim1, LPTIM_FLAG_CMPM);
		HAL_LPTIM_TimeOut_Start_IT(&hlptim1, MAX_LOW_POWER_COUNT, lowpower_ticks);

		portNVIC_SYSTICK_CTRL_REG &= ~portNVIC_SYSTICK_ENABLE_BIT;


	//	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET);


			HAL_PWREx_EnterSTOP1Mode(PWR_STOPENTRY_WFI);


	//	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);
#if ((STDIO_INTERFACE == UART_STDIO) || (ENABLE_UART_DRIVER))		//It is necessary to restart the HSI to receive in the UART after sleep mode
		  RCC_OscInitTypeDef RCC_OscInitStruct;
		  RCC_ClkInitTypeDef RCC_ClkInitStruct;
			/**Initializes the CPU, AHB and APB busses clocks
			*/
		  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
		  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
		  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
		  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
		  {
			Error_Handler();
		  }
			/**Initializes the CPU, AHB and APB busses clocks
			*/
		  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
									  |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
		  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
		  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
		  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
		  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
		  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
		  {
			Error_Handler();
		  }
#endif

		uint32_t count = HAL_LPTIM_ReadCounter(&hlptim1);
		HAL_LPTIM_TimeOut_Stop_IT(&hlptim1);
		__HAL_LPTIM_CLEAR_FLAG(&hlptim1, LPTIM_FLAG_CMPM);


		//Microticks elapsed
		uint64_t lapsed_uticks = (uint64_t)((uint64_t)(count+4+elapsed_lowpowerticks)*(uint64_t)configTICK_RATE_HZ*1000)/ LPTIM_CLOCK_HZ;			//+4: Valor empirico medido para que los valores se ajusten a la realidad
		uint32_t lapsed_ticks = (uint32_t)lapsed_uticks/1000;	//Comleted ticks
		uint32_t remaining_uticks = 1000 - ((uint32_t)lapsed_uticks - (lapsed_ticks*1000));//Decimal part of remainig ticks

		if(remaining_uticks < 1000){
			portNVIC_SYSTICK_LOAD_REG = ((configCPU_CLOCK_HZ/1000000)*remaining_uticks) - 1UL;		//New reload value
			vTaskStepTick(lapsed_ticks);
	#if HAL_SYNC_WITH_RTOS_SYSTICK
			uwTick += lapsed_ticks+1;			//Update HAL time in ms
	#endif
		}
		else{										//1000 uticks remaining means the tick cycle is complete
			portNVIC_SYSTICK_LOAD_REG = 1UL;		//Systick interrupt is launched immediately
			vTaskStepTick(lapsed_ticks-1);
	#if HAL_SYNC_WITH_RTOS_SYSTICK
			uwTick += lapsed_ticks;  			//Update HAL time in ms
	#endif
		}

		portNVIC_SYSTICK_CURRENT_VALUE_REG = 0UL;
		portENTER_CRITICAL();
		{
			portNVIC_SYSTICK_CTRL_REG |= portNVIC_SYSTICK_ENABLE_BIT;	//Activate timer with the reload value configurated previously
			portNVIC_SYSTICK_LOAD_REG = ( configCPU_CLOCK_HZ / configTICK_RATE_HZ ) - 1UL;	//Default reload value
		}
		portEXIT_CRITICAL();


		HAL_ResumeTick();
		low_power_entered--;
	}
}
#endif
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
