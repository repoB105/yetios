/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define B1_EXTI_IRQn EXTI15_10_IRQn
#define GPIO_Free_PC0_Pin GPIO_PIN_0
#define GPIO_Free_PC0_GPIO_Port GPIOC
#define GPIO_Free_PC1_Pin GPIO_PIN_1
#define GPIO_Free_PC1_GPIO_Port GPIOC
#define GPIO_Free_PC2_Pin GPIO_PIN_2
#define GPIO_Free_PC2_GPIO_Port GPIOC
#define GPIO_Free_PC3_Pin GPIO_PIN_3
#define GPIO_Free_PC3_GPIO_Port GPIOC
#define GPIO_Free_PA0_Pin GPIO_PIN_0
#define GPIO_Free_PA0_GPIO_Port GPIOA
#define GPIO_Free_PA1_Pin GPIO_PIN_1
#define GPIO_Free_PA1_GPIO_Port GPIOA
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define LD2_Pin GPIO_PIN_5
#define LD2_GPIO_Port GPIOA
#define GPIO_Free_PC4_Pin GPIO_PIN_4
#define GPIO_Free_PC4_GPIO_Port GPIOC
#define GPIO_Free_PC5_Pin GPIO_PIN_5
#define GPIO_Free_PC5_GPIO_Port GPIOC
#define LSM303AGR_INT_Pin GPIO_PIN_0
#define LSM303AGR_INT_GPIO_Port GPIOB
#define LSM303AGR_INT_EXTI_IRQn EXTI0_IRQn
#define GPIO_Free_PB1_Pin GPIO_PIN_1
#define GPIO_Free_PB1_GPIO_Port GPIOB
#define GPIO_Free_PB2_Pin GPIO_PIN_2
#define GPIO_Free_PB2_GPIO_Port GPIOB
#define LPS22H_INT1_Pin GPIO_PIN_10
#define LPS22H_INT1_GPIO_Port GPIOB
#define LPS22H_INT1_EXTI_IRQn EXTI15_10_IRQn
#define GPIO_Free_PB11_Pin GPIO_PIN_11
#define GPIO_Free_PB11_GPIO_Port GPIOB
#define GPIO_Free_PB12_Pin GPIO_PIN_12
#define GPIO_Free_PB12_GPIO_Port GPIOB
#define GPIO_Free_PB13_Pin GPIO_PIN_13
#define GPIO_Free_PB13_GPIO_Port GPIOB
#define GPIO_Free_PB14_Pin GPIO_PIN_14
#define GPIO_Free_PB14_GPIO_Port GPIOB
#define GPIO_Free_PB15_Pin GPIO_PIN_15
#define GPIO_Free_PB15_GPIO_Port GPIOB
#define GPIO_Free_PC6_Pin GPIO_PIN_6
#define GPIO_Free_PC6_GPIO_Port GPIOC
#define SPIRIT1_GPIO3_Pin GPIO_PIN_7
#define SPIRIT1_GPIO3_GPIO_Port GPIOC
#define SPIRIT1_GPIO3_EXTI_IRQn EXTI9_5_IRQn
#define GPIO_Free_PC8_Pin GPIO_PIN_8
#define GPIO_Free_PC8_GPIO_Port GPIOC
#define GPIO_Free_PC9_Pin GPIO_PIN_9
#define GPIO_Free_PC9_GPIO_Port GPIOC
#define GPIO_Free_PA8_Pin GPIO_PIN_8
#define GPIO_Free_PA8_GPIO_Port GPIOA
#define GPIO_Free_PA9_Pin GPIO_PIN_9
#define GPIO_Free_PA9_GPIO_Port GPIOA
#define GPIO_Free_PA9_EXTI_IRQn EXTI9_5_IRQn
#define SPIRIT1_SDN_Pin GPIO_PIN_10
#define SPIRIT1_SDN_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define GPIO_Free_PA15_Pin GPIO_PIN_15
#define GPIO_Free_PA15_GPIO_Port GPIOA
#define GPIO_Free_PC10_Pin GPIO_PIN_10
#define GPIO_Free_PC10_GPIO_Port GPIOC
#define GPIO_Free_PC11_Pin GPIO_PIN_11
#define GPIO_Free_PC11_GPIO_Port GPIOC
#define GPIO_Free_PC12_Pin GPIO_PIN_12
#define GPIO_Free_PC12_GPIO_Port GPIOC
#define GPIO_Free_PD2_Pin GPIO_PIN_2
#define GPIO_Free_PD2_GPIO_Port GPIOD
#define LSM6DSL_INT2_Pin GPIO_PIN_4
#define LSM6DSL_INT2_GPIO_Port GPIOB
#define LSM6DSL_INT2_EXTI_IRQn EXTI4_IRQn
#define LSM6DSL_INT1_Pin GPIO_PIN_5
#define LSM6DSL_INT1_GPIO_Port GPIOB
#define LSM6DSL_INT1_EXTI_IRQn EXTI9_5_IRQn
#define SPIRIT1_CS_Pin GPIO_PIN_6
#define SPIRIT1_CS_GPIO_Port GPIOB
#define GPIO_Free_PB7_Pin GPIO_PIN_7
#define GPIO_Free_PB7_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
