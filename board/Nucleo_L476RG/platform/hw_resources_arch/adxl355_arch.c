/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * adxl355_arch.c
 *
 *  Created on: 8 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file adxl355_arch.c
 */

#include <adxl355_lib.h>
#include "platform-conf.h"

#if ENABLE_SPI_DRIVER
#include "spi_driver.h"
#endif
#define ADXL355_SPI_SPEED	6000000

#define READ_MASK	0x01
#define WRITE_MASK	0xFE

#if ENABLE_NETSTACK_ARCH
/**
 *
 * @param adxl355_data
 * @param reg
 * @param data
 * @return
 */
retval_t ADXL355_ReadReg(adxl355_data_t* adxl355_data, uint8_t reg, uint8_t* data) {
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	spi_read_write_buffs_t read_write_buffs;
	uint8_t spi_bus_data_tx[2];
	uint8_t spi_bus_data_rx[2];

	if(adxl355_data == NULL){
		return RET_ERROR;
	}

	read_write_buffs.prx = &spi_bus_data_rx[0];
	read_write_buffs.ptx = &spi_bus_data_tx[0];
	read_write_buffs.size = 2;
	spi_bus_data_tx[0] = ((reg<<1) | READ_MASK);	//Read


	if(ytIoctl(adxl355_data->spi_fd, SPI_READ_WRITE_OP, &read_write_buffs) != RET_OK){
		return RET_ERROR;
	}

	(*data) = spi_bus_data_rx[1];

  return RET_OK;
#endif
}


/**
 *
 * @param adxl355_data
 * @param reg
 * @param data
 * @return
 */
retval_t ADXL355_WriteReg(adxl355_data_t* adxl355_data, uint8_t reg, uint8_t data){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	uint8_t spi_bus_data[2];

	if(adxl355_data == NULL){
		return RET_ERROR;
	}

	spi_bus_data[0] = ((reg<<1) & WRITE_MASK);	//Write
	spi_bus_data[1] = data;

	if( ytWrite(adxl355_data->spi_fd, &spi_bus_data[0], 2) != 2){
		return RET_ERROR;
	}

  return RET_OK;
#endif
}


retval_t ADXL355_GetAccAxesRaw_FIFO_Mode(adxl355_data_t* adxl355_data, AxesRaw_adxl_t* buff, uint16_t size) {
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	spi_read_write_buffs_t read_write_buffs;
	uint16_t i;
	int32_t value;
	uint8_t flags;
	uint8_t *valueL = (uint8_t *)(&value);
	uint8_t *valueM = ((uint8_t *)(&value)+1);
	uint8_t *valueH = ((uint8_t *)(&value)+2);
	if(adxl355_data == NULL){
		return RET_ERROR;
	}

	uint8_t* tx_values = (uint8_t*) ytMalloc((2*size*9*sizeof(uint8_t))+2);	//Reservo el doble de espacio y asigno el puntero a read_values
	uint8_t* read_values = tx_values + (9*size) + 1;

	read_write_buffs.prx = read_values;
	read_write_buffs.ptx = tx_values;
	read_write_buffs.size = (size*9)+1;

	tx_values[0] = ((ADXL355_FIFO_DATA<<1) | READ_MASK);	//Read

	if(ytIoctl(adxl355_data->spi_fd, SPI_READ_WRITE_OP, &read_write_buffs) != RET_OK){
		ytFree(tx_values);
		return RET_ERROR;
	}

	for(i=0; i<size; i++){

		*valueL = read_values[(i*9)+3];
		*valueM = read_values[(i*9)+2];
		*valueH = read_values[(i*9)+1];
		flags = (read_values[(i*9)+3] & 0x03);
		if(flags != 0x01){
			ytFree(tx_values);
			return RET_ERROR;
		}
		buff[i].AXIS_X = (value<<8);

		*valueL = read_values[(i*9)+6];
		*valueM = read_values[(i*9)+5];
		*valueH = read_values[(i*9)+4];
		flags = (read_values[(i*9)+6] & 0x03);
		if(flags != 0x00){
			ytFree(tx_values);
			return RET_ERROR;
		}
		buff[i].AXIS_Y = (value<<8);

		*valueL = read_values[(i*9)+9];
		*valueM = read_values[(i*9)+8];
		*valueH = read_values[(i*9)+7];
		flags = (read_values[(i*9)+9] & 0x03);
		if(flags != 0x00){
			ytFree(tx_values);
			return RET_ERROR;
		}
		buff[i].AXIS_Z = (value<<8);

	}
	ytFree(tx_values);

	return RET_OK;
#endif
}

/**
 *
 * @param adxl355_data
 * @param adxl355_csPin
 * @return
 */
retval_t ADXL355_config_spi_device(adxl355_data_t* adxl355_data, gpioPin_t adxl355_csPin, char* spi_dev){
#if !ENABLE_SPI_DRIVER
	return RET_ERROR;
#else
	spi_driver_ioctl_cmd_t spi_ioctl_cmd;
	if(adxl355_data == NULL){
		return RET_ERROR;
	}

	adxl355_data->spi_fd = ytOpen(spi_dev, 0);
	if(!adxl355_data->spi_fd){
		return RET_ERROR;
	}
	spi_ioctl_cmd = ENABLE_SW_CS;
	if(ytIoctl(adxl355_data->spi_fd, spi_ioctl_cmd, (void*) adxl355_csPin) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = DISABLE_SW_CS_EACH_BYTE;
	if(ytIoctl(adxl355_data->spi_fd, spi_ioctl_cmd, NULL) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = SET_POLARITY_LOW;
	if(ytIoctl(adxl355_data->spi_fd, spi_ioctl_cmd, NULL) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = SET_EDGE_1;
	if(ytIoctl(adxl355_data->spi_fd, spi_ioctl_cmd, NULL) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = SET_SPI_MODE_MASTER;
	if(ytIoctl(adxl355_data->spi_fd, spi_ioctl_cmd, NULL) != RET_OK){
		return RET_ERROR;
	}
	spi_ioctl_cmd = SET_SPI_SPEED;
	if(ytIoctl(adxl355_data->spi_fd, spi_ioctl_cmd, (void*) ADXL355_SPI_SPEED) != RET_OK){
		return RET_ERROR;
	}
	return RET_OK;
#endif
}
#endif
