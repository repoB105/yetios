/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * gpio_arch.c
 *
 *  Created on: 20 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file gpio_arch.c
 */

#include "sys_gpio.h"

#define MAX_EXTI_LINES	16

extern void sysGpioInterruptCallback(gpio_pin_t gpio_pin);

static gpio_pin_t exti_lines[MAX_EXTI_LINES];

static uint16_t get_gpio_num(uint32_t reg_gpio);


retval_t gpio_arch_init_pin(uint32_t gpio, gpio_mode_t gpio_mode, gpio_pull_t gpio_pull){
	uint16_t pin;
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_TypeDef* GPIO_Port = (GPIO_TypeDef*) (GPIOA_BASE + (gpio & 0x0000FFFF));

	GPIO_InitStruct.Pin = (gpio>>16) & 0x0000FFFF;

	switch(gpio_mode){
	case GPIO_PIN_INPUT:
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT ;
		break;
	case GPIO_PIN_OUTPUT_PUSH_PULL:
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		break;
	case GPIO_PIN_OUTPUT_OPEN_DRAIN:
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
		break;
	case GPIO_PIN_INTERRUPT_FALLING:
		GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
		break;
	case GPIO_PIN_INTERRUPT_RISING:
		GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
		break;
	case GPIO_PIN_INTERRUPT_FALLING_RISING:
		GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
		break;
	case GPIO_PIN_ANALOG:
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		break;
	case GPIO_PIN_ANALOG_ADC_CONTROL:
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG_ADC_CONTROL;
		break;
	default:
		return RET_ERROR;
		break;
	}

	switch(gpio_pull){
	case GPIO_PIN_PULLUP:
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		break;
	case GPIO_PIN_PULLDOWN:
		GPIO_InitStruct.Pull = GPIO_PULLDOWN;
		break;
	case GPIO_PIN_NO_PULL:
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		break;
	default:
		return RET_ERROR;
		break;
	}

	HAL_GPIO_Init(GPIO_Port, &GPIO_InitStruct);

	if((gpio_mode == GPIO_PIN_INTERRUPT_FALLING) || (gpio_mode == GPIO_PIN_INTERRUPT_RISING) || (gpio_mode == GPIO_PIN_INTERRUPT_FALLING_RISING)){
		exti_lines[get_gpio_num(GPIO_InitStruct.Pin)] = (gpio_pin_t) gpio;
		pin =(uint16_t) ((gpio>>16) & 0x0000FFFF);
		if (pin == GPIO_PIN_0 ){
			HAL_NVIC_SetPriority(EXTI0_IRQn, 13, 0);
			HAL_NVIC_EnableIRQ(EXTI0_IRQn);
		}
		else if(pin == GPIO_PIN_1){
			HAL_NVIC_SetPriority(EXTI1_IRQn, 13, 0);
			HAL_NVIC_EnableIRQ(EXTI1_IRQn);
		}
		else if(pin == GPIO_PIN_2){
			HAL_NVIC_SetPriority(EXTI2_IRQn, 13, 0);
			HAL_NVIC_EnableIRQ(EXTI2_IRQn);
		}
		else if(pin == GPIO_PIN_3){
			HAL_NVIC_SetPriority(EXTI3_IRQn, 13, 0);
			HAL_NVIC_EnableIRQ(EXTI3_IRQn);
		}
		else if(pin == GPIO_PIN_4){
			HAL_NVIC_SetPriority(EXTI4_IRQn, 13, 0);
			HAL_NVIC_EnableIRQ(EXTI4_IRQn);
		}
		else if(pin <= GPIO_PIN_9){
			HAL_NVIC_SetPriority(EXTI9_5_IRQn, 13, 0);
			HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
		}
		else{
			HAL_NVIC_SetPriority(EXTI15_10_IRQn, 13, 0);
			HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
		}
	}

	return RET_OK;
}

retval_t gpio_arch_set_pin(uint32_t gpio){
	GPIO_TypeDef* GPIO_Port = (GPIO_TypeDef*) (GPIOA_BASE + (gpio & 0x0000FFFF));
	uint16_t pin = (uint16_t)((gpio>>16) & 0x0000FFFF);
	HAL_GPIO_WritePin(GPIO_Port, pin, GPIO_PIN_SET);
	return RET_OK;
}

retval_t gpio_arch_reset_pin(uint32_t gpio){
	GPIO_TypeDef* GPIO_Port = (GPIO_TypeDef*) (GPIOA_BASE + (gpio & 0x0000FFFF));
	uint16_t pin = (uint16_t)((gpio>>16) & 0x0000FFFF);
	HAL_GPIO_WritePin(GPIO_Port, pin, GPIO_PIN_RESET);
	return RET_OK;
}

retval_t gpio_arch_toggle_pin(uint32_t gpio){
	GPIO_TypeDef* GPIO_Port = (GPIO_TypeDef*) (GPIOA_BASE + (gpio & 0x0000FFFF));
	uint16_t pin = (uint16_t)((gpio>>16) & 0x0000FFFF);
	HAL_GPIO_TogglePin(GPIO_Port, pin);
	return RET_OK;
}

retval_t gpio_arch_get_pin(uint32_t gpio){
	GPIO_TypeDef* GPIO_Port = (GPIO_TypeDef*) (GPIOA_BASE + (gpio & 0x0000FFFF));
	uint16_t pin = (uint16_t)((gpio>>16) & 0x0000FFFF);
	uint16_t state;
	state = (uint16_t) HAL_GPIO_ReadPin(GPIO_Port, pin);
	return state;
}

static uint16_t get_gpio_num(uint32_t reg_gpio){
	uint16_t i;
	for(i=0; i<16; i++){
		if((reg_gpio & 0x0001)){
			return i;
		}

		reg_gpio = reg_gpio >> 1;
	}

	return 0xFFFF;
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){

	uint32_t pin = (((uint32_t)GPIO_Pin) & 0x0000FFFF);
	sysGpioInterruptCallback(exti_lines[get_gpio_num(pin)]);

}
