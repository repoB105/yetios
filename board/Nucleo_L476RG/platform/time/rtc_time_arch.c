/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * rtc_time_arch.c
 *
 *  Created on: 6 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file rtc_time_arch.c
 */

#include "rtc_time.h"
#include "platform-conf.h"

#if ENABLE_RTC_TIMESTAMP
/* Private functions */
static retval_t rtc_time_arch_prepare_timestamp(uint64_t new_timestamp, RTC_TimeTypeDef* rtc_time, RTC_DateTypeDef* rtc_date);

/**
 *
 * @return
 */
retval_t rtc_time_arch_init(void){
	return RET_OK;
}

/**
 *
 * @return
 */
retval_t rtc_time_arch_deinit(void){
	return RET_OK;
}


/**
 * @brief					Synchronizes the HW RTC with the new_timestamp param. There is a delay setting the new timestamp that should be measured for accurate applications.
 * @param new_timestamp		New timestamp to be set in milliseconds. Type uint64_t which is usually equal to unsigned long long
 * @return					Return status
 */
retval_t rtc_time_arch_set_timestamp(uint64_t new_timestamp){
	RTC_TimeTypeDef rtc_time;
	RTC_DateTypeDef rtc_date;

	rtc_time_arch_prepare_timestamp(new_timestamp, &rtc_time, &rtc_date);

	HAL_RTC_SetTime(&hrtc, &rtc_time, RTC_FORMAT_BIN);
	HAL_RTC_SetDate(&hrtc, &rtc_date, RTC_FORMAT_BIN);
	return RET_OK;
}

/**
 * @brief				Returns the current RTC timestamp. This function has a delay that should be measured for accurate applications.
 * @return				Return the timestamp.  Type uint64_t which is usually equal to unsigned long long
 */
uint64_t rtc_time_arch_get_timestamp(){

	uint64_t timestamp = 0;

	RTC_TimeTypeDef rtc_time;
	RTC_DateTypeDef rtc_date;

	uint16_t year, is_leap_year, elapsed_year, num_leap_years, num_leap_years_1970;

	HAL_RTC_GetTime(&hrtc, &rtc_time, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &rtc_date, RTC_FORMAT_BIN);

	year =  rtc_date.Year + 2000;

	is_leap_year = 0;

	if((year%4) == 0){			//Gregorian calendar for leap years
		is_leap_year = 1;
		if((year%100) == 0){
			is_leap_year = 0;
		}
		if((year%400) == 0){
			is_leap_year = 1;
		}
	}

	elapsed_year = year - 1970;
	num_leap_years = (year/4) - (year/100) + (year/400) - is_leap_year -1;
	num_leap_years_1970 = 492 - 19 + 4; //Num of leap years elapsed till 1970. Counts the 4 and 400 multiples, and discount 100 multiples.


	num_leap_years -= num_leap_years_1970;
	elapsed_year -= num_leap_years;

	timestamp += (uint64_t) ((uint64_t)num_leap_years*366ULL*24ULL*3600ULL*1000ULL);
	timestamp += (uint64_t) ((uint64_t)elapsed_year*365ULL*24ULL*3600ULL*1000ULL);

	switch(rtc_date.Month){
	case 1:
		break;
	case 2:
		timestamp += (uint64_t)(31ULL * 24ULL * 3600ULL * 1000ULL);
		break;
	case 3:
		timestamp += (uint64_t)(59ULL+(uint64_t)is_leap_year) * 24ULL * 3600ULL * 1000ULL;
		break;
	case 4:
		timestamp += (uint64_t)(90ULL+(uint64_t)is_leap_year) * 24ULL * 3600ULL * 1000ULL;
		break;
	case 5:
		timestamp += (uint64_t)(120ULL+(uint64_t)is_leap_year) * 24ULL * 3600ULL * 1000ULL;
		break;
	case 6:
		timestamp += (uint64_t)(151ULL+(uint64_t)is_leap_year) * 24ULL * 3600ULL * 1000ULL;
		break;
	case 7:
		timestamp += (uint64_t)(181ULL+(uint64_t)is_leap_year) * 24ULL * 3600ULL * 1000ULL;
		break;
	case 8:
		timestamp += (uint64_t)(212ULL+(uint64_t)is_leap_year) * 24ULL * 3600ULL * 1000ULL;
		break;
	case 9:
		timestamp += (uint64_t)(243ULL+(uint64_t)is_leap_year) * 24ULL * 3600ULL * 1000ULL;
		break;
	case 10:
		timestamp += (uint64_t)(273ULL+(uint64_t)is_leap_year) * 24ULL * 3600ULL * 1000ULL;
		break;
	case 11:
		timestamp += (uint64_t)(304ULL+(uint64_t)is_leap_year) * 24ULL * 3600ULL * 1000ULL;
		break;
	case 12:
		timestamp += (uint64_t)(334ULL+(uint64_t)is_leap_year) * 24ULL * 3600ULL * 1000ULL;
		break;
	default:
		return 0;
		break;
	}
	timestamp += (uint64_t)(rtc_date.Date) * 24ULL * 3600ULL * 1000ULL;
	timestamp += (uint64_t)(rtc_time.Hours) * 3600ULL * 1000ULL;
	timestamp += (uint64_t)(rtc_time.Minutes) * 60ULL * 1000ULL;
	timestamp += (uint64_t)(rtc_time.Seconds) * 1000ULL;
	timestamp += (uint64_t)((1023-(uint64_t)rtc_time.SubSeconds)*1000ULL) / 1024ULL;

	return timestamp;
}

/**
 * @brief					Internal function used to prepare the RTC before setting a new timestamp. Measuring the time lapsed by this function allows estimation of the set_timestamp function delay.
 * @param new_timestamp		New timestamp to be prepared
 * @param rtc_time			RTC time struct
 * @param rtc_date			RTC date struct
 * @return					Return status
 */
static retval_t rtc_time_arch_prepare_timestamp(uint64_t new_timestamp, RTC_TimeTypeDef* rtc_time, RTC_DateTypeDef* rtc_date){

	uint32_t day;
	uint16_t elapsed_leap_years = 0;
	uint16_t i;
	uint16_t real_year;
	uint16_t local_weekday;

	uint16_t is_leap_year = 0;
	uint64_t year = new_timestamp/1000;
	year = year/365;
	year = year/24;
	year = year/3600;
	year = year + 1970;
	real_year = year;
	year = real_year-2000;
	rtc_date->Year = (uint8_t) year;

	for(i=1972; i<real_year; i+=4){		//1972 First leap year since 1970
		elapsed_leap_years++;
	}

	if((real_year%4) == 0){			//Gregorian calendar for leap years
		is_leap_year = 1;
		if((real_year%100) == 0){
			is_leap_year = 0;
		}
		if((real_year%400) == 0){
			is_leap_year = 1;
		}
	}
	day = (uint32_t)(new_timestamp/(1000*3600*24));			//The elapsed days since 1st January 1970

	local_weekday = (uint16_t) (day%7);						//Init day Thursday 1st January 1970
	if(local_weekday >= 4){
		rtc_date->WeekDay = local_weekday - 3;
	}
	else{
		rtc_date->WeekDay = local_weekday + 4;
	}

	day = day - ((real_year-1970)*365) - elapsed_leap_years + 1;			//The day on this year

	if(day<=31){
		rtc_date->Month = 1;
		rtc_date->Date = day;
	}
	else if(day<= (59+is_leap_year)){
		rtc_date->Month = 2;
		rtc_date->Date = day-31;
	}
	else if(day<= (90+is_leap_year)){
		rtc_date->Month = 3;
		rtc_date->Date = day-(59+is_leap_year);
	}
	else if(day<= (120+is_leap_year)){
		rtc_date->Month = 4;
		rtc_date->Date = day-(90+is_leap_year);
	}
	else if(day<= (151+is_leap_year)){
		rtc_date->Month = 5;
		rtc_date->Date = day-(120+is_leap_year);
	}
	else if(day<= (181+is_leap_year)){
		rtc_date->Month = 6;
		rtc_date->Date = day-(151+is_leap_year);
	}
	else if(day<= (212+is_leap_year)){
		rtc_date->Month = 7;
		rtc_date->Date = day-(181+is_leap_year);
	}
	else if(day<= (243+is_leap_year)){
		rtc_date->Month = 8;
		rtc_date->Date = day-(212+is_leap_year);
	}
	else if(day<= (273+is_leap_year)){
		rtc_date->Month = 9;
		rtc_date->Date = day-(243+is_leap_year);
	}
	else if(day<= (304+is_leap_year)){
		rtc_date->Month = 10;
		rtc_date->Date = day-(273+is_leap_year);
	}
	else if(day<= (334+is_leap_year)){
		rtc_date->Month = 11;
		rtc_date->Date = day-(304+is_leap_year);
	}
	else if(day<= (365+is_leap_year)){
		rtc_date->Month = 12;
		rtc_date->Date = day-(334+is_leap_year);
	}
	rtc_time->Hours = (new_timestamp/(1000*3600))%24;
	rtc_time->Minutes = (new_timestamp/(1000*60))%60;
	rtc_time->Seconds = (new_timestamp/(1000))%60;
	rtc_time->SubSeconds = 1023 -(((new_timestamp%1000)*1024)/1000);
	rtc_time->StoreOperation = RTC_STOREOPERATION_RESET;
	rtc_time->DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	rtc_time->TimeFormat = RTC_FORMAT_BIN;

	return RET_OK;
}

/**
 * @brief				Gets the current date stored by the RTC
 * @param rtc_time_date	Return value with the current date
 * @return				Return status
 */
retval_t rtc_time_arch_get_date(rtc_time_date_t* rtc_time_date){

	RTC_TimeTypeDef rtc_time;
	RTC_DateTypeDef rtc_date;

	HAL_RTC_GetTime(&hrtc, &rtc_time, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &rtc_date, RTC_FORMAT_BIN);

	rtc_time_date->year = rtc_date.Year + 2000;
	rtc_time_date->month = rtc_date.Month;
	rtc_time_date->month_day = rtc_date.Date;
	rtc_time_date->week_day = rtc_date.WeekDay;
	rtc_time_date->hour = rtc_time.Hours;
	rtc_time_date->minute = rtc_time.Minutes;
	rtc_time_date->second = rtc_time.Seconds;


	return RET_OK;
}

/**
 * @brief				Sets the current date to the RTC timer
 * @param rtc_time_date	New date to be set
 * @return				Return status
 */
retval_t rtc_time_arch_set_date(rtc_time_date_t* rtc_time_date){

	RTC_TimeTypeDef rtc_time;
	RTC_DateTypeDef rtc_date;


	uint16_t year, is_leap_year;

	if(rtc_time_date->year>= 10000){
		return RET_ERROR;
	}

	year = rtc_time_date->year;

	if((year%4) == 0){			//Gregorian calendar for leap years
		is_leap_year = 1;
		if((year%100) == 0){
			is_leap_year = 0;
		}
		if((year%400) == 0){
			is_leap_year = 1;
		}
	}


	rtc_date.Year = rtc_time_date->year - ((rtc_time_date->year/100)*100);

	if((rtc_time_date->month < 1) || (rtc_time_date->month > 12)){
		return RET_ERROR;
	}
	rtc_date.Month = rtc_time_date->month;

	if((rtc_time_date->month_day < 1) || (rtc_time_date->month_day > 31)){
		return RET_ERROR;
	}

	switch(rtc_time_date->month){
		case 1:
			if(rtc_time_date->month_day > 31){
				return RET_ERROR;
			}
			break;
		case 2:
			if(is_leap_year){
				if(rtc_time_date->month_day > 29){
					return RET_ERROR;
				}
			}
			else{
				if(rtc_time_date->month_day > 28){
					return RET_ERROR;
				}
			}
			break;
		case 3:
			if(rtc_time_date->month_day > 31){
				return RET_ERROR;
			}
			break;
		case 4:
			if(rtc_time_date->month_day > 30){
				return RET_ERROR;
			}
			break;
		case 5:
			if(rtc_time_date->month_day > 31){
				return RET_ERROR;
			}
			break;
		case 6:
			if(rtc_time_date->month_day > 30){
				return RET_ERROR;
			}
			break;
		case 7:
			if(rtc_time_date->month_day > 31){
				return RET_ERROR;
			}
			break;
		case 8:
			if(rtc_time_date->month_day > 31){
				return RET_ERROR;
			}
			break;
		case 9:
			if(rtc_time_date->month_day > 30){
				return RET_ERROR;
			}
			break;
		case 10:
			if(rtc_time_date->month_day > 31){
				return RET_ERROR;
			}
			break;
		case 11:
			if(rtc_time_date->month_day > 30){
				return RET_ERROR;
			}
			break;
		case 12:
			if(rtc_time_date->month_day > 31){
				return RET_ERROR;
			}
			break;
		default:
			return RET_ERROR;
			break;
	}
	rtc_date.Date = rtc_time_date->month_day;

	if((rtc_time_date->week_day < 1) || (rtc_time_date->week_day > 7)){
		return RET_ERROR;
	}
	rtc_date.WeekDay = rtc_time_date->week_day;

	if(rtc_time_date->hour > 23){
		return RET_ERROR;
	}
	rtc_time.Hours = rtc_time_date->hour;

	if(rtc_time_date->minute > 59){
		return RET_ERROR;
	}
	rtc_time.Minutes = rtc_time_date->minute;

	if(rtc_time_date->second > 59){
		return RET_ERROR;
	}
	rtc_time.Seconds = rtc_time_date->second;

	HAL_RTC_SetTime(&hrtc, &rtc_time, RTC_FORMAT_BIN);
	HAL_RTC_SetDate(&hrtc, &rtc_date, RTC_FORMAT_BIN);

	return RET_OK;
}

#endif
