/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * platform_cerberus_phy.h
 *
 *  Created on: 12 de ene. de 2018
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file platform_cerberus_phy.h
 */
#ifndef APPLICATION_BOARD_PLATFORM_INCLUDE_PLATFORM_CERBERUS_PHY_H_
#define APPLICATION_BOARD_PLATFORM_INCLUDE_PLATFORM_CERBERUS_PHY_H_

#define CERBERUS_SPI_DEV		SPI_DEV_2

#define ENABLE_CC2500		1
#define ENABLE_SPIRIT1_433	1
#define ENABLE_SPIRIT1_868	1

/* *********SPIRIT1 433 DEFAULT CONFIG ***********/
#define SPIRIT1_433_CS_PIN		GPIO_PIN_B12
#define SPIRIT1_433_SDN_PIN		GPIO_PIN_C7
#define SPIRIT1_433_GPIO3_PIN	GPIO_PIN_C4

/* *********SPIRIT1 868 DEFAULT CONFIG ***********/
#define SPIRIT1_868_CS_PIN		GPIO_PIN_B0
#define SPIRIT1_868_SDN_PIN		GPIO_PIN_C6
#define SPIRIT1_868_GPIO3_PIN	GPIO_PIN_C2

/* *********CC2500 DEFAULT CONFIG ***********/
#define CC2500_CS_PIN		GPIO_PIN_B2
#define CC2500_GDO2_PIN		GPIO_PIN_C0
#endif /* APPLICATION_BOARD_PLATFORM_INCLUDE_PLATFORM_CERBERUS_PHY_H_ */
