/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * spi_driver.h
 *
 *  Created on: 3 de nov. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file spi_driver.h
 */
#ifndef APPLICATION_CORE_DRIVERS_INCLUDE_SPI_DRIVER_H_
#define APPLICATION_CORE_DRIVERS_INCLUDE_SPI_DRIVER_H_

/* Device driver IOCTL commands */
typedef enum spi_driver_ioctl_cmd_{
	ENABLE_SW_CS = 0,
	DISABLE_SW_CS = 1,

	ENABLE_SW_CS_EACH_BYTE = 2,
	DISABLE_SW_CS_EACH_BYTE = 3,

	SET_SPI_SPEED = 4,

	SET_POLARITY_HIGH = 5,
	SET_POLARITY_LOW = 6,

	SET_EDGE_1 = 7,
	SET_EDGE_2 = 8,

	SET_SPI_MODE_MASTER = 9,
	SET_SPI_MODE_SLAVE = 10,

	SPI_READ_WRITE_OP = 11,

	ENABLE_INPUT_HW_CS = 12,
	ENABLE_OUTPUT_HW_CS = 13,

	SPI_NO_CMD = 0xFFFF,
}spi_driver_ioctl_cmd_t;

typedef struct spi_driver_read_write_buffs_{
	uint8_t* ptx;
	uint8_t* prx;
	size_t size;
}spi_read_write_buffs_t;

#endif /* APPLICATION_CORE_DRIVERS_INCLUDE_SPI_DRIVER_H_ */
