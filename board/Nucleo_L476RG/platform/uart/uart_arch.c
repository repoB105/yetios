/*
 * Copyright (c) 2017, Universidad Politecnica de Madrid - B105 Electronic Systems Lab
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    This product includes software developed by the B105 Electronic Systems Lab.
 * 4. Neither the name of the B105 Electronic Systems Lab nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY UNIVERSITY AND CONTRIBUTORS ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * spi_arch.c
 *
 *  Created on: 20 de sept. de 2017
 *      Author: Roberto Rodriguez-Zurrunero  <r.rodriguezz@b105.upm.es>
 *
 */
/**
 * @file spi_arch.c
 */


#include "uart_arch.h"
#include "usart.h"
#include "system_api.h"
#include "low_power.h"
#define DEFAULT_TIMEOUT			500

#define UART_RX_BUFFER_SIZE		256
#define UART_RX_TIMER_TIMEOUT	95		//An overflow may occur in the timer if the uart speed and data flow are very high.
										//8 ms timeout prevents overflow with a 256 buffer and up to 250 kbps of continous datarate. Higher speed require a larger buffer
										//When overflow occurs old data is discarded and replaced by new one
#define UART_RX_POLL_TIME		100

#define UART_RX_MAX_READ_SIZE	192

#define UART_DEFAULT_ECHO		0

static uint32_t uart_tx_semaphore_id;

static uint32_t uart_rx_timer_id;

static uint32_t uart_tx_mutex_id;

static uint32_t uart_rx_mutex_id;

static uint8_t uart_rx_buffer[UART_RX_BUFFER_SIZE];
static uint8_t* uart_rx_ptr_head;			//Last byte stored in the buffer
static uint8_t* uart_rx_ptr_tail;			//First byte stored in the buffer
static uint16_t num_stored_bytes;

static uint16_t enable_local_echo = UART_DEFAULT_ECHO;

static void update_stored_bytes(void);
static void uart_rx_timer_cb(void const * argument);
/**
 *
 * @return
 */
retval_t uart_arch_init(){


	if((uart_tx_semaphore_id = ytSemaphoreCreate(1)) == 0){
		return RET_ERROR;
	}
	uart_tx_mutex_id = ytMutexCreate();	//Initialize blocking mutex
	uart_rx_mutex_id = ytMutexCreate();	//Initialize blocking mutex

	ytSemaphoreWait(uart_tx_semaphore_id, YT_WAIT_FOREVER);


	uart_rx_timer_id = ytTimerCreate(ytTimerPeriodic, uart_rx_timer_cb, NULL);

	num_stored_bytes = 0;

	MX_USART2_UART_Init();
	HAL_UARTEx_EnableStopMode(&huart2);
	__HAL_UART_ENABLE_IT(&huart2, UART_IT_RXNE);							//Enable the interrupt to wake up from sleep mode. The Receive_DMA does not enable this interrupt which is necessary

	HAL_UART_Receive_DMA(&huart2, uart_rx_buffer, UART_RX_BUFFER_SIZE);			//Continuously read by DMA and store in the buffer


	return RET_OK;
}

/**
 *
 * @return
 */
retval_t uart_arch_deinit(){

	ytSemaphoreDelete(uart_tx_semaphore_id);
	ytMutexDelete(uart_tx_mutex_id);
	ytMutexDelete(uart_rx_mutex_id);
	ytTimerStop(uart_rx_timer_id);
	ytTimerDelete(uart_rx_timer_id);
	HAL_UART_DMAStop(&huart2);
	HAL_UART_DeInit(&huart2);

	return RET_OK;
}

/**
 *
 * @return
 */
retval_t uart_arch_start_storing_data(void){

	uart_rx_ptr_head = uart_rx_buffer + UART_RX_BUFFER_SIZE - __HAL_DMA_GET_COUNTER(huart2.hdmarx);	//Start storing the samples
	uart_rx_ptr_tail = uart_rx_ptr_head;
	num_stored_bytes = 0;
	return ytTimerStart(uart_rx_timer_id, UART_RX_TIMER_TIMEOUT);


}

/**
 *
 * @return
 */
retval_t uart_arch_stop_storing_data(void){
	uart_rx_ptr_head = uart_rx_buffer + UART_RX_BUFFER_SIZE - __HAL_DMA_GET_COUNTER(huart2.hdmarx);	//Start storing the samples
	uart_rx_ptr_tail = uart_rx_ptr_head;
	num_stored_bytes = 0;
	return ytTimerStop(uart_rx_timer_id);
}


/**
 *
 * @param txData
 * @param size
 * @return
 */
retval_t uart_arch_write(uint8_t* txData, uint16_t size, uint32_t timeout){

	if(!size){
		return RET_ERROR;
	}
	ytMutexWait(uart_tx_mutex_id, YT_WAIT_FOREVER);
	disable_low_power_mode();
	HAL_UART_Transmit_DMA(&huart2, txData, size);
	if(ytSemaphoreWait(uart_tx_semaphore_id, timeout) != RET_OK){
		enable_low_power_mode();
		ytMutexRelease(uart_tx_mutex_id);
		return RET_ERROR;
	}
	enable_low_power_mode();
	ytMutexRelease(uart_tx_mutex_id);
	return RET_OK;
}

/**
 *
 * @param rxData
 * @param size
 * @return
 */
retval_t uart_arch_read(uint8_t* rxData, uint16_t size, uint32_t timeout){
	uint32_t start_time;
	uint16_t i;
	uint16_t read_bytes = 0;


	ytMutexWait(uart_rx_mutex_id, YT_WAIT_FOREVER);
	update_stored_bytes();
	start_time = ytGetSysTickMilliSec();
	//Now read the buffer depending on the parameter size
	if(size <= UART_RX_MAX_READ_SIZE){
		while(num_stored_bytes<size){		//Wait till the required bytes are available
			ytDelay(UART_RX_POLL_TIME);
			if((ytGetSysTickMilliSec() - start_time) > timeout){
				ytMutexRelease(uart_rx_mutex_id);
				return RET_ERROR;
			}
		}
		while (read_bytes < size){
			rxData[read_bytes] = *uart_rx_ptr_tail;
			uart_rx_ptr_tail++;
			num_stored_bytes--;
			read_bytes++;
			if (uart_rx_ptr_tail >= &uart_rx_buffer[UART_RX_BUFFER_SIZE]){
				uart_rx_ptr_tail = uart_rx_buffer;
			}
			if(enable_local_echo){
				uart_arch_write(&rxData[read_bytes-1], 1, timeout);
			}
		}

	}
	else{
		while(read_bytes < size){

			if((size-read_bytes) > UART_RX_MAX_READ_SIZE){	//If there is no room in the buffer for the remaining bytes
				while(num_stored_bytes < UART_RX_MAX_READ_SIZE){		//Wait till the required bytes are available
					ytDelay(UART_RX_POLL_TIME);
					if((ytGetSysTickMilliSec() - start_time) > timeout){
						ytMutexRelease(uart_rx_mutex_id);
						return RET_ERROR;
					}
				}
				for(i=0; i<UART_RX_MAX_READ_SIZE; i++){
					rxData[read_bytes] = *uart_rx_ptr_tail;
					uart_rx_ptr_tail++;
					num_stored_bytes--;
					read_bytes++;
					if (uart_rx_ptr_tail >= &uart_rx_buffer[UART_RX_BUFFER_SIZE]){
						uart_rx_ptr_tail = uart_rx_buffer;
					}
					if(enable_local_echo){
						uart_arch_write(&rxData[read_bytes-1], 1, timeout);
					}
				}

			}
			else{						//There is room in the buffer for the remaining bytes
				while(num_stored_bytes < (size-read_bytes)){		//Wait till the required bytes are available
					ytDelay(UART_RX_POLL_TIME);
					if((ytGetSysTickMilliSec() - start_time) > timeout){
						ytMutexRelease(uart_rx_mutex_id);
						return RET_ERROR;
					}
				}
				while(read_bytes < size){		//Read the last bytes
					rxData[read_bytes] = *uart_rx_ptr_tail;
					uart_rx_ptr_tail++;
					num_stored_bytes--;
					read_bytes++;
					if (uart_rx_ptr_tail >= &uart_rx_buffer[UART_RX_BUFFER_SIZE]){
						uart_rx_ptr_tail = uart_rx_buffer;
					}
					if(enable_local_echo){
						uart_arch_write(&rxData[read_bytes-1], 1, timeout);
					}
				}
			}



		}

	}

	ytMutexRelease(uart_rx_mutex_id);
	return RET_OK;
}

/**
 *
 * @param rxData
 * @param size
 * @return
 */
retval_t uart_arch_read_line(uint8_t* rxData, uint16_t size, uint32_t timeout){
	uint32_t start_time;
	uint16_t read_bytes = 0;

	ytMutexWait(uart_rx_mutex_id, YT_WAIT_FOREVER);
	update_stored_bytes();

	start_time = ytGetSysTickMilliSec();
	while(read_bytes < size){
			while(num_stored_bytes){
				if(((*uart_rx_ptr_tail) != '\r') && ((*uart_rx_ptr_tail) != '\n')){		//If not line end, store the character

					if((*uart_rx_ptr_tail) != '\b'){

						rxData[read_bytes] = *uart_rx_ptr_tail;
						read_bytes++;
						uart_rx_ptr_tail++;
						num_stored_bytes--;
						if (uart_rx_ptr_tail >= &uart_rx_buffer[UART_RX_BUFFER_SIZE]){
							uart_rx_ptr_tail = uart_rx_buffer;
						}
						if(read_bytes >= size){
							ytMutexRelease(uart_rx_mutex_id);
							return RET_ERROR;
						}
						if(enable_local_echo){
							uart_arch_write(&rxData[read_bytes-1], 1, timeout);
						}
					}else{			//Move Backwards. Dont store the character and reduce one
						if(read_bytes){
							read_bytes--;
						}
						uart_rx_ptr_tail++;
						num_stored_bytes--;
						if (uart_rx_ptr_tail >= &uart_rx_buffer[UART_RX_BUFFER_SIZE]){
							uart_rx_ptr_tail = uart_rx_buffer;
						}
						if(enable_local_echo){
							uart_arch_write((uint8_t*)"\b \b", 3, timeout);
						}
					}

				}
				else{		//Line end detected.
					rxData[read_bytes] = '\0';
					uart_rx_ptr_tail++;
					num_stored_bytes--;
					read_bytes++;
					if (uart_rx_ptr_tail >= &uart_rx_buffer[UART_RX_BUFFER_SIZE]){
						uart_rx_ptr_tail = uart_rx_buffer;
					}

					//Check if the next element is a '\r' or a '\n' which is very feasible when the line end is "\r\n"
					if(num_stored_bytes){
						if(((*uart_rx_ptr_tail) == '\r') || ((*uart_rx_ptr_tail) == '\n')){
							uart_rx_ptr_tail++;
							num_stored_bytes--;
							if (uart_rx_ptr_tail >= &uart_rx_buffer[UART_RX_BUFFER_SIZE]){
								uart_rx_ptr_tail = uart_rx_buffer;
							}
						}
					}

					if(enable_local_echo){
						uart_arch_write((uint8_t*)"\r\n", 2, timeout);
					}
					ytMutexRelease(uart_rx_mutex_id);
					return RET_OK;
				}


			}
			ytDelay(UART_RX_POLL_TIME);
			if((ytGetSysTickMilliSec() - start_time) > timeout){
				ytMutexRelease(uart_rx_mutex_id);
				return RET_ERROR;
			}
		}
		ytMutexRelease(uart_rx_mutex_id);
		return RET_ERROR;
}
/**
 *
 * @param speed
 * @return
 */
retval_t uart_arch_set_speed(uint32_t speed){
	huart2.Instance = USART2;
	huart2.Init.BaudRate = speed;
	ytMutexWait(uart_rx_mutex_id, YT_WAIT_FOREVER);
	ytMutexWait(uart_tx_mutex_id, YT_WAIT_FOREVER);
	disable_low_power_mode();

	if (HAL_UART_Init(&huart2) != HAL_OK)
	{
	_Error_Handler(__FILE__, __LINE__);
	}
	enable_low_power_mode();
	ytMutexRelease(uart_rx_mutex_id);
	ytMutexRelease(uart_tx_mutex_id);
	return RET_OK;
}

/**
 *
 * @param parity
 * @return
 */
retval_t uart_arch_set_parity(uint16_t parity){
	huart2.Instance = USART2;

	ytMutexWait(uart_rx_mutex_id, YT_WAIT_FOREVER);
	ytMutexWait(uart_tx_mutex_id, YT_WAIT_FOREVER);
	disable_low_power_mode();

	switch(parity){
	case 0:
		huart2.Init.Parity = UART_PARITY_NONE;
		break;
	case 1:
		huart2.Init.Parity = UART_PARITY_EVEN;
		break;
	case 2:
		huart2.Init.Parity = UART_PARITY_ODD;
		break;
	default:
		enable_low_power_mode();
		ytMutexRelease(uart_rx_mutex_id);
		ytMutexRelease(uart_tx_mutex_id);
		return RET_ERROR;
		break;
	}

	if (HAL_UART_Init(&huart2) != HAL_OK)
	{
	_Error_Handler(__FILE__, __LINE__);
	}
	enable_low_power_mode();
	ytMutexRelease(uart_rx_mutex_id);
	ytMutexRelease(uart_tx_mutex_id);
	return RET_OK;
}

/**
 *
 * @return
 */
retval_t uart_arch_enable_echo(void){
	enable_local_echo = 1;
	return RET_OK;
}

/**
 *
 * @return
 */
retval_t uart_arch_disable_echo(void){
	enable_local_echo = 0;
	return RET_OK;
}


/**
 *
 * @param argument
 */
static void uart_rx_timer_cb(void const * argument){
	update_stored_bytes();
}


/**
 *
 */
static void update_stored_bytes(void){
	uint8_t* new_uart_rx_ptr_head;
	uint32_t dma_counter = __HAL_DMA_GET_COUNTER(huart2.hdmarx);	//dma remaining downcounter
	uint16_t new_bytes = 0;

	if(!dma_counter){							//Prevent error if the dma downcounter is 0 (the uart pointer will overflow)
		dma_counter = UART_RX_BUFFER_SIZE;
	}
	new_uart_rx_ptr_head = uart_rx_buffer + UART_RX_BUFFER_SIZE - dma_counter;

	if (new_uart_rx_ptr_head >= uart_rx_ptr_head){
		new_bytes = (uint16_t)(new_uart_rx_ptr_head - uart_rx_ptr_head);
		num_stored_bytes += new_bytes;
	}
	else{
		new_bytes = UART_RX_BUFFER_SIZE - ((uint16_t)(uart_rx_ptr_head - new_uart_rx_ptr_head));
		num_stored_bytes += new_bytes;
	}

	uart_rx_ptr_head = new_uart_rx_ptr_head; //Update head pointer

	if(num_stored_bytes >= UART_RX_BUFFER_SIZE){		//The buffer is full. Move the tail pointer
		num_stored_bytes = UART_RX_BUFFER_SIZE;
		uart_rx_ptr_tail = uart_rx_ptr_head;			//The first writen byte is the last unwritenn byte when the buffer is full
	}
}

/**
 *
 * @param huart
 */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){
	if(huart->Instance == USART2){
		ytSemaphoreRelease(uart_tx_semaphore_id);
	}
}
